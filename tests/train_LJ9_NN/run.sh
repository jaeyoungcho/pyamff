echo "Running LJ9 Test"
#../../bin/pyamff 
pyamff 

if [[ $(tail -1 pyamff.log) = *'1   0.26226984   0.07315895   0.00206539'* ]]; then
    echo "Passed LJ9"
else
    echo 'Failed LJ9 -- incorrect output values'
    exit 1
fi
