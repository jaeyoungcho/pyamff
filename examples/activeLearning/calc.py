import numpy as np
from pyamff.aseCalc import aseCalc
from pyamff.activeLearn import activeLearn
import torch, sys
from ase.io import read

args = sys.argv
images = read(args[1], index=":")

calc = activeLearn('./config.ini')

#traj=[images[0:2], images[2:4], images[4:6]]
traj=[images[0:2], images[2:4]]
#traj=images


 
weights = {}
bias = {}
params = []
l1_weight = [[-0.06245638, -0.02177071], [ 0.01106360, -0.02641876]]
l1_bias   = [-0.15927899, 0.08015668]
l2_weight = [[-0.01062437, -0.00506942],[0.00031429, -0.12331749]]
l2_bias   = [0.18475361, 0.17465138]
l3_weight = [[-0.10928684, 0.02496535]]
l3_bias   = [0.12033310]

l1_weight1 = [[-0.12125415, -0.09574964], [-0.12175528,-0.01690174]]
l1_bias1   = [-0.00525357, 0.16651967]
l2_weight1 = [[0.14360507, -0.03717202],[0.18517374,-0.01645058]]
l2_bias1   = [0.03095685, -0.14551263]
l3_weight1 = [[-0.12728526, 0.03061981]]
l3_bias1   = [-0.00860174]

params = [l1_weight, l1_bias, l2_weight, l2_bias, l3_weight, l3_bias,
          l1_weight1, l1_bias1, l2_weight1, l2_bias1, l3_weight1, l3_bias1]

#calc.train(images, debug=True, params=params)
debug=True
for image in traj:
   calc.train(image, debug=debug, params=params)
   #debug = False
   #print('params', params)
