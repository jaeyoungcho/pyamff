PROGRAM testH2
  USE training
  IMPLICIT NONE
  ! This program is only for testing energy training for Pd3H2 (single image) 
  CHARACTER*8 :: opt_type
  INTEGER :: i
  INTEGER, PARAMETER :: max_epoch=5
  INTEGER, PARAMETER :: nAtoms=2, nelement=1
  INTEGER, DIMENSION(nAtoms) :: atomicNumbers
  INTEGER, DIMENSION(nelement) :: uniqueNrs
  DOUBLE PRECISION, DIMENSION(nAtoms,3) :: pos_car
  REAL, DIMENSION(9) :: box
 
  ! Set atomic numbers and unique element numbers
  atomicNumbers=(/1, 1/)
  uniqueNrs=(/1/)

  ! Set positions in cartesian 
  pos_car(1,1:3)=(/5., 5., 5./)
  pos_car(2,1:3)=(/5., 5.9, 5./)

  ! Set box in 1d array
  box(1:9)=(/10., 0., 0., 0., 10.9, 0., 0., 0., 10./)

  ! Set optimizer type
  opt_type='adam'
  ! Set number of images 
  nimages=1
  ! Set energy training .true.
  energy_training = .TRUE.
  
  ! Initiate training: read mlff.pyamff, allocate arrays for NN and backward propagations
  CALL train_init(nAtoms, nelement, atomicNumbers, uniqueNrs)
  ! Set target energy values after train_init
  targetE(1)=6.141598394997107

  ! Do loop is present here due to calculate all images' fps, energy/forces before training. 
  ! From i=1, nimages-1, execute neighborlist, fingerprints, and forward propagation
  ! When only i=nimages, trainer is executed. 
  DO i=1, nimages
    img_idx=i
    CALL trainExec(nAtoms,pos_car,box,atomicNumbers,nelement,uniqueNrs,opt_type,max_epoch)
  END DO

  ! Deallocate all allocated arrays
  CALL traincleanup

END PROGRAM
