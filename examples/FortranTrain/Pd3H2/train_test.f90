PROGRAM testPd3H2
  USE training
  IMPLICIT NONE
  ! This program is only for testing energy training for Pd3H2 (single image) 
  CHARACTER*8 :: opt_type
  INTEGER :: i
  INTEGER, PARAMETER :: max_epoch=1
  INTEGER, PARAMETER :: nAtoms=5, nelement=2
  INTEGER, DIMENSION(nAtoms) :: atomicNumbers
  INTEGER, DIMENSION(nelement) :: uniqueNrs
  DOUBLE PRECISION, DIMENSION(nAtoms,3) :: pos_car
  REAL, DIMENSION(9) :: box
 
  ! Set atomic numbers and unique element numbers
  atomicNumbers=(/1, 46, 46, 46, 1/)
  uniqueNrs=(/1, 46/)

  ! Set positions in cartesian 
  pos_car(1,1:3)=(/20.95283952, 20.17586172, 18.59396852/)
  pos_car(2,1:3)=(/21.03637452, 19.41463472, 20.05463852/)
  pos_car(3,1:3)=(/19.93023098, 20.58050723, 21.40603148/)
  pos_car(4,1:3)=(/18.96362548, 18.94142759, 20.00113628/)
  pos_car(5,1:3)=(/19.93354457, 21.05857241, 20.04938567/)

  ! Set box in 1d array
  box(1:9)=(/40., 0., 0., 0., 40., 0., 0., 0., 40./)

  ! Set optimizer type
  opt_type='adam'
  ! Set number of images 
  nimages=1
  ! Set energy training .true.
  energy_training = .TRUE.
  
  ! Initiate training: read mlff.pyamff, allocate arrays for NN and backward propagations
  CALL train_init(nAtoms, nelement, atomicNumbers, uniqueNrs)
  ! Set target energy values after train_init
  targetE(1)=-56.930236

  ! Execution of training over images. Trainer is executed only when i=nimages
  ! From i=1, nimages-1, execute neighborlist, fingerprints, and forward propagation 
  DO i=1, nimages
    img_idx=i
    CALL trainExec(nAtoms,pos_car,box,atomicNumbers,nelement,uniqueNrs,opt_type,max_epoch)
  END DO

  ! Deallocate all allocated arrays
  CALL traincleanup


END PROGRAM
