
   oo      o       o   oooooo
  o  o     oo     oo   o     o
 o    o    o o   o o   o     o
o      o   o  o o  o   o     o
oooooooo   o   o   o   oooooo
o      o   o       o   o
o      o   o       o   o
o      o   o       o   o

Amp: Atomistic Machine-learning Package
Developed by Andrew Peterson, Alireza Khorshidi, and others,
Brown University.
PI Website: http://brown.edu/go/catalyst
Official repository: http://bitbucket.org/andrewpeterson/amp
Official documentation: http://amp.readthedocs.io/
Citation:
  Alireza Khorshidi & Andrew A. Peterson,
  Computer Physics Communications 207: 310-324 (2016).
  http://doi.org/10.1016/j.cpc.2016.05.010
======================================================================
User: as68397
Hostname: halifax.oden.utexas.edu
Date: 2021-08-31T17:55:22 (2021-08-31T22:55:22 UTC)
Architecture: x86_64
PID: 8998
Amp version: 0.7.0-beta
Amp directory: /home/as68397/amp/amp
 Last commit: cf250bcc5b0be0f2ccb63a34f17c872f6096bdb9
 Last commit date: 2021-07-30 14:50:37 +0000
Python: v3.7.4: /usr/local/bin/python
ASE v3.22.0: /home/as68397/ase/ase
NumPy v1.19.4: /usr/local/lib/python3.7/site-packages/numpy
SciPy v1.4.1: /home/as68397/.local/lib/python3.7/site-packages/scipy
ZMQ/PyZMQ v4.3.2/v18.1.0: /usr/local/lib/python3.7/site-packages/zmq
pxssh: Not available from pxssh.
pxssh (via pexpect v4.7.0): /usr/local/lib/python3.7/site-packages/pexpect/pxssh.py
======================================================================
Serial operation on one core specified.

Amp training started. 2021-08-31T17:55:22

Descriptor: Gaussian
  (<amp.descriptor.gaussian.Gaussian object at 0x7f0b3c0af390>)
Model: NeuralNetwork
  (<amp.model.neuralnetwork.NeuralNetwork object at 0x7f0b3c0af3d0>)
Attempting to read images from file a.traj.
Hashing images...
 1 unique images after hashing.
...hashing completed. 1.5 ms

Descriptor
==========
Cutoff function: <Cosine cutoff with Rc=6.500 from amp.descriptor.cutoffs>
Finding unique set of elements in training data.
1 unique elements included: Au
Number of symmetry functions for each element:
 Au: 2
Au feature vector functions:
 0: G2, Au, eta = 1269.8, offset = 0.74
 1: G4, (Au, Au), eta=0.005, gamma=1.0, zeta=241.0
Calculating neighborlists...
 Data stored in file amp-neighborlists.
 File exists with 0 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...neighborlists calculated. 110.3 ms
Fingerprinting images...
 Data stored in file amp-fingerprints.
 File exists with 0 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...fingerprints calculated. 1.7 ms
Calculating fingerprint derivatives...
 Data stored in file amp-fingerprint-primes.
 File exists with 0 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...fingerprint derivatives calculated. 6.6 ms

Model fitting
=============
Regression in atom-centered mode.
Calculating new fingerprint range; this range is part of the model.
Hidden-layer structure:
 Au: (2,)
Initializing with random weights.
Initializing with random scalings.
Starting parameter optimization.
 Optimizer: <function minimize at 0x7f0b06cab170>
 Optimizer kwargs: {'method': 'BFGS', 'options': {'gtol': 1e-15}, 'jac': True, 'args': (True,)}
 Loss function convergence criteria:
  energy_rmse: 0.005
  energy_maxresid: None
  force_rmse: 0.005
  force_maxresid: None
 Loss function set-up:
  energy_coefficient: 1.0
  force_coefficient: 0.02
  overfit: 0.0
  weight duplicates:False


                                                          Energy                     Force
 Step                Time   Loss (SSD)   EnergyRMSE     MaxResid    ForceRMSE     MaxResid
===== =================== ============ ============ ============ ============ ============
    1 2021-08-31T17:55:22   1.1111e-02 0.0000e+00 C 0.0000e+00 C 7.4536e-01 - 2.0000e+00 C
    2 2021-08-31T17:55:22   1.1111e-02 8.3526e-04 C 8.3526e-04 C 7.4532e-01 - 2.0000e+00 C
    3 2021-08-31T17:55:22   1.1110e-02 7.4726e-04 C 7.4726e-04 C 7.4530e-01 - 2.0000e+00 C
    4 2021-08-31T17:55:22   1.1107e-02 3.9526e-04 C 3.9526e-04 C 7.4522e-01 - 2.0000e+00 C
    5 2021-08-31T17:55:22   1.1101e-02 3.2142e-05 C 3.2142e-05 C 7.4501e-01 - 2.0000e+00 C
    6 2021-08-31T17:55:22   1.1078e-02 1.7430e-03 C 1.7430e-03 C 7.4414e-01 - 2.0000e+00 C
    7 2021-08-31T17:55:22   1.1025e-02 4.2285e-03 C 4.2285e-03 C 7.4187e-01 - 2.0000e+00 C
    8 2021-08-31T17:55:22   1.0933e-02 7.1549e-03 - 7.1549e-03 C 7.3762e-01 - 2.0000e+00 C
    9 2021-08-31T17:55:22   1.0770e-02 1.0160e-02 - 1.0160e-02 C 7.3031e-01 - 2.0000e+00 C
   10 2021-08-31T17:55:22   1.0470e-02 1.0290e-02 - 1.0290e-02 C 7.1987e-01 - 2.0000e+00 C
   11 2021-08-31T17:55:22   1.0169e-02 6.5037e-03 - 6.5037e-03 C 7.1157e-01 - 2.0000e+00 C
   12 2021-08-31T17:55:22   1.0153e-02 2.4083e-03 C 2.4083e-03 C 7.1229e-01 - 2.0000e+00 C
   13 2021-08-31T17:55:22   1.0131e-02 4.5977e-05 C 4.5977e-05 C 7.1172e-01 - 2.0000e+00 C
   14 2021-08-31T17:55:22   1.0126e-02 7.8705e-04 C 7.8705e-04 C 7.1154e-01 - 2.0000e+00 C
   15 2021-08-31T17:55:22   1.0126e-02 2.9585e-04 C 2.9585e-04 C 7.1153e-01 - 2.0000e+00 C
   16 2021-08-31T17:55:22   1.0125e-02 6.5761e-06 C 6.5761e-06 C 7.1153e-01 - 2.0000e+00 C
   17 2021-08-31T17:55:22   1.0125e-02 1.3968e-07 C 1.3968e-07 C 7.1153e-01 - 2.0000e+00 C
   18 2021-08-31T17:55:22   1.0125e-02 2.7727e-09 C 2.7727e-09 C 7.1153e-01 - 2.0000e+00 C
   19 2021-08-31T17:55:22   1.0125e-02 1.4779e-12 C 1.4779e-12 C 7.1153e-01 - 2.0000e+00 C
   20 2021-08-31T17:55:22   1.0125e-02 9.4739e-15 C 9.4739e-15 C 7.1153e-01 - 2.0000e+00 C
   21 2021-08-31T17:55:22   1.0125e-02 9.4739e-15 C 9.4739e-15 C 7.1153e-01 - 2.0000e+00 C
...optimization unsuccessful. 55.8 ms
...maximum absolute value of loss prime: 1.895e-14
Amp not trained successfully. Saving current parameters.
Parameters saved in file "amp-untrained-parameters.amp".
This file can be opened with `calc = Amp.load('amp-untrained-parameters.amp')`
Timing:                      incl.     excl.
---------------------------------------------------
train:                       0.232     0.052  17.8% |------|
 calculate_fingerprints:     0.119     0.119  41.1% |---------------|
 fit model:                  0.059     0.059  20.4% |-------|
 hash_images:                0.003     0.003   1.0% |
Other:                       0.057     0.057  19.8% |-------|
---------------------------------------------------
Total:                                 0.290 100.0%

