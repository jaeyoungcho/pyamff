from amp import Amp
from amp.descriptor.gaussian import Gaussian
from amp.model.neuralnetwork import NeuralNetwork
from amp.model import LossFunction

#calc = Amp(descriptor=Gaussian(),model=NeuralNetwork(),label='calc')
#calc = Amp(descriptor=Gaussian(Gs={"H":[{"type":"G2","element":"H","eta":5,"offset":0.2},"type":"G4","element":"H","eta":2,"offset":0.2}]}),cores=1, model=NeuralNetwork(hiddenlayers=(2,)))
amp_fg ={"Au":[{"type":"G2","element":"Au","eta":1269.80,"offset":0.74},
{"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":1.0, "zeta":241.0}]}

#amp_fg ={"O":[{"type":"G2","element":"O","eta":0.0,"offset":0.0}]}
#amp_fg ={"O":[{"type":"G4", "elements":["O", "O"],"eta":0.5, "gamma":-1.0, "zeta":-1.0}]}
#{"type":"G4", "elements":["O", "O"],"eta":0.005, "gamma":-1.0, "zeta":1.0}]}
calc = Amp(descriptor=Gaussian(Gs=amp_fg),cores=1, model=NeuralNetwork(hiddenlayers=(2,)))

#conv = {'energy_rmse':0.02}
conv = {'energy_rmse':0.005,'force_rmse':0.005}
calc.model.lossfunction = LossFunction(convergence=conv,force_coefficient=0.02)
calc.train(images='a.traj')

"""
#type centralElemnt  eta      Rs        Rc
G1         O O       5.00     0.0       6.5
#G2    centralElemnt neighbor1 neighbor2 eta zeta  lambda thetas  Rc
G2          O            O         O    0.005 1.0   -1.0     0     6.5
"""

"""
#
1 1
#
G1   Au   Au   1269.80    0.74       6.0
#
G2 Au Au   Au    0.005   241.0  1.00  0.5     6.0


"""
