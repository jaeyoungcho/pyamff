#!/usr/bin/env python
import os
from pyamff.neighborlist import NeighborLists
from pyamff.config import ConfigClass
from pyamff.fingerprints.behlerParrinello import BehlerParrinello, represent_BP
from ase.io import Trajectory

#Read and set up setting parameters
config = ConfigClass()
config.initialize()


#Fetch fingerprint parameters in Format: {'H':[G1, G2], 'Pd':[G1, G2]}
fp_paras = config.config['fp_paras'].fp_paras 


#Read in images
images = Trajectory(config.config['trajectory_file'], 'r')


#Convert fingerprint papamter objects and store in a list
g_paras = []
for key in fp_paras.keys():
   g_paras.extend(fp_paras[key])


#Set up NeighborLists calculator
nl = NeighborLists(cutoff = 6.0)


#Do the calculation
(n_dists, n_data_pairs, n_data_triplets, n_list, n_angles) = nl.calculate(images)

#Calculate fingerprints
g1_g2, dg1_dg2 = represent_BP(n_dists, n_data_pairs, n_data_triplets, n_list, n_angles, [0,0], [1,1], fp_paras=g_paras)
print(g1_g2)

