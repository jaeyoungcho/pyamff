#!/usr/bin/env python

#setup copied from AMP - thanks Andy!

import os
import warnings

try:
    from numpy.distutils.core import Extension, setup
except ImportError:
    msg = ("Please install numpy (version 1.7.0 or greater) before installing pyamff"
           "Pyamff uses numpy's installer so it can compile the fortran modules with f2py."
           "You should be able to do this with the command : pip install numpy")
    raise RuntimeError(msg)

name = 'pyamff'
version = open(os.path.join('pyamff', 'VERSION')).read().strip()
description = 'Python Atom-Centered Machine Learning Force Field'
long_description = open('README.rst').read()
packages = ['pyamff']
package_dir = {'pyamff': 'pyamff'}
classifiers = ['Programming Language :: Python',
               'Programming Language :: Python :: 3',
               'Programming Language :: Python :: 3.7']
install_requires = ['numpy>=1.7.0', 'pytorch', 'ase', 'pyyaml', 'pickle']
ext_modules = [Extension(name='pyamff.fmodules',
                         sources=[
                                  'pyamff/fortran/fptype.f90',
                                  'pyamff/fortran/neighborlist.f90',
                                  'pyamff/fortran/behlerParrinello.f90',
                                  'pyamff/fortran/nntype.f90',
                                  'pyamff/fortran/normalization.f90',
                                  'pyamff/fortran/fingerprints.f90',
                                  'pyamff/fortran/lossgrad.f90',
                                  'pyamff/fortran/neuralnetwork.f90',
                                  'pyamff/fortran/nnmodule.f90',
                                  'pyamff/fortran/pyamffFit.f90',
                                  'pyamff/fortran/pyamffCalc.f90'
                                  ])]

author = 'Henkelman and Li Groups'
url = 'https://gitlab.com/pyamff'
package_data = {'pyamff': ['VERSION']}

scripts = ['bin/pyamff']

try:
    setup(name=name,
          version=version,
          description=description,
          long_description=long_description,
          packages=packages,
          package_dir=package_dir,
          classifiers=classifiers,
          install_requires=install_requires,
          scripts=scripts,
          ext_modules=ext_modules,
          author=author,
          url=url,
          package_data=package_data,
          )
except SystemExit as ex:
    if 'pyamff.fmodules' in ex.args[0]:
        warnings.warn('It looks like no fortran compiler is present. '
                      'Installing pyamff without fortran modules.')
    else:
        raise ex
    setup(name=name,
          version=version,
          description=description,
          long_description=long_description,
          packages=packages,
          package_dir=package_dir,
          classifiers=classifiers,
          install_requires=install_requires,
          scripts=scripts,
          ext_modules=[],
          author=author,
          url=url,
          package_data=package_data,
          )
    warnings.warn('Installed pyamff without fortran modules since no fortran '
                  'compiler was found. The code will run slow as a result.')
