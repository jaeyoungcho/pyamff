"""
This calculator is used to load a trained machine-learning model and do calculations
"""
from __future__ import division

import numpy as np

from ase.neighborlist import NeighborList
from ase.calculators.calculator import Calculator, all_changes
from pyamff.utilities.preprocessor import generateInputs, normalize
from ase.calculators.calculator import PropertyNotImplementedError
from pyamff.mlModels.pytorchNN import NeuralNetwork
from pyamff.utilities.preprocessor import normalizeParas
from pyamff.fingerprints.fingerprints import Fingerprints
from pyamff.fingerprints.fingerprintsWrapper import atomCenteredFPs
from pyamff.neighborlist import NeighborLists
import torch, sys, time

def loadModel(model_path, modelType='NeuralNetwork'):
    loaded = torch.load(model_path)
    modelParameters = loaded['Modelparameters']
    model = NeuralNetwork(
             hiddenlayers=modelParameters['hiddenlayers'],
             nFPs=modelParameters['nFPs'],
             forceTraining=modelParameters['forceTraining'],
             slope=modelParameters['slope'],
             activation=modelParameters['activation']
            )
    model.load_state_dict(loaded['state_dict']) 
    return model, loaded['preprocessParas']

class aseCalc(Calculator):
    implemented_properties = ['energy', 'forces']
    default_parameters = {}
    nolabel = True

    def __init__(self, model, modelType='NeuralNetwork', preprocessParas=None, **kwargs):
        Calculator.__init__(self, **kwargs)
        self.modelType = modelType
        if preprocessParas is None:
            if isinstance(model, str):
                self.model, self.preprocessParas = loadModel(model, modelType)
            else:
                print('Please assing a path to model or a pre-defined modle')
                sys.exit(2)
        else:
            self.model = model
            self.preprocessParas = preprocessParas
        self.Gs = self.preprocessParas['fingerprints'].fp_paras
        print('Gs:',self.Gs)
        self.nfps = {}
        for key in self.Gs.keys():
          self.nfps[key] = len(self.Gs[key])
        #self.Gs = self.preprocessParas['fingerprints']
        self.fprange = self.preprocessParas['fprange']
        self.intercept = self.preprocessParas['intercept']
        print('keys',self.Gs.keys())
        self.fpcalc = Fingerprints(uniq_elements=self.Gs.keys(), nfps=self.nfps)
        self.ttime = 0

    def calculate(self, atoms=None, properties=['energy'], system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)
        energy, forces = self.calculateFingerprints(self.atoms)
        self.results['energy'] = (energy + self.intercept).data.numpy()[0]
        self.results['forces'] = forces.data.numpy()

    def calculateFingerprints(self, atoms=None):
        images = {0:atoms}
        keylist = [0]
        chemsymbols = atoms.get_chemical_symbols()
        fps, dfps = self.fpcalc.calcFPs(atoms, chemsymbols)
        acf = atomCenteredFPs()
        acf.sortFPs(chemsymbols, fps, self.nfps, properties=None, keylist=keylist, fingerprintDerDB=dfps)
        acfs=atomCenteredFPs()
        acf.stackFPs([acf])
        fprange, magnitudeScale, interceptScale = normalizeParas(self.fprange)
        acf.normalizeFPs(fprange, magnitudeScale, interceptScale)
        st = time.time()
        predEnergies, predForces = self.model(acf.allElement_fps, acf.dgdx, acf, device=torch.device("cpu"))
        usedtime = time.time() - st
        #self.ttime += usedtime
        #print('OneTIME:', self.ttime)
        return predEnergies, predForces
