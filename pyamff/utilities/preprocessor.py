#from . import generateTensorFlowArrays
#from .. import FileDatabase
import numpy as np
import torch
import copy, math, sys
from pyamff.fingerprints.fingerprintsWrapper import atomCenteredFPs
from collections import OrderedDict, Counter

#def fetchProp(images, startkey=0, forceTraining=False):
def fetchProp(images, refEs=None, forceTraining=False, activeLearning=False):
    energies = []
    forces = []
    properror = False
    if refEs is None:
      for i in range(len(images)):
          nsymbols = Counter(list(images[i].symbols))
          try:
              energies.append(images[i].get_potential_energy(apply_constraint=False))
          except:
              sys.stderr.write('Image %d has no property of energy\n'%(i))
              properror = True
              pass
          if forceTraining:
              try:
                  forces.append(images[i].get_forces(apply_constraint=False).tolist())
              except:
                  sys.stderr.write('Image %d has no property of force\n'%(i))
                  properror = True
                  pass
    else:
      for i in range(len(images)):
          nsymbols = Counter(list(images[i].symbols))
          nelems = []
          res = []
          for key in refEs:
            if key not in nsymbols:
              continue
            nelems.append(nsymbols[key])
            res.append(refEs[key])
          try:
              energies.append((images[i].get_potential_energy(apply_constraint=False)
                              - np.dot(np.array(nelems), np.array(res)))/len(images[i]))
          except:
              sys.stderr.write('Image %d has no property of energy\n'%(i))
              properror = True
              pass
          if forceTraining:
              try:
                  forces.append(images[i].get_forces(apply_constraint=False).tolist())
              except:
                  sys.stderr.write('Image %d has no property of force\n'%(i))
                  properror = True
                  pass
    if properror:
       sys.exit(2)
    properties = OrderedDict()
    trainingimages = OrderedDict()

    slope = None
    intercept = None
    energies = np.array(energies)
    if len(images) >1 and not activeLearning:
        intercept = np.mean(energies)
        energies = energies - intercept
        slope = np.mean(np.abs(energies))
    else:
        intercept = 0.5
        energies = energies - intercept
        slope = (0.5/(np.abs(energies)))[0]
        
    for i in range(len(images)):
        properties[i] = [np.array([energies[i]]), forces[i]]
        trainingimages[i] = images[i]
    return trainingimages, properties, slope, intercept

#collate_fn: imagesFPs: a list of atomsCenteredFps
#def batchGenerator(acfs):
    #keylist = list(imagesFPs.keys())
    #interval = int(len(keylist)/numbbatch)
    #if len(keylist)%numbbatch != 0:
    #  numbbatch += 1
    #fprange_temp = batch.fprange
    #for element in elements:
    #   if element not in minlist:
    #     minlist[element] = np.array([fprange_temp[element][0].tolist()])
    #     maxlist[element] = np.array([fprange_temp[element][1].tolist()])
    #   else:
    #     minlist[element] = np.concatenate(( minlist[element], np.array([fprange_temp[element][0].tolist()])), axis = 0)
    #     maxlist[element] = np.concatenate(( maxlist[element], np.array([fprange_temp[element][1].tolist()])), axis = 0)
    #for element in elements:
    #  minv = np.min( minlist[element], axis=0)
    #  maxv = np.max( maxlist[element], axis=0)
    #  fprange[element] = [minv, maxv, maxv-minv]
#    return batch


def batchNormalize(batch, fprange, magnitudeScale, interceptScale, force_coefficient=None):
    print('Normalizing')
    elements = list(fprange.keys())
    #print("fprange", fprange)
    #for batch in batches:
    for element in elements:
        total_atoms = len(batch.allElement_fps[element])
        mags = magnitudeScale[element].repeat(total_atoms,1)
        inters = interceptScale[element].repeat(total_atoms,1)
        batch.allElement_fps[element] = (torch.mul(torch.tensor(batch.allElement_fps[element]), mags) + inters).double()
        batch.allElement_fps[element].requires_grad = True
        if force_coefficient > 1.e-5:
            batch.dgdx[element] = torch.mul(torch.tensor(batch.dgdx[element]),
                                      torch.flatten(mags)[batch.dEdg_AtomIndices[element]])
            batch.dgdx[element].requires_grad = True
        #batch.allElement_fps[element] = batch.allElement_fps[element].unsqueeze(0)
        #batch.dgdx[element] = batch.dgdx[element].unsqueeze(0)
        #batch.dEdg_AtomIndices[element] = batch.dEdg_AtomIndices[element].unsqueeze(0)
            #dgdx_XYZindices[element][i] = torch.tensor(dgdx_XYZindices[element][i])
            #print('dgdx after:', dgdx[element])
    #print("after normalization",atoms_fps)
    return batch

def normalizeParas(fprange):
    magnitudeScale = {}
    interceptScale = {}
    for element in fprange.keys():
        for i in range(len(fprange[element][0])):
            #avoid overflow
            if fprange[element][2][i] < 10.**-8:
               fprange[element][0][i] = -1.
               fprange[element][2][i] = 2.
        #magnitudeScale[element] = torch.from_numpy(2.0/fprange[element][2])
        #interceptScale[element] = torch.from_numpy(-2.0 * fprange[element][0] / fprange[element][2] - 1 )
        magnitudeScale[element] = 2.0/fprange[element][2]
        #print ('magnitude scale')
        #print (magnitudeScale[element])
        interceptScale[element] = -2.0 * fprange[element][0] / fprange[element][2] - 1
        #print ('intercept scale')
        #print (interceptScale[element])
    return fprange, magnitudeScale, interceptScale

def normalize(atoms_fps, fprange, dgdx, dEdg_AtomIndices, force_coefficient=None):

    normalize_matrix = {}
    magnitudeScale = {}
    interceptScale = {}
    elements = list(atoms_fps.keys())
    for element in elements:
        for i in range(len(fprange[element][0])):
            #avoid overflow
            if fprange[element][2][i] < 10.**-8:
                fprange[element][0][i] = -1.
                fprange[element][2][i] = 2.
        magnitudeScale[element] = torch.from_numpy(2.0/fprange[element][2])
        interceptScale[element] = torch.from_numpy(-2.0 * fprange[element][0] / fprange[element][2] - 1 )

    for element in elements:
        total_atoms = len(atoms_fps[element])
        mags = magnitudeScale[element].repeat(total_atoms,1)
        inters = interceptScale[element].repeat(total_atoms,1)
        atoms_fps[element] = (torch.mul(torch.tensor(atoms_fps[element]), mags) + inters).double()
        atoms_fps[element].requires_grad = True
        if force_coefficient > 1.e-5:
            dgdx[element] = torch.mul(torch.tensor(dgdx[element]),
                                      torch.flatten(mags)[dEdg_AtomIndices[element]])
            dgdx[element].requires_grad = True
            #dgdx_XYZindices[element][i] = torch.tensor(dgdx_XYZindices[element][i])
            #print('dgdx after:', dgdx[element])
    #print("after normalization",atoms_fps)
    return (atoms_fps, dgdx)

def generateInputs(fingerprintDB, elementFPs, 
                      keylist,
                     fingerprintDerDB=None):
      """
      This function generates the inputs to the tensorflow graph for the selected
      images.
      The essential problem is that each neural network is associated with a
      specific element type. Thus, atoms in each ASE image need to be sent to
      different networks.
 
      Inputs:
 
      fingerprintDB: a database of fingerprints, as taken from the descriptor
 
      elementFPs: a Ordered dictionary of number of fingerprints for each type of element (e.g. {'C':2,'O':5}, etc)
 
      keylist: a list of hashs into the fingerprintDB that we want to create
               inputs for
 
      fingerprintDerDB: a database of fingerprint derivatives, as taken from the
                        descriptor
 
      Outputs:
 
      allElement_fps: a dictionary of fingerprint inputs to each element's neural
          network
          Note: G_H_1 represent the 1st fingerprint centered on 'H'
                Assume we have g1 fingerprints that are centered on 'H',
                               g2 fingerprints that are centered on 'Pd'
          {'H':tensor([ [G_H_1,G_H_2,...,G_H_g1],  Atom 1  in Image 1
                        [G_H_1,G_H_2,...,G_H_g1],  Atom 2  in Image 1
                               ...
                        [G_H_1,G_H_2,...,G_H_g1],  Atom N1 in Image 1
 
                        [G_H_1,G_H_2,...,G_H_g1],  Atom 1  in Image 2
                               ...
                        [G_H_1,G_H_2,...,G_H_g1],  Atom N2 in Image 2
                               ...
                        [G_H_1,G_H_2,...,G_H_g1],  Atom NM in Image M
                       ])
           'Pd':tensor([[G_Pd_1,G_Pd_2,...,G_Pd_g2],  Atom 1  in Image 1
                        [G_Pd_1,G_Pd_2,...,G_Pd_g2],  Atom 2  in Image 1
                               ...             
                        [G_Pd_1,G_Pd_2,...,G_Pd_g2],  Atom N1 in Image 1
                                               
                        [G_Pd_1,G_Pd_2,...,G_Pd_g2],  Atom 1  in Image 2
                               ...             
                        [G_Pd_1,G_Pd_2,...,G_Pd_g2],  Atom N2 in Image 2
                               ...             
                        [G_Pd_1,G_Pd_2,...,G_Pd_g2],  Atom NM in Image M
                       ])
            }
      ntotalAtoms: the total number of atoms in the training batch
 
      dgdx: dictionary of fingerprint derivatives. Grouped by elements:          contrib. From      contrib. TO
           {'H': ([  [[G1x, G1y, G1z],[G2x, G2y, G2z]...,[Ggx, Ggy, Ggz]],        neighbor 1           atom 1
                       -----------     ------------       --------------
                          1st fp         2nd fp              gth fp 
                     [[G1x, G1y, G1z],[G2x, G2y, G2z]...,[Gmx, Gmy, Gmz]]         neighbor 2           atom 1
                                             ...                                     ...
                     [[G1x, G1y, G1z],[G2x, G2y, G2z]...,[Gmx, Gmy, Gmz]]         neighbor M1          atom 1
                     [[G1x, G1y, G1z],[G2x, G2y, G2z]...,[Gmx, Gmy, Gmz]]         neighbor 1           atom 2
                                             ...                                     ...
                     [[G1x, G1y, G1z],[G2x, G2y, G2z]...,[Gmx, Gmy, Gmz]]         neighbor M2          atom 2
                                              .                                       .                  .
                                              .                                       .                  .
                                              .                                       .                  .

                     [[G1x, G1y, G1z],[G2x, G2y, G2z]...,[Gmx, Gmy, Gmz]]         neighbor 1           atom N
                                             ...                                     ...
                     [[G1x, G1y, G1z],[G2x, G2y, G2z]...,[Gmx, Gmy, Gmz]]         neighbor MN          atom N
                         (Note: N is total number of 'element' atoms in training images)

                             ] ),
                          'Pd': ()
                         },
           }
      """
      elements = elementFPs.keys()
      allElement_fps = {}
      allAtomIndices = {} #store the location of atom in the whole tensor
      allfpIndices = {}   #store index of each fingerpint in allElement_fps
      natoms = []
      natomsPerimage = []
      fprange = {}
      fp_imageIndices = {}
      for element in elements:
          allElement_fps[element] = []
          fp_imageIndices[element] = []
      #print(allElement_fps.keys())
      tlocation = 0
      for j in range(len(keylist)):

          fp = fingerprintDB[keylist[j]]
          atomSymbols, fpdata = zip(*fp)
          nElement = {}
          allfpIndices[keylist[j]] = []
          allAtomIndices[keylist[j]] = []
          natom = len(atomSymbols)
          for i in range(len(atomSymbols)):
              allElement_fps[atomSymbols[i]].append(fpdata[i])
              currlocation = len(allElement_fps[atomSymbols[i]])
              allAtomIndices[keylist[j]].append(tlocation)
              allfpIndices[keylist[j]].append([index for index in range( \
                                    (currlocation-1) * elementFPs[atomSymbols[i]], \
                                     currlocation * elementFPs[atomSymbols[i]])
                                   ])
              fp_imageIndices[atomSymbols[i]].append([j])
              tlocation += 1
              if atomSymbols[i] not in nElement:
                  nElement[atomSymbols[i]] = 1
              else:
                  nElement[atomSymbols[i]] += 1
              natomsPerimage.append(natom)
          natoms.append(natom)


      natomsPerElement = {}
      ntotalAtoms = tlocation
      for element in elements:
          minv = np.min(allElement_fps[element], axis=0)
          maxv = np.max(allElement_fps[element], axis=0)
          fprange[element] = [minv, maxv, maxv-minv]
          print ('fprange in generate input')
          natomsPerElement[element] = len(allElement_fps[element])
      # Set up the array for atom-based fingerprint derivatives.
      dgdx = {}
      dgdx_XYZindices = {}
      force_AtomIndices = {}   #Used to sum forces over atoms
      dEdg_AtomIndices = {}   #Used to fetch dEdg to be used to multiply with dgdx tensor
      dict_init = {}
      if fingerprintDerDB is not None:
          for element in elements:
              dgdx[element] = []
              force_AtomIndices[element] = []
              dict_init[element] = [] 
              dEdg_AtomIndices[element] = []

          for j in range(len(keylist)):

              fp = fingerprintDB[keylist[j]]
              atomSymbols, fpdata = zip(*fp) #Fetch atomSymbols for each image

              fpDer = fingerprintDerDB[keylist[j]]

              #iterate over all atoms in the image
              #natom = natoms[keylist[j]]
              natom = natoms[j]
              dgdx_image = copy.deepcopy(dict_init)
              for wrtIndex in range(natom):     #make sure images ordered by element and in order of elementFPs
                  wrtSymbol = atomSymbols[wrtIndex]
                  #TODO: iterate over neighborlist only  
                  for centerIndex in range(natom):
                      dgdx_temp = copy.deepcopy(dict_init)
                      centerSymbol = atomSymbols[centerIndex]
                      for direction in range(3):
                          try:
                              dgdx_temp[centerSymbol].append(fpDer[(wrtIndex, wrtSymbol,centerIndex, centerSymbol, direction)])
                          except:
                              pass
                      #TODO: there may exist an atom with 0 neighbor.
                      if len(dgdx_temp[centerSymbol]) > 1:
                          dgdx[centerSymbol].append(np.array(dgdx_temp[centerSymbol]).T.tolist())
                          dEdg_AtomIndices[centerSymbol].append(allfpIndices[keylist[j]][centerIndex])
                          force_AtomIndices[centerSymbol].append([allAtomIndices[keylist[j]][wrtIndex]]*3)
          forceIndices = None
          for element in elements:
              dEdg_AtomIndices[element] = torch.tensor(dEdg_AtomIndices[element]).\
                                           resize_(len(dEdg_AtomIndices[element]), elementFPs[element], 1).\
                                           repeat(1,1,3)
              fp_imageIndices[element] = torch.tensor(fp_imageIndices[element])
              if forceIndices is None:
                  forceIndices = torch.tensor(force_AtomIndices[element])
              else:
                  forceIndices = torch.cat([forceIndices, torch.tensor(force_AtomIndices[element])])
          force_AtomIndices = forceIndices
      return (allElement_fps, fp_imageIndices, fprange, dgdx, dEdg_AtomIndices, force_AtomIndices,
              natomsPerElement, torch.tensor(natomsPerimage), torch.tensor(natoms), ntotalAtoms)

