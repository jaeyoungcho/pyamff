import numpy as np
import torch
import copy, math
from pyamff.fingerprints.fingerprintsWrapper import atomCenteredFPs
from .fileIO import loadData
from collections import OrderedDict
import random, time
import os
import pickle

class Dataset(torch.utils.data.Dataset):
  'Characterizes a dataset for PyTorch'
  def __init__(self, data, srcDir, indices):
        'Initialization'
        self.data = data
        self.srcDir = srcDir
        self.indices = indices

  def __len__(self):
        'Denotes the total number of samples'
        return len(self.indices)

  def __getitem__(self, index):
        'Generates one sample of data'
        # Select sample
        #print('fetch', self.indices[index])
        fname = os.path.join(self.srcDir,
                             'batches_{}.pckl'.format(self.indices[index]))
        with open(fname, 'rb') as f1:
            X = pickle.load(f1)
        #X = self.data[self.indices[index]]
        return X

class DataPartitioner(object):
    """
    Used to split data based on number of process
    srcData: a list of keys that used to store fp data
    """
    def __init__(self, srcData, fprange, magnitudeScale,
                 interceptScale, process_numb, fpDir=None, batch_numb=None, seed=1234, st=None):
       print('%.2fs: Entering dataPartition'%(time.time()-st))
       if isinstance(srcData, str):
         self.inmemory = True
         data = load_data(filename=srcData, rb='rb')
         self.fprange = data['fprange']
         self.fpData = data['fpData']
       if isinstance(srcData, list):
         self.inmemory = False
         self.fpData = srcData 
         self.fprange = fprange
         self.magnitudeScale = magnitudeScale
         self.interceptScale = interceptScale
       self.partitions = []
       if fpDir is None:
         self.srcDir = os.getcwd() +'/fingerprints'
       else:
         self.srcDir = fpDir
       self.batchDir = os.getcwd() +'/batches'
       if not os.path.exists(self.batchDir):
         os.mkdir(self.batchDir)
       #print ("BATCH NUMBER: ",batch_numb)
       if batch_numb:
          self.partitionData(batch_numb, seed)
       self.partitionBatches(process_numb, seed)
       print('%.2fs: Data partition done'%(time.time()-st))

    def partitionData(self, batch_numb, seed=1234):
       """
       Partition fp data to 'batch_numb' batches
       """
       batches = {}
       sizes = [1.0 / batch_numb for _ in range(batch_numb)]
       random.seed(seed)
       if self.inmemory:
         indexes = list(self.fpData.keys())
       else:
         indexes = copy.copy(self.fpData)
       data_len = len(indexes)
       random.shuffle(indexes)
       batchID = 0
       for frac in sizes:
           part_len = int(frac * data_len)
           #acfs = list(map(self.fpData.__getitem__, indexes[0:part_len]))
           if self.inmemory:
              batches[batchID]  =  self.preprocess(indexes[0:part_len], batchID)
           else:
              self.preprocess(indexes[0:part_len], batchID)
              batches[batchID] = batchID
           #batches[batchID] = batchGenerator(acfs)
           #self.partitions.append(indexes[0:part_len])
           indexes = indexes[part_len:]
           batchID+=1
           if frac == sizes[-1] and len(indexes) < part_len and len(indexes)>0:
              print("Warning batch size of one partition is smaller than others: %d vs %d"%(len(indexes), part_len), flush=True) 
       self.fpData = batches

    def preprocess(self, dataIDs, batchID):
        acfs = []
        for dataID in dataIDs:
            fname = os.path.join(self.srcDir, 'fps_{}.pckl'.format(dataID))
            with open(fname, 'rb') as f1:
                acf = pickle.load(f1)
                #acf.normalizeFPsList(self.fprange, self.magnitudeScale, self.interceptScale)
                acf.normalizeFPs(self.fprange, self.magnitudeScale, self.interceptScale)
                acfs.append(acf)
        batch = batchGenerator(acfs)
        fname = os.path.join(self.batchDir, 'batches_{}.pckl'.format(batchID))
        with open(fname, 'wb') as f1:
            pickle.dump(batch, f1)
        return batch

    #Partition data based on number of process
    def partitionBatches(self, process_numb, seed=1234):
       """
       Partition batches to 'process_numb' set
       """
       sizes = [1.0 / process_numb for _ in range(process_numb)]
       random.seed(seed)
       indexes = list(self.fpData.keys())
       data_len = len(indexes)
       random.shuffle(indexes)
       for frac in sizes:
           part_len = int(frac * data_len)
           self.partitions.append(indexes[0:part_len])
           indexes = indexes[part_len:]

    def use(self, rank):
        return Dataset(self.fpData, self.batchDir, self.partitions[rank])

#collate_fn: imagesFPs: a list of atomsCenteredFps
def batchGenerator(acfs):
    st = time.time()
    if len(acfs) == 1:
       #print (acfs)
       #print (acfs[0])
       batch = atomCenteredFPs()
       batch.stackFPs(acfs)
       return batch
    batch = atomCenteredFPs()
    #print("Batch: ",batch)
    #print ('ACFS: ',acfs)
    batch.stackFPs(acfs)
    #print("After Batch: ",batch)
    #batch.stackFPsList(acfs)
    #print(' BatchingTIMEUSED:', time.time()-st)
    return batch

