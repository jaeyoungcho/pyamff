#!/usr/bin/env python3

from pyamff.config import ConfigClass
from ase.io import Trajectory
from pyamff.utilities.preprocessor import normalize, fetchProp
from pyamff.neighborlist import NeighborLists
from pyamff.mlModels.pytorchNN import NeuralNetwork
from pyamff.fingerprints.behlerParrinello import represent_BP
from pyamff.utilities.dataPartition import Dataset, batchGenerator, DataPartitioner
from pyamff.utilities.logTool import setLogger, writeSysInfo
from torch.multiprocessing import Process
from pyamff.training import Trainer
from pyamff.utilities import fileIO as io
from pyamff.fingerprints.fingerprints import Fingerprints
from collections import OrderedDict
import torch.distributed as dist
import os, sys, time, glob
import torch
import torch.multiprocessing as mp

logger = setLogger()

def init_processes(rank, size, thread, fn, partition,batch_number,  maxepoch,device, backend='gloo'):
    #Initialize the distributed environment.
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = '12355'
    #os.environ['MASTER_ADDR'] = master_addr
    #os.environ['MASTER_PORT'] = master_port
    #print('lauching', rank)
    dist.init_process_group(backend, rank=rank, world_size=size)
    #fn(rank, size)
    fn(rank, size, thread, partition, batch_number, maxepoch, device, logger=logger)

def main():

    # Read in parameters

    st = time.time()
    writeSysInfo(logger)
    cwd = os.getcwd()
    logger.info('=======================================================')
    logger.info('Starting a PyAMFF job at %s', time.strftime('%X %x %Z'))
    logger.info('=======================================================')
    logger.info('Reading inputs from %s', cwd+'/config.ini')
    print('=====================')
    print('Starting a PyAMFF job')
    print('=====================')
    print('Reading inputs')
    config = ConfigClass()
    config.initialize()
    fp_paras = config.config['fp_paras'].fp_paras
    nFPs = {}
    for key in fp_paras.keys():
        nFPs[key] = len(fp_paras[key])

    # Read in images

    logger.info('Reading training images from %s'%cwd+'/'+config.config['trajectory_file'])

    print('%.2fs: Reading training images'%(time.time()-st))
    if os.path.isfile(config.config['trajectory_file']):
        images = Trajectory(config.config['trajectory_file'], 'r')
    else:
        print("Trajectory file %s does not exist" % config.config['trajectory_file'], sys.stderr)
        sys.exit(2)

    # Preprocess and check properties and images

    logger.info('Checking and preprocessing training images')
    print('%.2fs: Checking and preprocessing training images'%(time.time()-st))
    uniq_elements=config.config['fp_paras'].uniq_elements
    if config.config['use_cohesive_energy']:
        coeh = config.config['fp_paras'].refEs
        refEs = OrderedDict(zip(uniq_elements, coeh))
    else:
        refEs = None
    trainingimages, properties, slope, intercept = fetchProp(images, refEs=refEs, forceTraining=config.config['force_training'])
    nimages = len(trainingimages)
    logger.info('  Total number of traning images: %d', nimages)

    # Get neighborlists

    srcData = list(trainingimages.keys())
    useexisting = config.config['fp_use_existing']
    if useexisting:
        logger.info('Using pre-calculated fingerprints from %s', cwd+'/'+config.config['fp_file'])
        print('%.2fs: Using pre-calculated fingerprints'%(time.time()-st))
        fpDir = config.config['fp_file']
        #nfpFiles = len(glob.glob(os.path.join(os.getcwd(), 'fingerprints/*')))
        nfpFiles = len(glob.glob(os.path.join(fpDir, '*')))
        if nfpFiles != nimages:
            print(" Not enough fingerprint files: %d expected but only %d provided" % (nimages, nfpFiles), sys.stderr)
            sys.exit(2)
    else:
        fpDir = None

    if config.config['fp_engine'] == 'Fortran':
        print('%.2fs: Calculating fingerprints'%(time.time()-st))
        fpcalc = Fingerprints(uniq_elements=config.config['fp_paras'].uniq_elements, filename=config.config['fp_parameter_file'], nfps=nFPs)
        fprange, magnitudeScale, interceptScale = fpcalc.loop_images(nFPs, config.config['fp_batch_num'], trainingimages, properties,
                                   normalize=True, logger=logger, fpDir=fpDir, useexisting=useexisting)
    else:
        logger.info('Calculating neighborlists')
        fp_st = time.time()
        nl = NeighborLists(cutoff = 6.0)
        nl.calculate(trainingimages, fortran=True)
        et = time.time()
        logger.info('  Time used: %.2fs', (et-fp_st))

        # Calculate fingerpints

        logger.info('Calculating fingerprints')
        acfs, fprange = represent_BP(nl, nFPs, trainingimages, properties,
                                   G_paras=fp_paras, fpfilename=srcData,
                                   fortran=True, logger=logger)
        #fingerprints, fingerprintsDer = represent_BP(nl, G_paras=fp_paras, fortran=True)
        logger.info('  Time used: %.2fs', (time.time()-et))
    if config.config['run_type']=='fingerprints':
        logger.info('Fingerprint calculation done.')
        print('Fingerprint calculation done.')
        sys.exit()
    params = None
    if config.config['init_model_parameters']:
        weights = {}
        bias = {}
        params = []

        l1_weight = [[-6.29343251e-02, -1.81609897e-02], [-1.13921890e-01, -1.92165552e-01]]
        l1_bias = [-6.02987072e-02, 2.38641264e-02]
        l2_weight = [[-2.64460289e-02, -1.65831516e-01], [7.34679286e-02, 2.04791646e-01]]
        l2_bias = [-1.96539788e-01, -8.66611724e-02]
        l3_weight = [[7.65615210e-02, -1.31135597e-01]]
        l3_bias = [-1.13299330e-01]

        l1_weight1 =  [[-2.31912084e-01, 3.42103536e-01, 2.60925378e-01], [1.82964682e-01, -5.71268008e-01, -5.67102883e-01]]
        l1_bias1   =  [-2.04090957e-01, 1.85009592e-01]
        l2_weight1 =  [[-3.64072220e-01, 4.79076873e-01], [5.85580412e-01, -6.64089070e-01]]
        l2_bias1   =  [1.90364738e-01, -3.90105786e-01]
        l3_weight1 =  [[-2.92259273e-01, 5.29267594e-01]]
        l3_bias1   =  [1.96972082e-02]

        """
        l1_weight = [[-0.06245638, -0.02177071], [ 0.01106360, -0.02641876]]
        l1_bias   = [-0.15927899, 0.08015668]
        l2_weight = [[-0.01062437, -0.00506942],[0.00031429, -0.12331749]]
        l2_bias   = [0.18475361, 0.17465138]
        l3_weight = [[-0.10928684, 0.02496535]]
        l3_bias   = [0.12033310]

        l1_weight1 = [[-0.12125415, -0.09574964], [-0.12175528,-0.01690174]]
        l1_bias1   = [-0.00525357, 0.16651967]
        l2_weight1 = [[0.14360507, -0.03717202],[0.18517374,-0.01645058]]
        l2_bias1   = [0.03095685, -0.14551263]
        l3_weight1 = [[-0.12728526, 0.03061981]]
        l3_bias1   = [-0.00860174]
        """
        params = [l1_weight, l1_bias, l2_weight, l2_bias, l3_weight, l3_bias,
                  l1_weight1, l1_bias1, l2_weight1, l2_bias1, l3_weight1, l3_bias1]

    # Define the NN model

    logger.info('Defining machine-learning model')
    print('%.2fs: Defining machine-learning model'%(time.time()-st))
    losstol = config.config['force_coefficient'] * (config.config['force_tol'] * nimages) ** 2 \
             + config.config['energy_coefficient'] * (config.config['energy_tol'] * nimages) ** 2
    if config.config['model_type'] == 'neural_network' and config.config['restart'] == False:
        logger.info('  Model type: neural_network')
        logger.info('  Model structure: %s', ' '.join([str(x) for x in config.config['hidden_layers']]))
        model = NeuralNetwork(
                  hiddenlayers=config.config['hidden_layers'],
                  activation=config.config['activation_function'],
                  nFPs=nFPs,
                  forceTraining=config.config['force_training'],
                  cohE = config.config['use_cohesive_energy'],
                  # TODO: load pretrained params
                  params=params,
                  slope=slope
                  )
    if config.config['model_type'] == 'neural_network' and config.config['restart'] == True:
        logger.info('  Model type: neural_network')
        logger.info('  Model structure: %s', ' '.join([str(x) for x in config.config['hidden_layers']]))
        logger.info('  Creating model from saved potential' )
        loaded = torch.load(config.config['model_path'])
        modelParameters = loaded['Modelparameters']
        model = NeuralNetwork(
                 hiddenlayers=modelParameters['hiddenlayers'],
                 nFPs=modelParameters['nFPs'],
                 forceTraining=modelParameters['forceTraining'],
                 slope=modelParameters['slope'],
                 activation=modelParameters['activation']
                )
        model.load_state_dict(loaded['state_dict'])

    logger.info('  Energy coefficient: %f', config.config['energy_coefficient'])
    logger.info('  Force coefficent:   %f', config.config['force_coefficient'])
    logger.info('  Energy tolerance:   %f', config.config['energy_tol'])
    logger.info('  Force tolerance:    %f', config.config['force_tol'])
    logger.info('  Loss tolerance:     %f', losstol)
    model.share_memory()
    logger.info('Parallelization setup:')
    process_number = config.config['process_num']
    batch_number = config.config['batch_num']
    tbatches = process_number * batch_number
    #if config.config['dynamic']:
    #    tbatches = None
    #    minibatch_number = batch_number
    #    logger.info('  Batch collation mode: Dynamic')
    #else:
    #    minibatch_number = 1
    #    logger.info('  Batch collation mode: Static')
    #minibatch_number = batch_number
    logger.info('  Total number of batches:       %d', tbatches)
    processes = []
    #partition_sizes = [1.0 / size for _ in range(size)]
    #partitions = DataPartitioner(datafilename="fps.pckl", sizes =partition_sizes, seed=1234)
    print('%.2fs: Partition data'%(time.time()-st))
    partitions = DataPartitioner(srcData=srcData,
                                 fprange=fprange,
                                 magnitudeScale=magnitudeScale,
                                 interceptScale=interceptScale,
                                 process_numb=process_number,
                                 fpDir=fpDir,
                                 batch_numb=tbatches,
                                 seed=1234,
                                 st=st)
    fprange = partitions.fprange
    logger.info('  Number of processes:           %d', process_number)
    logger.info('  Number of batches per process: %d', batch_number)

    # Define the pyamff training 

    logger.info('Setup PyAMFF trainer:')
    print('%.2fs: Setup PyAMFF trainer:'%(time.time()-st))
    calc = Trainer(model=model,
                  optimizer=config.config['optimizer_type'],
                  # TODO: check it can be reloaded
                  fpParas=config.config['fp_paras'],
                  energy_coefficient=config.config['energy_coefficient'],
                  force_coefficient=config.config['force_coefficient'],
                  lossConvergence=losstol,
                  energyRMSEtol=config.config['energy_tol'],
                  forceRMSEtol=config.config['force_tol'],
                  lossgradtol=config.config['loss_grad_tol'],
                  #TODO
                  intercept=intercept,
                  learningRate=config.config['learning_rate'],
                  model_logfile='pyamff.pt',
                  logmodel_interval=100,
                  debug=None,
                  weight_decay=config.config['weight_decay'],
                  fprange=fprange,
                  nimages=nimages)

    # Train the NN

    logger.info('=======================================================')
    logger.info('Starting training')
    head = "{:>12s} {:>12s} {:>12s} {:>12s}".format('epoch', 'lossValue', 'EnergyRMSE', 'ForceRMSE')
    logger.info('%s', head)
    """
    ranks_per_node = 12

    fin = open('nodes','r')
    mastaddr = []
    mastips = []
    for line in fin.readlines():
        mastaddr.append(line.split()[0].split('@')[1].strip('').split('.local')[0])
        mastips.append('')
    print(mastaddr)
    fin.close()

    fhost =open('hosts', 'r')
    for line in fhost.readlines():
       print('line', line)
       try:
           fields = line.split('.local')[0].split()
           # print(fields[1].strip())
           for i in range(len(mastaddr)):
               if fields[1].strip() == mastaddr[i]:
                   print('found',mastaddr[i], fields[0])
                   mastips[i]=fields[0].strip()
       except:
           pass
    print(mastips)
    """
    #for node in range(2):
    #  print(mastaddr[node], mastips[node])
    #  for rank in range(int(process_number/2)):
    useCuda = False

    if config.config['device_type'] == 'GPU' and torch.cuda.is_available():
        useCuda = True

    device = torch.device("cuda:0" if useCuda else "cpu")

    print("%.2fs: Training start"%(time.time()-st))
    train_st = time.time()
    mp.set_start_method(config.config['mp_start_method'])
    for rank in range(process_number):
    #      local_rank = rank
    #      rank = ranks_per_node * node + local_rank
          #print('rank', rank)

    #      mp.spawn(calc.parallelFit, nprocs=process_number, args=(process_number, config.config['thread_num'],
    #                                                                 partitions.use(rank),
    #                                                                 batch_number, config.config['epochs_max'],
    #                                                                 device, logger),join=True)
          p = Process(target=init_processes,
                      args=(rank, process_number, config.config['thread_num'],
                            #config.config['master_addr'], 
                            #mastips[node],
                            #config.config['master_port'],
                            calc.parallelFit, partitions.use(rank),
                            #config.config['batch_number'],
                            batch_number,
                            config.config['epochs_max'],
                            device
                            ))
          p.start()
          processes.append(p)
    for p in processes:
        p.join()
    et = time.time()
    print("%.2fs: Training done, time used: %.2fs" % (et-st,et-train_st))

if __name__ == "__main__":
    main()
