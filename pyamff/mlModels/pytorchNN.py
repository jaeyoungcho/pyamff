#from . import generateTensorFlowArrays
#from .. import FileDatabase
from collections import OrderedDict
import numpy as np

import torch
from torch.utils import data
from scipy.stats import truncnorm
import torch.nn as nn
from torch.nn import Linear
from pyamff.mlModels.lossFunctions import LossFunction
from pyamff.utilities.truncatedNormal import TruncatedNormal
from pyamff.utilities.fileIO import saveData
#from mlModels.linear import Linear

import time
import sys
import copy


class NeuralNetwork(nn.Module):
    """
    hiddenlayers: define the structure of the neural network.
                 i.e. (2,3) define a neural network with two hidden layers,
                            containing 2 and 3 neurons, respectively
    nFPs: a dictionary that defines number of fingerprints for each element.
          i.e. {'H': 3, 'Pd':2}: 3 and 2 fingerprints for 'H' and 'Pd', respectively
    maxEpochs: maximum epochs the training process will take
    energy_coefficient: the weight of energy in the loss function
    force_coefficient:  the weight of force in the loss function
    params: a list or array of parameters will be used to initilize the NN model
          i.e., [ #params for 'H': fp1: fingerprint 1, n1: neuron 1 in the corresponding layer
                  [[fp1_n1, fp2_n1, fp3_n1], [fp1_n2, fp2_n2, fp3_n2]],   #weights connecting inputlayer to hiddenlayer 1
                  [ bias_n1,                  bias_n2],                   #bias on hiddenlayer 1
                  [[n1_n1, n2_n1], [n1_n2, n2_n2], [n1_n3, n2_n3]],       #weights connecting hiddenlayer 1 to hiddenlayer 2
                  [ bias_n1,       bias_n2,         bias_n3],             #bias on hiddenlayer 2
                  [[n1_n1, n2_n1, n3_n1]],                                #weights connecting hiddenlayer 1 to outputlayer
                  [ bias_n1],                                             #bias on outputlayer 

                  #params for 'Pd': fp1: fingerprint 1, n1: neuron 1 in the corresponding layer
                  [[fp1_n1, fp2_n1], [fp1_n2, fp2_n2]],                   #weights connecting inputlayer to hiddenlayer 1
                  [ bias_n1,          bias_n2],                           #bias on hiddenlayer 1
                  [[n1_n1, n2_n1], [n1_n2, n2_n2], [n1_n3, n2_n3]],       #weights connecting hiddenlayer 1 to hiddenlayer 2
                  [ bias_n1,       bias_n2,         bias_n3],             #bias on hiddenlayer 2
                  [[n1_n1, n2_n1, n3_n1]],                                #weights connecting hiddenlayer 1 to outputlayer
                  [ bias_n1],                                             #bias on outputlayer 
                ]
   """
    def __init__(self,
                hiddenlayers = (5, 5),
                nFPs = {'Au': 10, 'H':5},
                activation='sigmoid',
                forceTraining = True,
                cohE = False,
                params = None,
                scaling = None,
                slope = None,
                ):
        super().__init__()
        self.hiddenlayers = hiddenlayers
        self.activation = activation
        self.n_layers = len(hiddenlayers) + 2
        if activation == 'sigmoid':
           self.actF = nn.Sigmoid()
        if activation == 'relu':
           self.actF = nn.ReLU()
        if activation == 'tanh':
           self.actF = nn.Tanh()
        if activation == 'softplus':
           self.actF = nn.Softplus()
        self.nFPs = nFPs
        self.elements = np.array([element for element in nFPs.keys()])
        self.nn_models = {}
        self.hd_names = {}
        self.model_params = []
        self.model_namedparams = []
        for element, n_Gs in self.nFPs.items():
            self.hd_names[element] = []
            self.atomModel(n_Gs, element)
        #    self.model_params += list(self.nn_models[element].parameters())
        #    self.model_namedparams += list(self.nn_models[element].named_parameters())

        self.nn_models = nn.ModuleDict(self.nn_models)
        self.setParams(params)
        for element in self.elements:
            self.nn_models[element].double()
        #print('  paras',list(self.nn_models.parameters()))
        self.forceTraining = forceTraining
        self.cohE = cohE
        self.slope = slope

        self.imageIndices = None
        self.nimages = None
        self.fp_d = None
        self.dEdg_AtomIndices = None
        self.force_AtomIndices = None
        self.natomsPerElement = None
        self.ntotalAtoms = None

    """
    atomModel: define the NN model for each atom.
    It will generate a model dictionary with 'element' as the key and a inner dictionary with the name
    of each layer as the key
    """

    def setParams(self, params):
        i=0
        # mean, stddev, lowerbound, upbound
        tn = TruncatedNormal(torch.Tensor([0.0]), torch.Tensor([0.1]), torch.Tensor([-0.2]), torch.Tensor([0.2]))
        if params is not None:
            for param in self.parameters():
                param.data = torch.tensor(params[i]).double()
                #print('shape', param.data.shape)
                i+=1
        else:
            #lower, upper = 3.5, 6
            #mu, sigma = 5, 0.7
            #X = stats.truncnorm(
            #    (lower - mu) / sigma, (upper - mu) / sigma, loc=mu, scale=sigma)
            for param in self.parameters(): 
                #param.data = torch.tensor(truncnorm.rvs(-10, 10, loc=0.0, scale=0.1, size=list(param.data.shape)))
                param.data = tn.rsample(param.data.shape)

    def atomModel(self, n_Gs, element):
        self.nn_models[element] = nn.ModuleDict({'inputLayer':Linear(n_Gs, self.hiddenlayers[0]).double()})
        for i in range(len(self.hiddenlayers)-1):
            self.hd_names[element].append('hiddenLayer_'+str(i+1))
            self.nn_models[element][self.hd_names[element][i]] = \
                   Linear(self.hiddenlayers[i], self.hiddenlayers[i+1]).double()
        self.nn_models[element]['outputLayer'] = Linear(self.hiddenlayers[-1], 1).double()

    def set(self, imageIndices,nimages,
        dEdg_AtomIndices,force_AtomIndices,
        natomsPerElement, ntotalAtoms, slope=None):

        self.imageIndices = imageIndices
        self.nimages = nimages
        self.dEdg_AtomIndices = dEdg_AtomIndices
        self.force_AtomIndices = force_AtomIndices
        self.natomsPerElement = natomsPerElement
        self.ntotalAtoms = ntotalAtoms
        if self.slope is None:
            self.slope = slope


    """
    forward function: define the operation that acts on each layer of neural network
    fps: list of tensor of fingerprints for one image with requires_grad=True
         for each tensor, must have requires_grad=True to get gradient
         {'H':tensor([ [G1,G2,...,Gg],  Atom 1  in Image 1
                       [G1,G2,...,Gg],  Atom 2  in Image 1
                              ...
                       [G1,G2,...,Gg],  Atom N1 in Image 1
                       [G1,G2,...,Gg],  Atom 1  in Image 2
                              ...
                       [G1,G2,...,Gg],  Atom N2 in Image 2
                              ...
                       [G1,G2,...,Gg],  Atom NM in Image M
                      ])
          'Pd': ...}
    imageIndices: used to sum up energy for each image over atoms
    nimages: number of images in the training batch
    fp_d: derivative of fingeprints
          format: refer to dgdx in function 'generateInputs()'
    dEdg_AtomIndices: used to gather dEdg for forces calculations
    force_AtomIndices: used to sum up force for each atom over neighbor atoms 
    natomsPerElement: total number of atoms of each type of element.
            {'H':4,'Pd':26}
    ntotalAtoms: total number of atoms in the training batch
    """

    def forward(self, fps, fp_d, batch, device, logger=None, debug=False):
    #def forward(self, batch):
        #fps = batch.allElement_fps
        #fp_d = batch.dgdx
        #for params in self.nn_models.parameters():
           #print(params)
           #break
        self.imageIndices = batch.fp_imageIndices
        self.nimages = batch.nimages

        #self.fp_d = fp_d
        self.dEdg_AtomIndices = batch.dEdg_AtomIndices

        self.force_AtomIndices = batch.force_AtomIndices
        self.force_AtomIndices = self.force_AtomIndices.to(device)

        self.natomsPerElement = batch.natomsPerElement

        self.ntotalAtoms = batch.ntotalAtoms

        energies = torch.tensor([[0.0]] * self.nimages,device=device).double()  # Initialize energies
        forces = torch.tensor([], dtype=torch.double,device=device)
        for element in self.elements:
            fp = fps[element]
            #x = torch.sigmoid(self.nn_models[element]['inputLayer'](fp))
            x = self.actF(self.nn_models[element]['inputLayer'](fp))
            x = x.to(device)
            if debug == True:
                hl_count = 0
                dead_hid = {}
                dead_hid['element'] = element
                dead_in = 0

                if self.activation == 'sigmoid':
                    dead_in += np.count_nonzero( (x.detach().numpy() < 0.001) )
                    dead_in += np.count_nonzero((0.999 < x.detach().numpy())  )
                    dead_hid[hl_count] = (dead_in, x.numel())
                    hl_count += 1
                if self.activation == 'tanh':
                    dead_in += np.count_nonzero( (x.detach().numpy() < -0.999) )
                    dead_in += np.count_nonzero((0.999 < x.detach().numpy())  )
                    dead_hid[hl_count] = (dead_in, x.numel())
                    hl_count += 1
                if self.activation == 'relu' or self.activation == 'softplus':
                    dead_in += np.count_nonzero( (x.detach().numpy() < 0.0) )
                    dead_hid[hl_count] = (dead_in, x.numel())
                    hl_count += 1

            for hd_name in self.hd_names[element]:
                dead_tmp = 0
                #x = torch.sigmoid(self.nn_models[element][hd_name](x))
                x = self.actF(self.nn_models[element][hd_name](x))
                x = x.to(device)
                if debug == True:
                    if self.activation == 'sigmoid':
                        dead_tmp += np.count_nonzero( (x.detach().numpy() < 0.001) )
                        dead_tmp += np.count_nonzero((0.999 < x.detach().numpy())  )
                    if self.activation == 'tanh':
                        dead_tmp += np.count_nonzero( (x.detach().numpy() < -0.999) )
                        dead_tmp += np.count_nonzero((0.999 < x.detach().numpy())  )
                    if self.activation == 'relu' or self.activation == 'softplus':
                        dead_in += np.count_nonzero( (x.detach().numpy() < 0.0) )
                        dead_hid[hl_count] = (dead_in, x.numel())

                    dead_hid[hl_count] = (dead_tmp, x.numel())
                    hl_count += 1

            x=self.nn_models[element]['outputLayer'](x)
            x = x.to(device)
            if debug == True:
                logger.info('%s', "  ".join("{}".format(v) for k,v in dead_hid.items()))
            self.imageIndices[element] = self.imageIndices[element].to(device)
            energies = torch.add(energies, torch.zeros(self.nimages,1,device=device).double().scatter_add_(0, self.imageIndices[element], x))

            # TODO: training in batchs, it maybe better to define torch.tensor() ahead of time
            #x.backward(torch.tensor([[1.0]]*self.natomsPerElement[element]), retain_graph=True) 
            dedg, = torch.autograd.grad(energies, fp, 
                                 grad_outputs=energies.data.new(energies.shape).fill_(1.0),
                                 retain_graph=True,
                                 create_graph=True)
            currforces = torch.sum(torch.mul(fp_d[element],
                                      torch.flatten(dedg)[self.dEdg_AtomIndices[element]]), 1)
            forces = torch.cat([forces, currforces])
        energies = torch.flatten(torch.mul(energies, self.slope))
        #energies = energies * self.slope
        #print('noSumForce:', forces)
        #print("self.force_AtomIndices", self.force_AtomIndices.numpy())
        #save_data(forces, filename='forces.pl')
        #save_data(self.force_AtomIndices, filename='forceAtomInd.new')
        #print("self.ntotalAtoms", self.ntotalAtoms)
        forces = torch.mul(
                  torch.zeros([self.ntotalAtoms, 3], dtype=torch.double, device=device).\
                  scatter_add_(0, self.force_AtomIndices, forces), -self.slope)
        #forces = torch.flatten(torch.mul(
        #                        torch.zeros([self.ntotalAtoms, 3], dtype=torch.double).\
        #                        scatter_add_(0,self.force_AtomIndices, forces), -self.slope))
        #print("E:",energies)
        #print("Fs",forces)
        #print("",torch.div(forces, torch.reshape(batch.natomsPerimageForce, (self.ntotalAtoms, 1)).repeat(1,3)))
        if self.cohE:
          forces = torch.mul(forces, batch.natomsPerimageFxyz)
        return energies, forces  #, dead_hid

    def parametersDict(self):
        Modelparameters = {}
        Modelparameters['hiddenlayers'] = self.hiddenlayers
        Modelparameters['nFPs'] = self.nFPs
        Modelparameters['slope'] = self.slope
        Modelparameters['forceTraining'] = self.forceTraining
        Modelparameters['activation'] = self.activation
        return Modelparameters

