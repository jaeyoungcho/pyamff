import torch.nn as nn
import torch

class LossFunction(nn.Module):
    def __init__(self, force_coefficient=0.05, device=torch.device("cpu")):
        super().__init__()
        self.force_coefficient = force_coefficient
        self.energyloss = None
        self.forceloss  = None
        self.device = device
        #print("constructing loss")
        #self.time = 0.0
        #self.nstep = 0.0
    """
    inputs: [energies, forces]
          energies: tensor([E_1, E_2, ..., E_N])
          forces: tensor([f_1, f_2, ..., f_3N])
          N is the total number of atoms in the tranining set
    target: [energy_reference, forces_reference]
    """
    """
    def forward(self, inputs, targets, ntotalAtoms):
        #starttime = time.time()
        #with torch.autograd.profiler.profile() as prof:
        #print("inputs",inputs[0:4])
        #print("targets",targets[0:4])
        energyloss =  torch.sum(torch.pow(torch.div(torch.sub(inputs[0:4],targets[0:4]), ntotalAtoms),2.))
        #energyloss =  torch.sum(torch.pow(torch.div(torch.sub(input1,target1), ntotalAtoms),2.))
        forceloss = torch.mul(
                torch.sum(torch.div(torch.pow(
                   torch.sub(inputs[4:], targets[4:]),2.0), ntotalAtoms
                   )), self.force_coefficient)
        #forceloss = torch.mul(
        #        torch.sum(torch.div(torch.pow(
        #           torch.sub(inputs, target),2.0), ntotalAtoms
        #           )), self.force_coefficient)
        #print(energyloss, forceloss)
         # with torch.autograd.profiler.record_function("label-loss"):
         #   loss += 0.0
        #self.time+= time.time()-starttime
        #self.nstep+=1
        loss = energyloss + forceloss
        #loss = forceloss
        return loss
    """

    def forward(self, input1, input2, target1, target2, natomsEnergy, natomsForce):
        input1 = input1.to(self.device)
        input2 = input2.to(self.device)
        target1 = target1.to(self.device)
        target2 = target2.to(self.device)
        natomsEnergy = natomsEnergy.to(self.device)
        natomsForce = natomsForce.to(self.device)

        self.energyloss =  torch.sum(torch.pow(torch.div(torch.sub(input1,target1), natomsEnergy),2.))
        self.forceloss = torch.sum(torch.div(torch.sum(torch.pow(
                 torch.sub(input2, target2),2.0), dim=1), natomsForce
                 )) / 3.0
        loss = self.energyloss + torch.mul(self.forceloss, self.force_coefficient)
        return loss
