import numpy as np
import pickle
import math
import os, sys
import time
import torch
from ase.formula import Formula
from pyamff.fingerprints.fingerprintsWrapper import atomCenteredFPs
from pyamff.utilities import fileIO as io
from pyamff.utilities.preprocessor import normalizeParas
from collections import OrderedDict
import itertools
import tempfile
import copy

try:
    from pyamff import fmodules
    FMODULES = True
except:
    FMODULES = False


class Fingerprints():

    """
    An implementation of the Behler-Parrinello descriptors.

    References
    ----------
    Behler, J; Parrinello, M. Generalized Neural-Network Representation of
    High-Dimensional Potential-Energy Surfaces. Phys. Rev. Lett. 98, 146401.

    """


    def __init__(self, uniq_elements, filename='fpParas.dat', nfps=None):
        self.filename = filename
        self.nfps = nfps
        self.uniq_elements = uniq_elements
        #print('uniq', uniq_elements)
        self.nelement = len(self.uniq_elements)
        self.max_nfps = max(nfps.values())
        self.forceEngine = 0
        self.max_neighs = 100
        nelement = len(nfps.keys())
        self.coef =  np.zeros(nelement, order='F')
        self.coef = fmodules.fpcalc.read_fpparas(self.filename, nelement)

    def toIndex(self, symbols, nAtoms):
        elementDict = dict(zip(self.uniq_elements,range(1, self.nelement+1)))
        nsymbols = np.zeros(nAtoms,dtype=np.dtype('i4'))
        for i in range(nAtoms):
            try:
                nsymbols[i] = elementDict[symbols[i]]
            except:
                sys.stderr.write('Element %s has no fingerprints defined' % (symbols[i]))
                sys.exit(2)
        return nsymbols

    def calcFPs(self, atoms, chemsymbols):
        fingerprints = []
        fingerprintprimes = {}
        nAtoms = len(atoms)
        symbols = self.toIndex(chemsymbols, nAtoms)
        pos_car = atoms.get_positions()
        #cell = atoms.cell.array.astype(np.double, order='F')
        cell = atoms.cell.array
        fps =  np.zeros([nAtoms, self.max_nfps], order='F')
        dfps = np.zeros([nAtoms, self.max_neighs, 3, self.max_nfps], order='F')
        neighs = np.zeros([nAtoms, self.max_neighs], order='F')
        num_neigh = np.zeros(nAtoms, dtype=np.dtype('i4'))

        #print('fmodules:', [name for name in dir(fmodules) if not name.startswith("__")])
        fps, dfps, neighs, num_neigh = fmodules.fpcalc.calcfps(pos_car, cell, symbols, self.max_nfps, len(self.uniq_elements), self.forceEngine)
        #store fps and dfps in a dictionary
        for i in range(0, nAtoms):
            wrtsymbol = chemsymbols[i]
            #fingerprints.append((chemsymbols[i], fps[i][:self.nfps[wrtsymbol]]))
            for j in range(0,3):
                for k in range(0, num_neigh[i]+1):
                    if k == 0:
                        centerIndex  = i
                        centersymbol = wrtsymbol
                        #fingerprintprimes[(i, chemsymbols[i], i, chemsymbols[i], j)] = dfps[i,k,j,:]
                    else:
                        centerIndex = neighs[i, k-1]-1
                        centersymbol = chemsymbols[centerIndex]
                    fingerprintprimes[(i, wrtsymbol, centerIndex, centersymbol,j)] =  dfps[i,k,j,:self.nfps[centersymbol]]
        #print('fps ', fingerprints)
        #print('dfps')
        #for key in fingerprintprimes.keys():
        #   print(key, fingerprintprimes[key])
        return fps, fingerprintprimes

    #from memory_profiler import profile
    #@profile
    def loop_images(self, nFPs, num_batch, trainingimages, properties,
                    normalize, logger, fpDir=None, useexisting=False):
        print("Fingerprints start")
        #import tracemalloc 
        #tracemalloc.start()
        #snap1 = tracemalloc.take_snapshot()
        if fpDir is None:
            fpsdir = os.getcwd() + '/fingerprints'
        else:
            fpsdir = fpDir
        if not os.path.exists(fpsdir):
            os.mkdir(fpsdir)
        fpDb = {}
        fpDerDb = {}
        fpData = OrderedDict() 
        fpData_temp = {}
        fprange = {}
        minv = {}
        maxv = {}
        keybatch = []
        #for ele in nFPs.keys():
        #    aEfps[ele] = []
        fptime = 0
        for struct in trainingimages.keys():
            if logger and struct % 20 == 0:
                logger.info('  Calculating FPs for image %d', struct)
            # get FPs and FPprimes for each structure
            st = time.time()
            #print('struct', struct)
            if not useexisting:
                chemsymbols = trainingimages[struct].get_chemical_symbols()
                fingerprints, fingerprintprimes = self.calcFPs(trainingimages[struct], chemsymbols)
                et = time.time()
                fptime += et-st
                acf = atomCenteredFPs()

                # For pyamff Calculator
                if properties is None:
                    p1 = None
                    acf.sortFPs(fpDb, nFPs, p1, [struct], fpDerDb, batch=False)
                    return acf

                # store FPs and FPprimes as acf objects and make readable by pytorch/fortran machine learning
                #acf.sortFPs(fpDb, nFPs, p1, [struct], fpDerDb)
                acf.sortFPs(chemsymbols, fingerprints, nFPs, properties, [struct], fingerprintprimes)
                #acf.sortFPsList(chemsymbols, fingerprints, nFPs, properties[struct], fingerprintprimes)

                fpData[struct] = acf 
                #print(acf.allElement_fps)
                if struct%num_batch == 0 or struct == len(trainingimages)-1:
                    for key in fpData.keys():
                        f_name = fpsdir+'/fps_{}.pckl'.format(key)
                        with open(f_name, 'wb') as f:
                            pickle.dump(fpData[key],f)
                    fpData = {}

            else:
                #print('load existing')
                fname = os.path.join(fpsdir, 'fps_{}.pckl'.format(struct))
                with open(fname, 'rb') as f1:
                    acf = pickle.load(f1)

            if normalize:
                for k,v in acf.allElement_fps.items():
                    if k not in minv:
                        #minv[k] = np.amin(v, axis=0)
                        #maxv[k] = np.amax(v, axis=0)
                        minv[k] = torch.amin(v, dim=0)
                        maxv[k] = torch.amax(v, dim=0)
                    else:
                        #minv[k] = np.minimum(minv[k], np.amin(v, axis=0))
                        #maxv[k] = np.maximum(maxv[k], np.amax(v, axis=0))
                        minv[k] = torch.minimum(minv[k], torch.amin(v, dim=0))
                        maxv[k] = torch.maximum(maxv[k], torch.amax(v, dim=0))

        if normalize:
            for ele in nFPs.keys():
                fprange[ele] = [minv[ele], maxv[ele], maxv[ele]-minv[ele]]
            fprange, magnitudeScale, interceptScale = normalizeParas(fprange)

        #first_size, first_peak = tracemalloc.get_traced_memory()
        #print('loopimages:',first_peak/1024/1024)
        #fp = {'fprange':fprange, 'fpData': fpData}
        print("Fingerprints done, time: %.2f s" % fptime)
        #batch = atomCenteredFPs()
        #acfs = list(map(fpData.__getitem__, list(fpData.keys())))
        #batch.stackFPs(acfs)
        #batch.stackFPsList(acfs)

        #save fp dictionary with fprange and fpdata to pickle file. 'fps.pckl' is default file name
        #io.save_data(fp, fpfilename)
        #sys.exit()
        fmodules.fpcalc.cleanup()
        return fprange, magnitudeScale, interceptScale

