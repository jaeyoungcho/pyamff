import time
import sys
import torch
import numpy as np
from collections import OrderedDict
from pyamff.utilities.preprocessor import generateInputs, normalize
from pyamff.mlModels.lossfunctions import lossFunction
from pyamff.utilities.analyze import plotGradFlow
from pyamff.optimizer.lbfgs import LBFGSScipy
from pyamff.optimizer.sd import SD

class PyAMFF():

    def __init__(self, model,
                 fingerprints=None,
                 energy_coefficient=1.00,
                 force_coefficient=0.1,
                 optimizer='LBFGS',
                 learningRate=0.1,
                 lossConvergence=1e-4,
                 model_logfile='pyamff.pt',
                 logmodel_interval=100,
                 debug='force',
                 weight_decay=None):
        self.model = model
        self.fingerprints = fingerprints
        self.energy_coefficient = energy_coefficient
        self.force_coefficient = force_coefficient
        self.optimizer = optimizer
        self.learningRate = learningRate
        self.lossConvergence = lossConvergence
        self.model_logfile = model_logfile
        self.logmodel_interval = logmodel_interval
        self.weight_decay = weight_decay
        self.debug = debug

    """
    trainingimages: {'hashedkey':Atoms(), ...}
    descriptor: amp.descriptor object. 
               Use descriptor.fingerprints to fetch fingerprint data with
               following structure:
               {'hashedkey': fps for image 1, ...}
               atomSymbols, fpdata = zip(*fps1):
               atomSymbols: a tuple ('Au', 'Au',...,)
               fpdata: a tuple ([G1, G2, ...,] for atom 1, [G1, G2, ...,] for atom 2, ...)
               Use descriptor.fingerprintprimes to fetch corresponding derivatives

    """
    def fitSingleModel(self, rank, seed, device, trainingimages, descriptor, maxEpochs, log=None):

        torch.manual_seed(seed + rank)

        ref_energies = []
        ref_forces = []
        labels = []

        fingerprintDerDB = descriptor.fingerprintprimes
        keylist = list(trainingimages.keys())
        nimages = len(keylist)

        fingerprintDB = descriptor.fingerprints

        (atoms_fps,           # Fingerprints for all images {'Au':array([[]]]}
         fp_imageIndices,
         fpranges,
         dgdx,
         dEdg_AtomIndices,
         force_AtomIndices,
         natomsPerElement,
         ntotalAtoms
        ) = \
             generateInputs(fingerprintDB,
                            self.model.nFPs,
                            keylist,
                            fingerprintDerDB)

        atoms_fps = {}
        fp_imageIndices = {}
        fprange = {}
        dgdx = {}
        dEdg_AtomIndices = {}
        force_AtomIndices = {}
        natomsPerElement = {}
        ntotalAtoms = {}
        for i in range(len(keylist)):
            (atoms_fps[keylist[i]],           # Fingerprints for all images {'Au':array([[]]]}
             fp_imageIndices[keylist[i]],
             fprange[keylist[i]],
             dgdx[keylist[i]],
             dEdg_AtomIndices[keylist[i]],
             force_AtomIndices[keylist[i]],
             natomsPerElement[keylist[i]],
             ntotalAtoms[keylist[i]]
             ) = \
                 generateInputs({keylist[i]:fingerprintDB[keylist[i]]},
                                self.model.nFPs,
                                [keylist[i]],
                                {keylist[i]:fingerprintDerDB[keylist[i]]})

            (atoms_fps[keylist[i]], dgdx[keylist[i]]) = \
                   normalize(atoms_fps[keylist[i]],fpranges, dgdx[keylist[i]], dEdg_AtomIndices[keylist[i]], force_coefficient = self.energy_coefficient)

        # Fetch target energies and forces
        energy_reference = list(map(
            #lambda x: trainingimages[x].get_potential_energy(apply_constraint=False),
            lambda x: [trainingimages[x].get_potential_energy(apply_constraint=False)],
            keylist))
        energy_reference = np.array(energy_reference)
        intercept = np.mean(energy_reference)
        energy_reference = energy_reference - intercept
        if len(energy_reference) == 1:
            slope = 1.0
        else:
            slope = np.mean(np.abs(energy_reference))
        #slope = 1.0
        #print('ref e:', energy_reference)

        targets = []
        forces_reference = []
        if self.force_coefficient is not None:
            for i in range(len(keylist)):
                atoms = trainingimages[keylist[i]]
                #forces_reference.extend(atoms.get_forces(apply_constraint=False).flatten())
                targets.append(torch.cat([torch.tensor(energy_reference[i]), 
                                torch.flatten(torch.tensor(atoms.get_forces(apply_constraint=False).tolist(), dtype=torch.double))]))
        else:
            forces_reference = None

        # Set model parameters
        # TODO: for batch optimization, need to dynamically update these parameter
        # Define loss function

        if self.debug =='force':
            outputs = self.model(atoms_fps, dgdx)
            energies = (outputs[0:nimages]+intercept).data
            forces = torch.reshape(outputs[nimages:], (nimages, 15, 3)).data.numpy()
            outfile = open('pytrochforce.data', 'a')
            for i in range(nimages):
                outfile.write("{:12.8f}\n".format(energies[i]))
                np.savetxt(outfile, forces[i], fmt="%12.8f")
            outfile.close()
            sys.exit()
        criterion = lossFunction(force_coefficient=self.force_coefficient)

        # Define optimizers
        if 'LBFGS' in self.optimizer or 'SD' in self.optimizer:
            if self.optimizer == 'LBFGS':
               optimizer = torch.optim.LBFGS(
                                             #self.model.model_params,
                                             self.model.parameters(),
                                             lr = self.learningRate, # learning rate
                                             max_iter = 1,
                                             max_eval = 1000, # maximal number of function evaluation per optimization
                                             tolerance_grad = 1e-5,
                                             tolerance_change = 1e-9,
                                             history_size = 100,
                                             line_search_fn = None # either 'strong_wolfe' or None
                                             )
            if self.optimizer == 'LBFGSScipy':
                optimizer = LBFGSScipy(self.model.parameters(), max_iter=1, max_eval=None,
                                       tolerance_grad=1e-5, tolerance_change=1e-9, history_size=10)
            if self.optimizer =='SD':
                optimizer = SD(self.model.parameters())
            nimages = 1
            ttime = 0
            for epoch in range(maxEpochs):
                running_loss = 0

                start_time = time.time()
                def closure():
                    optimizer.zero_grad()
                    loss = 0
                    for i in range(len(keylist)):
                        self.model.set(fp_imageIndices[keylist[i]],
                                       nimages,
                                       #dgdx,
                                       dEdg_AtomIndices[keylist[i]],
                                       force_AtomIndices[keylist[i]],
                                       natomsPerElement[keylist[i]],
                                       ntotalAtoms[keylist[i]],
                                       slope)
                        outputs = self.model(atoms_fps[keylist[i]], dgdx[keylist[i]])
                        loss += criterion(outputs, targets[i], ntotalAtoms=30)
                        loss.backward(retain_graph=True)

                    # used to check if analytical gradient is correct
                    #energies.require_grad = True
                    #forces.require_grad = True
                    #res = torch.autograd.gradcheck(lossFunction(), (energies, forces, energy_reference, forces_reference, ntotalAtoms), raise_exception=False)
                    #print('res',res)
                    # check average gradient for each layer
                    #plot_grad_flow(self.model.model_namedparams)
                    #truncate gradient
                    #torch.nn.utils.clip_grad_norm_(self.model.model_params, 0.01)
                    #torch.nn.utils.clip_grad_value_(self.model.model_params, 0.01)
                    return loss
                loss = optimizer.step(closure)
                try:
                    running_loss += loss.item()
                except:
                    pass
                end_time = time.time()
                ttime = ttime + (end_time-start_time)
                print('loss:', epoch, running_loss)
            print('time used', ttime)
            sys.exit()

        elif self.optimizer == 'SGD':
            optimizer = torch.optim.SGD(self.model.parameters(), lr=self.learningRate, momentum=0.9)
        elif self.optimizer == 'ADAM':
            optimizer = torch.optim.Adam(self.model.parameters(), lr= self.learningRate)

        #print(self.model.model_params)
        for epoch in range(maxEpochs):
            running_loss = 0
            start_time = time.time()

            optimizer.zero_grad()
            #(energies, forces) = self.model(atoms_fps, dgdx)
            #loss = criterion(energies, forces, energy_reference, forces_reference, ntotalAtoms)
            outputs = self.model(atoms_fps, dgdx)
            loss = criterion(outputs, targets, ntotalAtoms)
            loss.backward()
            #loss = optimizer.step(closure)
            optimizer.step()
            running_loss = loss.item()
            end_time = time.time()
            print('loss:', epoch, running_loss)

    def saveModel(self, intercept):
        state_dict = self.model.state_dict()
        #for key in state_dict.keys():
        #  state_dict[key] = state_dict[key].cpu()
        modelParameters = self.model.parametersDict()
        preprocessParas = {}
        preprocessParas['intercept'] = intercept
        preprocessParas['fprange']=self.fprange
        preprocessParas['fingerprints']=self.fingerprints
        torch.save({
             'state_dict': state_dict,
             'preprocessParas': preprocessParas,
             'Modelparameters': modelParameters},
             self.model_logfile)

    #def fit(self, trainingimages, descriptor, maxEpochs, log=None):
    def fit(self, trainingimages, fpDb, fpDerDb, maxEpochs, log=None):

        #torch.manual_seed(1234)
        #torch.set_num_threads(8)

        ref_energies = []
        ref_forces = []
        labels = []

        #fingerprintDerDB = descriptor.fingerprintprimes
        #fingerprintDB = descriptor.fingerprints
        fingerprintDB = fpDb
        fingerprintDerDB = fpDerDb
        keylist = list(trainingimages.keys())
        nimages = len(keylist)

        (atoms_fps,           #Fingerprints for all images {'Au':array([[]]]}
         fp_imageIndices,
         self.fprange,
         dgdx,
         dEdg_AtomIndices,
         force_AtomIndices,
         natomsPerElement,
         natomsPerimageForce,
         natomsPerimageEnergy,
         ntotalAtoms,
         ) = \
             generateInputs(fingerprintDB,
                            self.model.nFPs,
                            keylist,
                            fingerprintDerDB)
        (atoms_fps, dgdx) = \
              normalize(atoms_fps, self.fprange, dgdx, dEdg_AtomIndices, force_coefficient = self.energy_coefficient)
        # Fetch target energies and forces
        print(self.fprange)
        energy_reference = list(map(
            #lambda x: trainingimages[x].get_potential_energy(apply_constraint=False),
            lambda x: [trainingimages[x].get_potential_energy(apply_constraint=False)],
            keylist))
        energy_reference = np.array(energy_reference)
        intercept = np.mean(energy_reference)
        energy_reference = energy_reference - intercept
        if len(energy_reference) == 1:
            slope = 1.0
        else:
            slope = np.mean(np.abs(energy_reference))
        #slope = 1.0
        energy_reference = torch.tensor(energy_reference,dtype=torch.double)
        #print('ref e:', energy_reference)

        refforce = open('refforce.dat', 'w')
        forces_reference = []
        if self.force_coefficient is not None:
            for i in range(len(keylist)):
                atoms = trainingimages[keylist[i]]
                forces_reference.extend(atoms.get_forces(apply_constraint=False))
                #targets.append(torch.cat([torch.tensor(energy_reference[i]),
                #                torch.flatten(torch.tensor(atoms.get_forces(apply_constraint=False).tolist(), dtype=torch.double))]))
        else:
            forces_reference = None
        forces_reference = torch.tensor(forces_reference, dtype=torch.double)
        targets = torch.cat([torch.flatten(energy_reference), torch.flatten(forces_reference)])
        forces_ref = torch.flatten(forces_reference)
        energies_ref = torch.flatten(energy_reference)

        print('es', energies_ref)
        # Set model parameters
        # TODO: for batch optimization, need to dynamically update these parameter
        #print('fp_imageIndices:', fp_imageIndices)
        #print('dEdg_AtomIndices:', dEdg_AtomIndices)
        #print('force_AtomIndices:', force_AtomIndices)
        #print('natomsPerElemen:', natomsPerElement)
        #print('ntotalAtoms:',ntotalAtoms)
        self.model.set(fp_imageIndices,
                       nimages,
                       #dgdx,
                       dEdg_AtomIndices,
                       force_AtomIndices,
                       natomsPerElement,
                       ntotalAtoms,
                       slope)
        # Define loss function
        #print('ntotalAtoms:',ntotalAtoms)
        criterion = lossFunction(force_coefficient=self.force_coefficient)

        #Define Optimizers
        if 'LBFGS' in self.optimizer or 'SD' in self.optimizer:
            if self.optimizer == 'LBFGS':
                optimizer = torch.optim.LBFGS(
                                             #self.model.model_params,
                                             self.model.parameters(),
                                             lr = self.learningRate, #learning rate
                                             max_iter = 1,
                                             max_eval = 1000, # maximal number of function evaluation per optimization
                                             tolerance_grad = 1e-5,
                                             tolerance_change = 1e-9,
                                             history_size = 100,
                                             line_search_fn = None #either 'strong_wolfe' or None
                                             )
            if self.optimizer == 'LBFGSScipy':
                optimizer = LBFGSScipy(self.model.parameters(), max_iter=1, max_eval=None,
                            tolerance_grad=1e-5, tolerance_change=1e-9, history_size=10)
            if self.optimizer =='SD':
                optimizer = SD(self.model.model.parameters())
                print('epoch', 'loss', 'expected')
            print(self.learningRate)
            #torch.set_num_threads(1)
            for epoch in range(maxEpochs):
                running_loss = 0
                start_time = time.time()
                def closure():
                    #outputs = self.model(atoms_fps, dgdx)
                    #loss = criterion(outputs, targets, ntotalAtoms=15)
                    predEnergies, predForces = self.model(atoms_fps, dgdx)
                    #print('natomsPerimageEnergy', natomsPerimageEnergy)
                    #print('natomsPerimageForce', natomsPerimageForce)
                    loss = criterion(predEnergies, predForces, energies_ref, forces_reference, 
                                     natomsEnergy=natomsPerimageEnergy,
                                     natomsForce=natomsPerimageForce)

                    # used to check if analytical gradient is correct
                    #energies.require_grad = True
                    #forces.require_grad = True
                    #res = torch.autograd.gradcheck(lossFunction(), (energies, forces, energy_reference, forces_reference, ntotalAtoms), raise_exception=False)
                    #print('res',res)
                    #loss.backward()
                    # check average gradient for each layer
                    #torch.nn.utils.clip_grad_norm_(self.model.model_params, 0.01)
                    lossgrads = torch.autograd.grad(loss, self.model.parameters(), create_graph=False) 
                    for p, g in zip(self.model.parameters(), lossgrads):
                        p.grad = g
                        #print(g)
                    #optimizer.step()
                    #plot_grad_flow(self.model.model_namedparams)
                    #truncate gradient
                    #torch.nn.utils.clip_grad_value_(self.model.model_params, 0.01)
                    return loss
                #try:
                #  loss, expected = optimizer.step(closure)
                #  print(epoch, loss, expected)
                #  #return loss.item()
                #except:
                #  print("LBFGS")
                loss  = optimizer.step(closure)
                running_loss = loss.item()
                end_time = time.time()
                print('loss:', epoch, running_loss)
                #pass
                if epoch % self.logmodel_interval == 0:
                    self.saveModel(intercept)
                if loss < self.lossConvergence:
                    break
            self.saveModel(intercept)

        elif self.optimizer == 'SGD':
            optimizer = torch.optim.SGD(self.model.parameters(), lr=self.learningRate, momentum=0.9, weight_decay=self.weight_decay)
        elif self.optimizer == 'ADAM':
            optimizer = torch.optim.Adam(self.model.parameters(), lr= self.learningRate, weight_decay=self.weight_decay)

        #print(self.model.model_params)
            for epoch in range(maxEpochs):
                running_loss = 0
                start_time = time.time()

                optimizer.zero_grad()
                #(energies, forces) = self.model(atoms_fps, dgdx)
                #loss = criterion(energies, forces, energy_reference, forces_reference, ntotalAtoms)
                outputs = self.model(atoms_fps, dgdx)
                loss = criterion(outputs, targets, ntotalAtoms)
                loss.backward()
                #loss = optimizer.step(closure)
                optimizer.step()
                running_loss = loss.item()
                #end_time = time.time()
                return loss
                #print('loss:', epoch, running_loss)

