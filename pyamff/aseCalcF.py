"""
This calculator is used to load a FORTRAN trained machine-learning model and do calculations
"""
from __future__ import division

import numpy as np

from ase.neighborlist import NeighborList
from ase.calculators.calculator import Calculator, all_changes
from pyamff.utilities.preprocessor import generateInputs, normalize
from ase.calculators.calculator import PropertyNotImplementedError
from pyamff.mlModels.pytorchNN import NeuralNetwork
from pyamff.utilities.preprocessor import normalizeParas
from pyamff.neighborlist import NeighborLists
from pyamff.config import ConfigClass
from pyamff.fingerprints.fingerprintsWrapper import atomCenteredFPs
import torch

from pyamff import fmodules

class aseCalcF(Calculator):
    implemented_properties = ['energy', 'forces']
    default_parameters = {}
    nolabel = True

    def __init__(self, modelType='NeuralNetwork',**kwargs):
        Calculator.__init__(self, **kwargs)
#        self.model_path = model_path
#        self.modelType = modelType
#        self.model, self.nFPs, self.slope, preprocessParas = loadModel(model_path, modelType)
#        self.Gs = preprocessParas['fingerprints'].fp_paras
#        self.fprange = preprocessParas['fprange']
#        self.intercept = preprocessParas['intercept']
        self.acf = atomCenteredFPs() 

    def calculate(self, atoms=None, 
                  properties=['energy'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)
        energy, forces = self.calculateFingerprints(self.atoms)

        self.results['energy'] = energy#+self.intercept
        self.results['forces'] = forces

    def calculateFingerprints(self, atoms=None):
        images = {0:atoms}
#        keylist = [0]
        max_fps = 100
        atomicNrs = np.array(atoms.get_atomic_numbers())
        N = len(atomicNrs)
#        print(N)
#        print(len(atomicNrs))
        uniqueNrs = []
        for i in atomicNrs:
            if i not in uniqueNrs:
                uniqueNrs.append(i)
        unique = np.array(uniqueNrs)
#        print(unique)
        num_elements = len(unique)
        max_fps = 100
#        print(fmodules.fpcalc.read_mlff.__doc__)
        print("loading mlff")
        fmodules.fpcalc.read_mlff(max_fps, atomicNrs, unique)
        print("mlff loaded")
#        print(fmodules.fnnmodule.prepfnn.__doc__)
        fmodules.fnnmodule.prepfnn_ase(max_fps, atomicNrs, unique)
#        print('fmod')
        pos_car = atoms.get_positions()
#        pos_car = np.asfortranarray(pos_car)
        cell = atoms.cell.array.flatten()
#        print(fmodules.pyamff.calc_ase.__doc__)
#        predForces=[]
#        print(pos_car)
#        print(cell)
#        print(atomicNrs)
#        print(predForces)
#        print(unique)
        predForces, predEnergies = fmodules.pyamff.calc_ase(pos_car, cell, atomicNrs, unique)
#        print('!!!', predEnergies)
#        print(predEnergies)
#        print(predForces)
#        fmodules.pyamff.calc_eon()
#        nl = NeighborLists(cutoff = 6.0)
#        nl.calculate(images, fortran=True)
#        acf = represent_BP(nl, self.nFPs, images, properties=None, G_paras=self.Gs, fortran=True, fNN=True)
#        print ('self.fprange')
#        print (self.fprange)
#        fprange, magnitudeScale, interceptScale = normalizeParas(self.fprange)
#        acf.normalizeFPs(fprange, magnitudeScale, interceptScale)
#        acf.normalize_fortrandgdx(fprange, magnitudeScale)
        #Set FP variables for fNN 
#        self.model = prep_FPvars(self.model, self.nFPs, acf, nl)
        #Start running fortran NN        
#        self.model.init()
#        self.model.forward()
        #print ('etotal before slope and intercept')
        #print (self.model.etotal)
        #print ('forces before slope')
        #print (self.model.forces)

#        predEnergies, predForces = self.model.etotal*self.slope, self.model.forces*-self.slope
#        self.model.cleanup()
#        print('entering cleanup')
        fmodules.fpcalc.cleanup()
#        print('exiting cleanup')
        fmodules.neuralnetwork.nncleanup()
#        print('exiting nncleanup')

        return predEnergies, predForces

