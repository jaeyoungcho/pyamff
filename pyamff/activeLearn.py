"""

Used for active learning
"""
from pyamff.utilities.dataPartition import batchGenerator, Dataset
from pyamff.utilities.preprocessor import normalize, fetchProp, normalizeParas
from .training import Trainer
from pyamff.mlModels.pytorchNN import NeuralNetwork
from pyamff.neighborlist import NeighborLists
from pyamff.config import ConfigClass
from pyamff.fingerprints.behlerParrinello import represent_BP
from pyamff.fingerprints.fingerprintsWrapper import atomCenteredFPs
from pyamff.utilities.logTool import setlogger
import copy, os

class activeLearn():

    def __init__(self, configfile):
        # Read in parameters
        self.logger = setlogger()
        print(os.getcwd())
        self.config = ConfigClass()
        self.config.initialize()

        fp_paras = self.config.config['fp_paras'].fp_paras
        self.nFPs = {}
        for key in fp_paras.keys():
            self.nFPs[key] = len(fp_paras[key])
        self.trainingimages = []
        self.cycle = 0
        self.maxEpochs = self.config.config['epochs_max']
        losstol = self.config.config['force_coefficient'] * self.config.config['force_tol'] \
                + self.config.config['energy_coefficient'] * self.config.config['energy_tol']
        self.model = NeuralNetwork(
                       hiddenlayers=self.config.config['hidden_layers'],
                       nFPs=self.nFPs,
                       forceTraining=self.config.config['force_training'],
                       # TODO: load pretrained params
                       params=None,
                       slope=None
                       )

        self.calc = Trainer(model=self.model,
                      optimizer=self.config.config['optimizer_type'],
                      # TODO: check if it can be reloaded
                      fpParas=self.config.config['fp_paras'],
                      energy_coefficient=self.config.config['energy_coefficient'],
                      force_coefficient=self.config.config['force_coefficient'],
                      lossConvergence=losstol,
                      lossgradtol=self.config.config['loss_grad_tol'],
                      # TODO
                      intercept=None,
                      learningRate=0.1,
                      model_logfile='pyamff.pt',
                      logmodel_interval=100,
                      debug=None,
                      weight_decay=None,
                      fprange=None)

        #self.fpDbs = OrderedDict()
        self.fpDbs = []


    def train(self, images, debug=False, params=None):

        #self.trainingimages.append(image)
        trainingimages, properties, slope, intercept = fetchProp(images, self.config.config['force_training'], activeLearning=True)

        # Get neighborlists

        nl = NeighborLists(cutoff=6.0)
        nl.calculate(trainingimages, fortran=True)

        # Calculate fingerpints of the new image: fprange={} case

        acfs, fprange = represent_BP(nl, self.nFPs, trainingimages, properties, G_paras=self.config.config['fp_paras'].fp_paras, fortran=True, normalize=False)
        for key in acfs.keys():
            self.fpDbs.append(acfs[key])
        print(' # of images', len(self.fpDbs))
        acfs = atomCenteredFPs()
        acfs.stackFPs(copy.copy(self.fpDbs))
        #if self.cycle == 0:
        #  #self.acfs = copy.copy(acfs[self.numberImages])
        #  self.acfs.stackFPs(list(acfs.values()), new=True, activelearning=True)
        #else:
        #  self.acfs.stackFPs(list(acfs.values()), new=False, activelearning=True)
        acfs.findFPrange()
        #self.acfs.setOriginal()
        slope, intercept = acfs.scaleEnergies()

        fprange, magnitudeScale, interceptScale = normalizeParas(acfs.fprange)

        setattr(self.model, 'slope', slope)
        setattr(self.calc, 'intercept', intercept)
        setattr(self.calc, 'fprange', fprange)
        setattr(self.calc, 'nimages', len(self.fpDbs))
        acfs.normalizeFPs(fprange, magnitudeScale, interceptScale)
        if debug:
            self.model.setParams(params)
        #partition = Dataset({0:self.acfs}, [0])
        self.calc.parallelFit(rank=0, size=1, thread=1,
                              partition=acfs, batch_number=1, maxEpochs=self.maxEpochs, logger=self.logger, parallel=False)
        #self.acfs.fetchOriginal()
        self.cycle += 1
        return self.model, self.calc.preprocessParas
