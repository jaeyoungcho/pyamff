#!/usr/bin/env python
import time, os
import sys, math
import torch
import numpy as np
from collections import OrderedDict
from pyamff.utilities.dataPartition import batchGenerator
from pyamff.utilities.preprocessor import normalize, fetchProp
from pyamff.utilities import fileIO as io
from pyamff.mlModels.lossFunctions import LossFunction
from pyamff.mlModels.pytorchNN import NeuralNetwork
from pyamff.utilities.analyze import plotGradFlow
from pyamff.optimizer.lbfgs import LBFGSScipy
from pyamff.optimizer.lbfgsNew import LBFGSNew
from pyamff.optimizer.sd import SD
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP

def averageGradients(model):
    size = float(dist.get_world_size())
    for param in model.parameters():
        dist.all_reduce(param.grad.data, op=dist.ReduceOp.SUM)
#        param.grad.data /= size


class Trainer():

    def __init__(self, model,
                fpParas=None,
                energy_coefficient=1.00,
                force_coefficient=0.1,
                optimizer='LBFGS',
                learningRate=0.1,
                lossConvergence=1e-4,
                energyRMSEtol=0.01,
                forceRMSEtol=0.1,
                lossgradtol=1e-09,
                model_logfile='pyamff.pt',
                logmodel_interval=100,
                debug='force', 
                weight_decay=0,
                intercept=None,
                fprange=None,
                nimages=None):
        self.model = model
        self.fpParas = fpParas
        self.energy_coefficient = energy_coefficient
        self.force_coefficient = force_coefficient
        self.optimizer = optimizer
        self.learningRate = learningRate
        self.lossConvergence = lossConvergence
        self.lossgradtol = lossgradtol
        self.model_logfile = model_logfile
        self.logmodel_interval = logmodel_interval
        self.weight_decay = weight_decay
        self.debug = debug
        self.intercept = intercept
        self.fprange = fprange
        self.nimages = nimages

        # Variable used to store training info
        self.energyRMSE = None
        self.forceRMSE = None
        self.energyloss = 0.
        self.forceloss = 0
        self.energyRMSEtol = energyRMSEtol
        self.forceRMSEtol = forceRMSEtol
        self.n_iter = 1

    """
    trainingimages: {'hashedkey':Atoms(), ...}
    descriptor: amp.descriptor object. 
               Use descriptor.fingerprints to fetch fingerprint data with
               following structure:
               {'hashedkey': fps for image 1, ...}
               atomSymbols, fpdata = zip(*fps1):
               atomSymbols: a tuple ('Au', 'Au',...,)
               fpdata: a tuple ([G1, G2, ...,] for atom 1, [G1, G2, ...,] for atom 2, ...)
               Use descriptor.fingerprintprimes to fetch corresponding derivatives
    """


    def saveModel(self):
        state_dict = self.model.state_dict()
        #for key in state_dict.keys():
        #  state_dict[key] = state_dict[key].cpu()
        modelParameters = self.model.parametersDict()
        self.preprocessParas = {}
        self.preprocessParas['intercept'] = self.intercept
        self.preprocessParas['slope'] = self.model.slope
        self.preprocessParas['fprange']=self.fprange
        self.preprocessParas['fingerprints']=self.fpParas
        torch.save({
             'state_dict': state_dict,
             'preprocessParas': self.preprocessParas,
             'Modelparameters': modelParameters},
             self.model_logfile)


    def parallelFit(self, rank, size, thread, partition, batch_number, maxEpochs, device, logger=None, parallel=True):

        #os.environ['MASTER_ADDR'] = '127.0.0.1'
        #os.environ['MASTER_PORT'] = '1234'

        #dist.init_process_group('gloo', init_method='env://', rank=rank, world_size=size)

        if self.optimizer == 'LBFGSScipy':
            pass
        else:
            self.model = self.model.to(device)

        if device == torch.device("cuda:0"):
            ddp_model = DDP(self.model, device_ids=[rank])
        else:
            ddp_model = DDP(self.model, device_ids=[])

        n_gpus = torch.cuda.device_count()

        torch.manual_seed(1234)
        torch.set_num_threads(thread)
        if parallel:
            #print('partition',partition)
            #print(len(partition))
            batch_size = math.ceil(float(len(partition))/float(batch_number))
            batches = torch.utils.data.DataLoader(partition,
                                                  batch_size=batch_size,
                                                  collate_fn=batchGenerator,
                                                  shuffle=False)
        else:
            batches = [partition]
        criterion = LossFunction(force_coefficient=self.force_coefficient, device=device)
        #torch.set_num_threads(12)
        # Define Optimizers
#        if 'LBFGS' in self.optimizer or 'SD' in self.optimizer:
#            if self.optimizer == 'LBFGSNew':
#                optimizer = LBFGSNew(
#                                     #self.model.model_params,
#                                     self.model.parameters(),
#                                     lr=self.learningRate, #learning rate
#                                     max_iter=1,
#                                     max_eval=1000, #maximal number of function evaluation per optimization
#                                     tolerance_grad=1e-5,
#                                     tolerance_change=1e-5,
#                                     history_size=10,
#                                     line_search_fn=False, #either 'strong_wolfe' or None
#                                     batch_mode=False
#                                     )

        if 'LBFGS' in self.optimizer:

            if self.optimizer == 'LBFGSScipy':

                optimizer = LBFGSScipy(self.model.parameters(), max_iter=maxEpochs, logger=logger, rank=rank)
                curr_loss = 0.
                energyloss = 0.
                forceloss = 0.
                start_time = time.time()
                activation = {}

                def closure():
                    loss = 0
                    energyloss = 0
                    forceloss = 0
                    energyRMSE = 0
                    forceRMSE = 0
                    batchid = 0
                    for batch in batches:
                        epoch = 0
                        #print('allFPs', batch.allElement_fps)
                        predEnergies, predForces  = self.model(batch.allElement_fps, batch.dgdx, batch, device, logger=logger)
                        for i in activation:
                            print(activation[i])
                            print('test')
                        loss += criterion(predEnergies, predForces, batch.energies, batch.forces,
                                          natomsEnergy = batch.natomsPerimageEnergy,
                                          natomsForce = batch.natomsPerimageForce)
                        lossgrads = torch.autograd.grad(loss, self.model.parameters(),
                                                        retain_graph = True, create_graph=False)
                        for p, g in zip(self.model.parameters(), lossgrads):
                            #if batchid == 0:
                            p.grad = g
                            #else:
                            #    p.grad += g
                        batchid += 1
                        energyloss += criterion.energyloss
                        forceloss += criterion.forceloss
                        #if parallel:
                        #    averageGradients(self.model)
                        #    dist.all_reduce(loss, dist.ReduceOp.SUM)
                        #    dist.all_reduce(energyloss, dist.ReduceOp.SUM)
                        #    dist.all_reduce(forceloss, dist.ReduceOp.SUM)
                        #if rank == 0:
                        #logger.info('%s', "{:12d} {:12.8f} {:12.8f} {:12.8f}".format(epoch, loss.item(), energyRMSE, forceRMSE))
                        if epoch % self.logmodel_interval == 0:
                            self.saveModel()

                    if parallel:
                        averageGradients(self.model)
                        dist.all_reduce(loss, dist.ReduceOp.SUM)
                        dist.all_reduce(energyloss, dist.ReduceOp.SUM)
                        dist.all_reduce(forceloss, dist.ReduceOp.SUM)
                    energyRMSE = np.sqrt(energyloss.item()/self.nimages)
                    forceRMSE = np.sqrt(forceloss.item()/self.nimages)

                    # Raise error if energy RMSE or/and force RMSE is nan
                    if math.isnan(energyRMSE):
                        raise ValueError('Energy RMSE is nan')
                    if math.isnan(forceRMSE):
                        raise ValueError('Force RMSE is nan')

                    if energyRMSE < self.energyRMSEtol and forceRMSE < self.forceRMSEtol:
                        logger.info('Minimization converged')
                        self.saveModel()
                        io.saveFF(self.model, self.preprocessParas, filename="mlff.pyamff")
                        sys.exit()
                    return loss, energyRMSE, forceRMSE

                optimizer.step(closure)
                 #if prev_loss is not None:
                 #    if abs(curr_loss - prev_loss) / max([abs(prev_loss), abs(curr_loss), 1]) <= self.lossgradtol:
                 #        print('Minimization stopped due to too small change on loss (<lossgradtol)')
                 #        break
                self.saveModel()
                io.saveFF(self.model, self.preprocessParas, filename="mlff.pyamff")
                sys.exit()

            elif self.optimizer == 'LBFGS':
                """
                    Uses torch LBFGS optimizer. Loss is not averaged over batches.
                """
                optimizer = torch.optim.LBFGS(
                                             self.model.parameters(),
                                             lr=self.learningRate, #learning rate
                                             tolerance_grad=1e-10,
                                             tolerance_change=1e-10,
                                             max_iter=maxEpochs,
                                             max_eval=None,
                                             history_size=10,
                                             line_search_fn=None # either 'strong_wolfe' or None
                                             )

                epoch = 0
                def closure():
                    curr_loss = 0.
                    energyloss = 0.
                    forceloss = 0.
                    loss = 0.

                    for batch in batches:
                        optimizer.zero_grad()
                        batchid = 0

                        predEnergies, predForces  = ddp_model(batch.allElement_fps, batch.dgdx, batch, device, logger=logger)
                        loss += criterion(predEnergies, predForces, batch.energies, batch.forces,
                                          natomsEnergy = batch.natomsPerimageEnergy,
                                          natomsForce = batch.natomsPerimageForce)
                        lossgrads = torch.autograd.grad(loss, self.model.parameters(),
                                                        retain_graph=True, create_graph=False)
                        for p, g in zip(self.model.parameters(), lossgrads):
                            p.grad = g
                        batchid += 1
                        #curr_loss += loss.item()
                        #energyloss += criterion.energyloss.item()
                        #forceloss += criterion.forceloss.item()
                        energyloss += criterion.energyloss
                        forceloss += criterion.forceloss
                        #energyRMSE = np.sqrt(energyloss/self.nimages)
                        #forceRMSE = np.sqrt(forceloss/self.nimages)

                        # Raise error if energy RMSE or/and force RMSE is nan
                        if math.isnan(energyloss.item()):
                            raise ValueError('energy RMSE is nan')
                        if math.isnan(forceloss.item()):
                            raise ValueError('force RMSE is nan')

                    averageGradients(ddp_model)
                    dist.all_reduce(loss, dist.ReduceOp.SUM)
                    dist.all_reduce(energyloss, dist.ReduceOp.SUM)
                    dist.all_reduce(forceloss, dist.ReduceOp.SUM)
                    energyRMSE = np.sqrt(energyloss.item()/self.nimages)
                    forceRMSE = np.sqrt(forceloss.item()/self.nimages)
                    if rank == 0:

                        # Raise error if energy RMSE or/and force RMSE is nan
                        if math.isnan(energyRMSE):
                            raise ValueError('energy RMSE is nan')
                        if math.isnan(forceRMSE):
                            raise ValueError('force RMSE is nan')

                        logger.info('%s', "{:12d} {:12.8f} {:12.8f} {:12.8f}".format(self.n_iter, loss.item(), energyRMSE, forceRMSE))
                        self.n_iter += 1

                    if self.n_iter % self.logmodel_interval == 0:
                        self.saveModel()

                    if energyRMSE < self.energyRMSEtol and forceRMSE < self.forceRMSEtol:
                        logger.info('Minimization converged')
                        self.saveModel()
                        io.saveFF(self.model, self.preprocessParas, filename="mlff.pyamff")
                        sys.exit()

                    return float(loss.item())

                optimizer.step(closure)

                self.saveModel()
                io.saveFF(self.model, self.preprocessParas, filename="mlff.pyamff")
        else: 
            if self.optimizer == 'SGD':
                """
                    Uses torch SGD optimizer. Loss is not averaged over batches in order to save memory.
                """

                optimizer = torch.optim.SGD(self.model.parameters(), lr=self.learningRate,
                                            weight_decay = self.weight_decay
                                           )
            elif self.optimizer == 'ADAM':
                """
                    Uses torch ADAM optimizer. Loss is not averaged over batches in order to save memory.
                """
                optimizer = torch.optim.Adam(self.model.parameters(), lr=self.learningRate,
                                             weight_decay=self.weight_decay
                                             )

            elif self.optimizer == 'Rprop':
                """
                    Uses torch Rprop optimizer. Loss is not averaged over batches in order to save memory.
                """
                optimizer = torch.optim.Rprop(self.model.parameters(), lr=self.learningRate)

            #torch.cuda.set_device(device)
            for epoch in range(maxEpochs):
                curr_loss = 0.
                energyloss = 0.
                forceloss = 0.
                for batch in batches:
                    predEnergies, predForces   = ddp_model(batch.allElement_fps, batch.dgdx, batch, device, logger=logger)
                    loss = criterion(predEnergies, predForces, batch.energies, batch.forces,
                                     natomsEnergy = batch.natomsPerimageEnergy,
                                     natomsForce = batch.natomsPerimageForce)
                    lossgrads = torch.autograd.grad(loss, self.model.parameters(), create_graph=False)
                    for p, g in zip(self.model.parameters(), lossgrads):
                        p.grad = g

                    optimizer.step()
                    curr_loss += loss.item()
                    energyloss += criterion.energyloss.item()
                    forceloss  += criterion.forceloss.item()
                if rank == 0:
                    energyRMSE = np.sqrt(energyloss/self.nimages)
                    forceRMSE = np.sqrt(forceloss/self.nimages)

                    # Raise error if energy RMSE or/and force RMSE is nan
                    if math.isnan(energyRMSE):
                        raise ValueError('energy RMSE is nan')
                    if math.isnan(forceRMSE):
                        raise ValueError('force RMSE is nan')

                    logger.info('%s', "{:12d} {:12.8f} {:12.8f} {:12.8f}".format(epoch, curr_loss, energyRMSE, forceRMSE))

                    if epoch % self.logmodel_interval == 0:
                        self.saveModel()

                    if energyRMSE < self.energyRMSEtol and forceRMSE < self.forceRMSEtol:
                        logger.info('Minimization converged')
                        self.saveModel()
                        io.saveFF(self.model, self.preprocessParas, filename="mlff.pyamff")
                        sys.exit()
                prev_loss = curr_loss
            self.saveModel()
            io.saveFF(self.model, self.preprocessParas, filename="mlff.pyamff")

            dist.destroy_process_group()
