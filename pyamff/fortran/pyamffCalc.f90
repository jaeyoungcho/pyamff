MODULE pyamff
    USE fpCalc
    IMPLICIT NONE
    !PRIVATE
    !PUBLIC::calc_eon, calc_ase, tdfps, tfps
    !DOUBLE PRECISION, DIMENSION(:, :, :, :), ALLOCATABLE :: tdfps
    !DOUBLE PRECISION, DIMENSION(:, :), ALLOCATABLE :: tfps
    CONTAINS

    !SUBROUTINE calc_eon(pos_car, nAtoms, ncoords, box, atomicNumbers, F, U, nelement, uniqueNrs) BIND(C,name='calc_eon')
    SUBROUTINE calc_eon(nAtoms, R, box, atomicNumbers, F, U, nelement, uniqueNrs) BIND(C,name='calc_eon')
        USE, INTRINSIC :: iso_c_binding
        !USE fpCalc
        USE nlist
        USE normalize
        USE fnnmodule
        USE nnType
        IMPLICIT NONE

        INTEGER(c_long), INTENT(IN) :: nAtoms
        INTEGER :: NAT, i, j
        REAL(c_double), DIMENSION(nAtoms*3), INTENT(IN) :: R
        REAL(c_double), DIMENSION(9), INTENT(IN) :: box
        REAL(c_double), INTENT(OUT) :: U
        REAL(c_double), DIMENSION(nAtoms*3):: F
        INTEGER(c_int), DIMENSION(nAtoms) :: atomicNumbers

        INTEGER, PARAMETER :: MAX_FPs = 100
        INTEGER, PARAMETER :: MAX_NEIGHS = 100
        INTEGER, PARAMETER ::forceEngine = 1
        DOUBLE PRECISION, DIMENSION(nAtoms,3) :: pos_car
        INTEGER, DIMENSION(nAtoms) :: symbols
        CHARACTER*3, DIMENSION(nAtoms) :: atomicSymbols

        DOUBLE PRECISION, DIMENSION(3,3) :: cell

        INTEGER(c_int) :: nelement
        INTEGER(c_int), DIMENSION(nelement) :: uniqueNrs
        CHARACTER*20 :: filename
        CHARACTER*3, DIMENSION(nelement) :: uniq_elements
        DOUBLE PRECISION, DIMENSION(nAtoms,3) :: pos_dir
        DOUBLE PRECISION, DIMENSION(3,3) :: dir2car

        DOUBLE PRECISION, DIMENSION(nAtoms, MAX_FPS) :: fps
        DOUBLE PRECISION, DIMENSION(nAtoms, MAX_NEIGHS, 3, MAX_FPS) :: dfps
        CHARACTER*3, DIMENSION(92) :: elementArray
        INTEGER, DIMENSION(nAtoms, MAX_NEIGHS) :: neighs
        INTEGER, DIMENSION(nAtoms) :: num_neigh

        DATA elementArray / "H","He","Li","Be","B","C","N","O", &
                 "F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc", &
                 "Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se", &
                 "Br","Kr","Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag", &
                 "Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd", &
                 "Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta", &
                 "W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn", &
                 "Fr","Ra","Ac","Th","Pa","U" /

        DO i = 1, nelement
            ! Read from pos.con
            uniq_elements(i) = elementArray(uniqueNrs(i))
        END DO

        DO i = 1, nAtoms
            DO j = 1, nelement
                IF (atomicNumbers(i) == uniqueNrs(j)) THEN 
                    symbols(i) = j
                END IF
            END DO
        END DO

        cell(1,1) = box(1)
        cell(1,2) = box(2)
        cell(1,3) = box(3)

        cell(2,1) = box(4)
        cell(2,2) = box(5)
        cell(2,3) = box(6)

        cell(3,1) = box(7)
        cell(3,2) = box(8)
        cell(3,3) = box(9)

        DO i = 1, nAtoms
            pos_car(i,1) = R(3*(i-1) + 1)
            pos_car(i,2) = R(3*(i-1) + 2)
            pos_car(i,3) = R(3*(i-1) + 3)
            !print *, "pos_car", pos_car(i,1), pos_car(i,2), pos_car(i,3)
        END DO

        nat = nAtoms
        dfps = 0.0
        fps = 0.0
        !print*, fps
        !print*, 'starting fp calc'
        CALL calcfps(nAt, pos_car, cell, symbols, MAX_FPs, nelement, forceEngine,&
                     fps, dfps, neighs, num_neigh)
        !             neighs, num_neigh)
        CALL normalizeFPs(nelement, nAt, uniq_elements, symbols, MAX_FPS, MAXVAL(num_neigh),&
               num_neigh, neighs, fps, dfps(:,1:MAXVAL(num_neigh)+1,:,:))
        CALL forwardCalc(num_neigh, MAXVAL(num_neigh),neighs(:,1:MAXVAL(num_neigh)), symbols,&
        fps, dfps(:,1:MAXVAL(num_neigh)+1,:,:),&
        MAXVAL(nGs), MAXVAL(natoms_arr), MAXVAL(nhidneurons)) 

        U = Etotal 

        open(unit=2, file='eonfc.dat', ACTION="write", STATUS="replace")

        DO i=1,nAtoms
            F(3*(i-1)+1) = forces(1,i) 
            F(3*(i-1)+2) = forces(2,i)
            F(3*(i-1)+3) = forces(3,i)
            write(2,*) F(3*(i-1)+1), F(3*(i-1)+2), F(3*(i-1)+3)
        END DO
        close(2)
        !CALL nncleanup()
    END SUBROUTINE


    SUBROUTINE calc_ase(nAtoms, pos_car, box, atomicNumbers, F, U, nelement, uniqueNrs) 

        !USE fpCalc
        USE nlist
        USE normalize
        USE fnnmodule
        USE nnType
        IMPLICIT NONE

        INTEGER :: nAtoms
        INTEGER :: NAT, i, j
        !REAL, DIMENSION(nAtoms*3), INTENT(IN) :: R
        DOUBLE PRECISION, DIMENSION(9), INTENT(IN) :: box
        DOUBLE PRECISION, INTENT(OUT) :: U
        DOUBLE PRECISION, DIMENSION(nAtoms,3), INTENT(OUT):: F
        INTEGER, DIMENSION(nAtoms) :: atomicNumbers

        INTEGER, PARAMETER :: MAX_FPs = 100
        INTEGER, PARAMETER :: MAX_NEIGHS = 100
        INTEGER, PARAMETER ::forceEngine = 1
        DOUBLE PRECISION, DIMENSION(nAtoms,3) :: pos_car
        INTEGER, DIMENSION(nAtoms) :: symbols
        CHARACTER*3, DIMENSION(nAtoms) :: atomicSymbols

        DOUBLE PRECISION, DIMENSION(3,3) :: cell

        INTEGER :: nelement
        INTEGER, DIMENSION(nelement) :: uniqueNrs
        CHARACTER*20 :: filename
        CHARACTER*3, DIMENSION(nelement) :: uniq_elements
        DOUBLE PRECISION, DIMENSION(nAtoms,3) :: pos_dir
        DOUBLE PRECISION, DIMENSION(3,3) :: dir2car

        DOUBLE PRECISION, DIMENSION(nAtoms, MAX_FPS) :: fps
        DOUBLE PRECISION, DIMENSION(nAtoms, MAX_NEIGHS, 3, MAX_FPS) :: dfps
        CHARACTER*3, DIMENSION(92) :: elementArray
        INTEGER, DIMENSION(nAtoms, MAX_NEIGHS) :: neighs
        INTEGER, DIMENSION(nAtoms) :: num_neigh

        DATA elementArray / "H","He","Li","Be","B","C","N","O", &
                 "F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc", &
                 "Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se", &
                 "Br","Kr","Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag", &
                 "Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd", &
                 "Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta", &
                 "W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn", &
                 "Fr","Ra","Ac","Th","Pa","U" /

        !ALLOCATE(tfps(2,2))
        DO i = 1, nelement
            ! Read from pos.con
            uniq_elements(i) = elementArray(uniqueNrs(i))
        END DO

        DO i = 1, nAtoms
            DO j = 1, nelement
                IF (atomicNumbers(i) == uniqueNrs(j)) THEN
                    symbols(i) = j
                END IF
            END DO
        END DO
        cell(1,1) = box(1)
        cell(1,2) = box(2)
        cell(1,3) = box(3)

        cell(2,1) = box(4)
        cell(2,2) = box(5)
        cell(2,3) = box(6)

        cell(3,1) = box(7)
        cell(3,2) = box(8)
        cell(3,3) = box(9)

        !DO i=1, nAtoms
        !    pos_car(i,1) = R(3*(i-1) + 1) 
        !    pos_car(i,2) = R(3*(i-1) + 2)
        !    pos_car(i,3) = R(3*(i-1) + 3)
!            print *, "pos_car", pos_car(i,1), pos_car(i,2), pos_car(i,3)
        !END DO

        nat = nAtoms

        dfps = 0.0
        fps = 0.0
        !print*, fps
        CALL calcfps(nAt, pos_car, cell, symbols, MAX_FPs, nelement, forceEngine, &
                     fps, dfps, neighs, num_neigh)
        !print*, tfps(1,1)
        !print*, tfps(1,2)
        !print*, "calASE 1", dfps(1, 1, 1, :)
        !print*, "calASE 2", dfps(2, 1, 1, :)
        !print*, "calASE 3", dfps(3, 1, 1, :)
        CALL normalizeFPs(nelement, nAt, uniq_elements, symbols, MAX_FPS, MAXVAL(num_neigh), &
                          num_neigh, neighs, fps, dfps(:,1:MAXVAL(num_neigh)+1,:,:))
        !print *, 'fps noarmalized'
!        CALL forwardCalc(num_neigh, neighs(:,1:MAXVAL(num_neigh)), fps, dfps(:,1:MAXVAL(num_neigh)+1,:,:), nAt)
!        print *, 'num_neigh', num_neigh
!        print *, "MAXVAL(num_neigh)", MAXVAL(num_neigh)
!        print *, "neighs(:,1:MAXVAL(num_neigh))", neighs(:,1:MAXVAL(num_neigh))
        !print *, "fps", fps
        !print *, "dfps", dfps(:,1:MAXVAL(num_neigh)+1,:,:)
!        print *, "max_ngs", MAXVAL(nGs)
!        print *, "MAXVAL(natoms_arr)", MAXVAL(natoms_arr)
!        print *, "max neur", nhidneurons
        CALL forwardCalc(num_neigh, MAXVAL(num_neigh),neighs(:,1:MAXVAL(num_neigh)), symbols, &
                         fps, dfps(:,1:MAXVAL(num_neigh)+1,:,:), &
                         MAXVAL(nGs), MAXVAL(natoms_arr), MAXVAL(nhidneurons))

!        print *, 'forward is done'

        U = Etotal

        !OPEN(UNIT=2, FILE='eonfc.dat', ACTION="write", STATUS="replace")
        !DO i = 1, nAtoms
        !    WRITE(2,*) forces(:,i)
        !END DO
        !CLOSE(2)
        
        DO i = 1, nAtoms
            F(i,1) = forces(1,i) 
            F(i,2) = forces(2,i)
            F(i,3) = forces(3,i)
        END DO

    END SUBROUTINE


END MODULE

