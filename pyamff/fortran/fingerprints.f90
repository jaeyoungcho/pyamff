MODULE fpCalc
    USE nlist
    USE fbp
    USE fpType
    USE normalize, only: normalizeParas, loadnormalizeParas
    USE nnType, only: natoms_arr, nGs, fpminvs, fpmaxvs, diffs, magnitude, interceptScale, coheEs, use_cohesive_energy, atom_idx
    IMPLICIT NONE
    PRIVATE
    PUBLIC :: read_fpParas, read_mlffParas, calcg2s, calcg1s, calcfps, cleanup, read_mlff
    !DOUBLE PRECISION, DIMENSION(:, :, :, :), ALLOCATABLE :: dfps
    !DOUBLE PRECISION, DIMENSION(:, :), ALLOCATABLE :: fps
    TYPE (fingerprints),DIMENSION(:),ALLOCATABLE,SAVE :: fpParas
    !TYPE (fingerprintsData), DIMENSION(:), ALLOCATABLE :: fpdata
    DOUBLE PRECISION, SAVE :: max_rcut   !fetch rcut from fpParas
    CHARACTER*2, DIMENSION(:), ALLOCATABLE :: uniq_elements
    DOUBLE PRECISION, DIMENSION(:, :), ALLOCATABLE :: rmins

    CONTAINS

!!!!!!-----------------------------------------------------------------------------------!
!!!!!! loopfps: calculate fingerprints for each images based on type of fingerprints
!!!!!!           i.e., G1(H, H)
!!!!!!          symbols: [0, 1, 1, 1]
!!!!!!-----------------------------------------------------------------------------------!
    SUBROUTINE read_fpParas(filename, nelement)

        ! inputs
        CHARACTER*20 :: filename
        INTEGER :: nelement
!f2py   INTENT(IN) :: filename
!f2py   INTENT(IN) :: nelement
        CHARACTER*2 :: G_type
        CHARACTER*2 :: center, neigh1, neigh2
        INTEGER*4 :: i, j, k, accN

        ! variables
        INTEGER :: cidx, tempidx, nidx1, nidx2, nG1, nG2
        INTEGER :: currIndex
        INTEGER :: nFPs
        DOUBLE PRECISION :: eta, junk, Rs, zeta, lambda, thetas, rcut
        CHARACTER*10 :: fp_type
        !CHARACTER*2, DIMENSION(nelement) :: uniq_elements

        ! outputs
        DOUBLE PRECISION, DIMENSION(nelement) :: coeh
!f2py   INTENT(OUT) :: coeh
        !COMMON fpParas, max_rcut

        ! Read fp Parameters from fpParas.dat
        !print *, 'reading', filename
        ALLOCATE(uniq_elements(nelement))
        max_rcut = 0.0
        !Initialize rmins
        ALLOCATE(rmins(nelement, nelement))
        DO i = 1, nelement
            DO j = 1, nelement
                rmins(i, j) = 100.0
                !print*, nelement, i, j
            END DO
        END DO

        OPEN (12, FILE = filename, STATUS = 'old')
        READ (12,*)
        READ (12,*) fp_type
        READ (12,*)
        READ (12,*) uniq_elements
        READ (12,*) coeh
        READ (12,*)
        READ (12,*) nG1, nG2
        !print *, nG1, nG2
        ALLOCATE(fpParas(nelement))
        DO i = 1, nelement
            ALLOCATE(fpParas(i)%g1s(nelement))
            ALLOCATE(fpParas(i)%g2s(nelement, nelement))
            fpParas(i)%tnFPs = 0
            fpParas(i)%g1_startpoint = 0
            fpParas(i)%g1_endpoint = 0
            fpParas(i)%g2_startpoint = 0
            fpParas(i)%g2_endpoint = 0
            DO j = 1, nelement
                fpParas(i)%g1s(j)%startpoint = 0
                fpParas(i)%g1s(j)%endpoint = 0
                fpParas(i)%g1s(j)%nFPs = 0
                fpParas(i)%g1s(j)%currIndex = 0
                DO k = 1, nelement
                    fpParas(i)%g2s(j, k)%startpoint = 0
                    fpParas(i)%g2s(j, k)%endpoint = 0
                    fpParas(i)%g2s(j, k)%nFPs = 0
                    fpParas(i)%g2s(j, k)%currIndex = 0
                END DO
            END DO
        END DO
        !print *, 'allocated'
        IF (nG1 .GT. 0) THEN
            READ (12,*)
            DO i = 1, nG1
                READ (12,*) G_type, center, neigh1, junk, junk, junk
                cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                nidx1 = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))
                !print *, 'nG1 index', center, neigh1, cidx, nidx1
                fpParas(cidx)%g1s(nidx1)%nFPs = fpParas(cidx)%g1s(nidx1)%nFPs + 1
            END DO
            !print *,'nG1', fpParas(1)%g1s(1)%nFPs
        END IF

        ! Count nG2 if there are G2s
        IF (nG2 .GT. 0) THEN
            READ (12,*)
            DO i = 1, nG2
                READ (12,*) G_type, center, neigh1, neigh2, junk, junk, junk, junk, junk
                cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                nidx1 = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))
                nidx2 = find_loc_char(uniq_elements, neigh2, SIZE(uniq_elements))
                fpParas(cidx)%g2s(nidx1, nidx2)%nFPs = fpParas(cidx)%g2s(nidx1, nidx2)%nFPs + 1
                IF (nidx1 .NE. nidx2) THEN
                    fpParas(cidx)%g2s(nidx2, nidx1)%nFPs = fpParas(cidx)%g2s(nidx2, nidx1)%nFPs + 1
                END IF
            END DO
            !print *, fpParas(1)%g2s(1,1)%nFPs
        END IF

        ! ALLOCATE fpParas
        DO i = 1, nelement
            DO j = 1, nelement
                nFPs = fpParas(i)%g1s(j)%nFPs
                ALLOCATE(fpParas(i)%g1s(j)%etas(nFPs))
                ALLOCATE(fpParas(i)%g1s(j)%rss(nFPs))
                ALLOCATE(fpParas(i)%g1s(j)%r_cuts(nFPs))
                DO k = 1, nelement
                    nFPs = fpParas(i)%g2s(j,k)%nFPs
                    IF (nFPs .GT. 0) THEN
                        ALLOCATE(fpParas(i)%g2s(j,k)%etas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%gammas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%lambdas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%zetas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%r_cuts(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%theta_ss(nFPs))
                    ELSE
                        ALLOCATE(fpParas(i)%g2s(j,k)%etas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%gammas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%lambdas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%zetas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%r_cuts(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%theta_ss(nFPs))
                    END IF
                END DO
            END DO
        END DO

        ! Rewind the fpParas.dat file
        REWIND 12
        ! read fp Paras dat again and put data into the right place 
        READ (12,*) !skip first line
        READ (12,*) !skip second line
        READ (12,*) !skip third line
        READ (12,*) !skip third line
        READ (12,*) !skip third line
        READ (12,*) !skip third line
        READ (12,*) !skip third line
        IF (nG1 .GT. 0) THEN
            READ (12,*)
            DO i = 1, nG1
                READ (12,*) G_type, center, neigh1, eta, Rs, rcut
                ! ideally have dynamic rcut for different interactions
                IF (rcut .GT. max_rcut) THEN
                    max_rcut = rcut
                ENDIF
                !print *, G_type, center, neigh1, eta, Rs, rcut
                cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                nidx1 = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))

                fpParas(cidx)%g1s(nidx1)%fp_type = G_type
                fpParas(cidx)%g1s(nidx1)%species1 = center
                fpParas(cidx)%g1s(nidx1)%species1_code = cidx
                fpParas(cidx)%g1s(nidx1)%species2 = neigh1
                fpParas(cidx)%g1s(nidx1)%currIndex = fpParas(cidx)%g1s(nidx1)%currIndex + 1

                currIndex = fpParas(cidx)%g1s(nidx1)%currIndex
                fpParas(cidx)%g1s(nidx1)%etas(currIndex) = eta
                fpParas(cidx)%g1s(nidx1)%rss(currIndex) = Rs
                fpParas(cidx)%g1s(nidx1)%r_cuts(currIndex) = rcut
                fpParas(cidx)%tnFPs = fpParas(cidx)%tnFPs + 1

                IF (nidx1 .EQ. 1) THEN
                    fpParas(cidx)%g1s(nidx1)%startpoint = 1
                    fpParas(cidx)%g1s(nidx1)%endpoint = fpParas(cidx)%g1s(nidx1)%endpoint + 1
                ELSE
                    fpParas(cidx)%g1s(nidx1)%startpoint = fpParas(cidx)%g1s(nidx1-1)%endpoint + 1
                    fpParas(cidx)%g1s(nidx1)%endpoint = fpParas(cidx)%g1s(nidx1)%startpoint + fpParas(cidx)%g1s(nidx1)%nFPs - 1
                END IF
            END DO
        END IF
        !print *, 'nG1 reading done'
        IF (nG2 .GT. 0) THEN
            READ (12,*) !skip #type
            DO k = 1, nelement
                fpParas(k)%g1_endpoint = fpParas(k)%tnFPs
                fpParas(k)%g2_startpoint = fpParas(k)%g1_endpoint + 1
            END DO

            DO i = 1, nG2
                READ (12,*) G_type, center, neigh1, neigh2, eta, zeta, lambda, thetas, rcut

                ! ideally have dynamic rcut for different interactions
                IF (rcut .GT. max_rcut) THEN
                    max_rcut = rcut
                END IF

  !!!!!!!        cidx = FINDLOC(uniq_elements, VALUE=center, DIM=1)
                cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                nidx1 = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))
                nidx2 = find_loc_char(uniq_elements, neigh2, SIZE(uniq_elements))
                !print *, 'nG2 index', center, neigh1, neigh2, cidx, nidx1, nidx2
                IF (nidx1 .GT. nidx2) THEN
                    tempidx = nidx1
                    nidx1 = nidx2
                    nidx2 = tempidx
                END IF
                fpParas(cidx)%g2s(nidx1, nidx2)%fp_type = G_type
                fpParas(cidx)%g2s(nidx1, nidx2)%species1 = center
                fpParas(cidx)%g2s(nidx1, nidx2)%species1_code = cidx
                fpParas(cidx)%g2s(nidx1, nidx2)%species2 = neigh1
                fpParas(cidx)%g2s(nidx1, nidx2)%currIndex = fpParas(cidx)%g2s(nidx1, nidx2)%currIndex + 1
                currIndex = fpParas(cidx)%g2s(nidx1, nidx2)%currIndex

                !print *, 'currIndex', currIndex
                fpParas(cidx)%g2s(nidx1, nidx2)%etas(currIndex)     = eta
                fpParas(cidx)%g2s(nidx1, nidx2)%zetas(currIndex)    = zeta
                fpParas(cidx)%g2s(nidx1, nidx2)%lambdas(currIndex)  = lambda
                fpParas(cidx)%g2s(nidx1, nidx2)%theta_ss(currIndex) = thetas
                fpParas(cidx)%g2s(nidx1, nidx2)%r_cuts(currIndex)   = rcut
                fpParas(cidx)%tnFPs = fpParas(cidx)%tnFPs + 1

                !fpParas(cidx)%g2s(nidx1, nidx2)%nFPs = fpParas(cidx)%g2s(nidx1, nidx2)%nFPs + 1
            END DO
            !print *, 'half done'

            DO i = 1, nelement
                DO j = 1, nelement
                    DO k = j, nelement
                       IF (fpParas(i)%g2s(j, k)%nFPs .EQ. 0) THEN
                            accN = 0
                        ELSE
                            accN = 1
                        END IF
                        IF ((j .EQ. 1) .AND. (k .EQ. 1)) THEN
                            fpParas(i)%g2s(j, k)%startpoint = fpParas(i)%g1_endpoint + accN
                        ELSE
                            IF (j .EQ. k) THEN
                                fpParas(i)%g2s(j, k)%startpoint = fpParas(i)%g2s(j-1, nelement)%endpoint + accN
                            ELSE
                                fpParas(i)%g2s(j, k)%startpoint = fpParas(i)%g2s(j, k-1)%endpoint + accN
                            END IF
                        END IF
                        IF (accN .EQ. 0) THEN
                            fpParas(i)%g2s(j, k)%endpoint = fpParas(i)%g2s(j, k)%startpoint
                        ELSE
                            fpParas(i)%g2s(j, k)%endpoint = fpParas(i)%g2s(j, k)%startpoint + fpParas(i)%g2s(j, k)%nFPs - 1
                        END IF
                    END DO
                END DO
            END DO
            !print *, fpParas(2)%g2s(2,2)%startpoint, fpParas(2)%g2s(2,2)%endpoint
            DO i = 1, nelement
                DO j = 1, nelement
                    DO k = 1, j-1
                        !print *, i, j, k
                        fpParas(i)%g2s(j, k)%etas       =  fpParas(i)%g2s(k, j)%etas
                        fpParas(i)%g2s(j, k)%zetas      =  fpParas(i)%g2s(k, j)%zetas
                        fpParas(i)%g2s(j, k)%lambdas    =  fpParas(i)%g2s(k, j)%lambdas
                        fpParas(i)%g2s(j, k)%theta_ss   =  fpParas(i)%g2s(k, j)%theta_ss
                        fpParas(i)%g2s(j, k)%r_cuts     =  fpParas(i)%g2s(k, j)%r_cuts
                        fpParas(i)%g2s(j, k)%startpoint =  fpParas(i)%g2s(k, j)%startpoint
                        fpParas(i)%g2s(j, k)%endpoint   =  fpParas(i)%g2s(k, j)%endpoint
                    END DO
                END DO
            END DO
            !write(*,*) 'Parameter reading done'
        END IF
        !CLOSE(12)
        !
        !Read mlff.pyamff to obtain fpminvs, and fpmaxvs
        !CALL loadnormalizeParas(nAtoms, nelement, MAX_FPS, symbols, uniq_element)
        !ALLOCATE(magnitude(MAX_FPS, MAXVAL(natoms_arr), nelement))
        !ALLOCATE(interceptScale(MAX_FPS, MAXVAL(natoms_arr), nelement))
        !Store magnitude interceptScale in memory
        !CALL normalizeParas(nelement)

    END SUBROUTINE

    SUBROUTINE read_mlffParas(nAtoms, nelement, MAX_FPS, atomicNumbers, uniqueNrs) BIND(C,name='read_mlffParas')
        USE, INTRINSIC :: iso_c_binding
        ! Inputs
        INTEGER(c_long) :: nAtoms
        INTEGER(c_int) :: nelement, MAX_FPS
        INTEGER(c_int), DIMENSION(nAtoms) :: atomicNumbers
        INTEGER(c_int), DIMENSION(nelement) :: uniqueNrs
        INTEGER, DIMENSION(nAtoms) :: symbols
        CHARACTER*2, DIMENSION(nelement) :: uniq_element

        ! Variables
        INTEGER :: nG1, nG2, numGs, nFPs, cidx, nidx, nidx1, nidx2, tempidx
        INTEGER :: i, j, k, accN, currIndex, m, n
        INTEGER, DIMENSION(nelement) :: g1_endpts, g2_endpts
        DOUBLE PRECISION :: djunk, fpmin, fpmax, eta, Rs, rcut, thetas, zeta, lambda
        CHARACTER*3 :: center, neigh1, neigh2, cjunk
        CHARACTER(LEN=30) :: G_type, line
        CHARACTER*3, DIMENSION(92) :: elementArray

        DATA elementArray / "H","He","Li","Be","B","C","N","O", &
                 "F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc", &
                 "Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se", &
                 "Br","Kr","Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag", &
                 "Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd", &
                 "Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta", &
                 "W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn", &
                 "Fr","Ra","Ac","Th","Pa","U" /

        CHARACTER(len=2), DIMENSION(nelement) :: coheElement
        DOUBLE PRECISION, DIMENSION(nelement) :: temp_coheEs
        
        ALLOCATE(coheEs(nelement))
        use_cohesive_energy = .FALSE.

        !filename = 'fpParas.dat'
        !print*, 'allocating rmins'
        ALLOCATE(rmins(nelement, nelement))
        DO i = 1, nelement
            ! Read from pos.con
            uniq_element(i) = elementArray(uniqueNrs(i))
            DO j = 1, nelement
                rmins(i, j) = 0.0
            END DO
        END DO

        DO i = 1, nAtoms
            DO j = 1, nelement
                IF (atomicNumbers(i) == uniqueNrs(j)) THEN
                    symbols(i) = j
                END IF
            END DO
        END DO

        ! Outputs
        !DOUBLE PRECISION, DIMENSION(MAX_FPS, nelement) :: fpminvs, fpmaxvs, diffs

        ALLOCATE (uniq_elements(nelement))
        uniq_elements = uniq_element
        ALLOCATE(natoms_arr(nelement))
        ALLOCATE(nGs(nelement))
        ALLOCATE(fpminvs(MAX_FPS, nelement))
        ALLOCATE(fpmaxvs(MAX_FPS, nelement))
        ALLOCATE(diffs(MAX_FPS, nelement))
        g1_endpts = 0
        g2_endpts = 0
        nGs = 0
        fpminvs = 0
        fpmaxvs = 0
        diffs = 0

        DO k = 1, nelement
           natoms_arr(k) = COUNT(symbols .EQ. k)
        END DO

        ALLOCATE(fpParas(nelement))
        DO i = 1, nelement
            ALLOCATE(fpParas(i)%g1s(nelement))
            ALLOCATE(fpParas(i)%g2s(nelement, nelement))
            fpParas(i)%tnFPs = 0
            fpParas(i)%g1_startpoint = 0
            fpParas(i)%g1_endpoint = 0
            fpParas(i)%g2_startpoint = 0
            fpParas(i)%g2_endpoint = 0
            DO j = 1, nelement
                fpParas(i)%g1s(j)%startpoint = 0
                fpParas(i)%g1s(j)%endpoint = 0
                fpParas(i)%g1s(j)%nFPs = 0
                fpParas(i)%g1s(j)%currIndex = 0
                DO k = 1, nelement
                    fpParas(i)%g2s(j, k)%startpoint = 0
                    fpParas(i)%g2s(j, k)%endpoint = 0
                    fpParas(i)%g2s(j, k)%nFPs = 0
                    fpParas(i)%g2s(j, k)%currIndex = 0
                END DO
            END DO
        END DO
        OPEN (11, FILE='mlff.pyamff', status='old')
        !print*, 'reading mlparas'
        READ (11,*) !skip #Fingerprint type
        READ (11,*) !fp_type
        READ (11,*) !Rmins
        !print*, 'reading Rmins'
        DO i = 1, nelement
            READ (11,*) (rmins(i,j), j = 1, nelement)
            !READ (11,*) r(i,1), r(i,2)
            !print*, 'rmins', rmins(i,1), rmins(i,2)
        END DO

        ! Mai edit
        !!!!!! If there is a #Cohesive tag
        ! Read the cohesives energies into the array
        !print*, 'Counting G1s'
        DO WHILE (line .NE. "#MachineLearning")
            READ (11,*) line! can be #  type or #Cohesive 
            IF (line .EQ. "#Cohesive") THEN
                use_cohesive_energy = .TRUE.
                READ (11,*) coheElement
                READ (11,*) temp_coheEs
                DO m = 1, nelement
                    n = find_loc_char(coheElement, uniq_element(m), nelement)
                    coheEs(m) = temp_coheEs(n)
                END DO
                READ (11,*) line! skip # type 
            END IF
            IF (line .EQ. "#MachineLearning") GOTO 40
            READ (11,*) G_type, numGs
            IF (G_type .EQ. 'G1') THEN
                nG1 = numGs
                READ (11,*) !skip # center neighbor ...
                DO i = 1, nG1
                    READ (11,*) center, neigh1, djunk, djunk, djunk, djunk, djunk
                    cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                    nidx = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))
                    nGs(cidx) = nGs(cidx) + 1
                    fpParas(cidx)%g1s(nidx)%nFPs = fpParas(cidx)%g1s(nidx)%nFPs + 1
                END DO
            ELSE IF (G_type .EQ. 'G2') THEN
                nG2 = numGs
                READ (11,*) !skip # center neighbor ...
                DO i = 1, nG2
                    READ (11,*) center, neigh1, neigh2, djunk, djunk, djunk, djunk, djunk, djunk, djunk
                    cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                    nidx1 = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))
                    nidx2 = find_loc_char(uniq_elements, neigh2, SIZE(uniq_elements))
                    nGs(cidx) = nGs(cidx) + 1
                    fpParas(cidx)%g2s(nidx1, nidx2)%nFPs = fpParas(cidx)%g2s(nidx1, nidx2)%nFPs + 1
                    IF (nidx1 .NE. nidx2) THEN
                        fpParas(cidx)%g2s(nidx2, nidx1)%nFPs = fpParas(cidx)%g2s(nidx2, nidx1)%nFPs + 1
                    END IF
                END DO
            END IF
        END DO

  40    REWIND 11
        !print*, 'allocating fpParas'
        ! ALLOCATE fpParas
        DO i = 1, nelement
            DO j = 1, nelement
                nFPs = fpParas(i)%g1s(j)%nFPs
                ALLOCATE(fpParas(i)%g1s(j)%etas(nFPs))
                ALLOCATE(fpParas(i)%g1s(j)%rss(nFPs))
                ALLOCATE(fpParas(i)%g1s(j)%r_cuts(nFPs))
                DO k = 1, nelement
                    nFPs = fpParas(i)%g2s(j,k)%nFPs
                    !IF (nFPs .GT. 0) THEN
                    ALLOCATE(fpParas(i)%g2s(j,k)%etas(nFPs))
                    ALLOCATE(fpParas(i)%g2s(j,k)%gammas(nFPs))
                    ALLOCATE(fpParas(i)%g2s(j,k)%lambdas(nFPs))
                    ALLOCATE(fpParas(i)%g2s(j,k)%zetas(nFPs))
                    ALLOCATE(fpParas(i)%g2s(j,k)%r_cuts(nFPs))
                    ALLOCATE(fpParas(i)%g2s(j,k)%theta_ss(nFPs))
                    !END IF
                END DO
            END DO
        END DO

        READ (11,*) !skip #Fingerprint type
        READ (11,*) !fp_type 
        READ (11,*) !Rmins
        DO i = 1, nelement
            READ (11,*)
        END DO

         ! Mai edit: Read the cohesive Energy
         !!!!!!!!! If there is a cohesive tag in mlff.pyamff
         ! read the cohesive energies into the array

        READ (11,*) line!  can be #  type or #Cohesive
        IF (line .EQ. "#Cohesive") THEN
            READ (11,*)
            READ (11,*)
            READ (11,*) ! # type
        END IF
        READ (11,*) G_type, numGs
        !print*, 'G_type', G_type, numGs
        IF (nG1 .GT. 0) THEN
            READ (11,*) !skip center ...
            DO i = 1, nG1
                READ (11,*) center, neigh1, eta, Rs, rcut, fpmin, fpmax
                ! ideally have dynamic rcut for different interactions
                IF (rcut .GT. max_rcut) THEN
                    max_rcut = rcut
                END IF
                cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                nidx1 = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))
                fpParas(cidx)%g1s(nidx1)%fp_type = G_type
                fpParas(cidx)%g1s(nidx1)%species1 = center
                fpParas(cidx)%g1s(nidx1)%species1_code = cidx
                fpParas(cidx)%g1s(nidx1)%species2 = neigh1
                fpParas(cidx)%g1s(nidx1)%currIndex = fpParas(cidx)%g1s(nidx1)%currIndex + 1
                currIndex = fpParas(cidx)%g1s(nidx1)%currIndex
                fpParas(cidx)%g1s(nidx1)%etas(currIndex) = eta
                fpParas(cidx)%g1s(nidx1)%rss(currIndex) = Rs
                fpParas(cidx)%g1s(nidx1)%r_cuts(currIndex) = rcut
                fpParas(cidx)%tnFPs = fpParas(cidx)%tnFPs + 1
                IF (cidx .EQ. 1) THEN
                    fpminvs(i,cidx) = fpmin
                    fpmaxvs(i,cidx) = fpmax
                    g1_endpts(cidx) = g1_endpts(cidx) + 1
                ELSE
                    fpminvs(i-g1_endpts(cidx-1), cidx) = fpmin
                    fpmaxvs(i-g1_endpts(cidx-1), cidx) = fpmax
                    g1_endpts(cidx) = g1_endpts(cidx) + 1
                END IF

                IF (nidx1 .EQ. 1) THEN
                    fpParas(cidx)%g1s(nidx1)%startpoint = 1
                    fpParas(cidx)%g1s(nidx1)%endpoint = fpParas(cidx)%g1s(nidx1)%endpoint + 1
                ELSE
                    fpParas(cidx)%g1s(nidx1)%startpoint = fpParas(cidx)%g1s(nidx1-1)%endpoint + 1
                    fpParas(cidx)%g1s(nidx1)%endpoint = fpParas(cidx)%g1s(nidx1)%startpoint + fpParas(cidx)%g1s(nidx1)%nFPs - 1
                END IF
            END DO
        END IF
        !print*, 'reading G2s'
        IF (nG2 .GT. 0) THEN
            IF (nG1 .GT. 0) THEN
                READ (11,*) !skip type ...
                READ (11,*) G_type, numGs
            ELSE
                CONTINUE
            END IF    
            !print *, G_type, numGs
            DO k = 1, nelement
                fpParas(k)%g1_endpoint = fpParas(k)%tnFPs
                fpParas(k)%g2_startpoint = fpParas(k)%g1_endpoint + 1
            END DO
            READ (11,*) !skip # center...
            DO i = 1, nG2
                READ (11,*) center, neigh1, neigh2, eta, zeta, lambda, thetas, rcut, fpmin, fpmax
                ! ideally have dynamic rcut for different interactions
                IF (rcut .GT. max_rcut) THEN
                    max_rcut = rcut
                END IF
                cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                nidx1 = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))
                nidx2 = find_loc_char(uniq_elements, neigh2, SIZE(uniq_elements))
                IF (nidx1 .GT. nidx2) THEN
                    tempidx = nidx1
                    nidx1 = nidx2
                    nidx2 = tempidx
                END IF
                fpParas(cidx)%g2s(nidx1, nidx2)%fp_type = G_type
                fpParas(cidx)%g2s(nidx1, nidx2)%species1 = center
                fpParas(cidx)%g2s(nidx1, nidx2)%species1_code = cidx
                fpParas(cidx)%g2s(nidx1, nidx2)%species2 = neigh1
                fpParas(cidx)%g2s(nidx1, nidx2)%currIndex = fpParas(cidx)%g2s(nidx1, nidx2)%currIndex + 1
                currIndex = fpParas(cidx)%g2s(nidx1, nidx2)%currIndex
 
                fpParas(cidx)%g2s(nidx1, nidx2)%etas(currIndex)     = eta
                fpParas(cidx)%g2s(nidx1, nidx2)%zetas(currIndex)    = zeta
                fpParas(cidx)%g2s(nidx1, nidx2)%lambdas(currIndex)  = lambda
                fpParas(cidx)%g2s(nidx1, nidx2)%theta_ss(currIndex) = thetas
                fpParas(cidx)%g2s(nidx1, nidx2)%r_cuts(currIndex)   = rcut
                fpParas(cidx)%tnFPs = fpParas(cidx)%tnFPs + 1

                IF (cidx .EQ. 1) THEN
                    fpminvs(g1_endpts(cidx)+i,cidx) = fpmin
                    fpmaxvs(g1_endpts(cidx)+i,cidx) = fpmax
                    g2_endpts(cidx) = g2_endpts(cidx) + 1
                ELSE
                    fpminvs(g1_endpts(cidx)+i-g2_endpts(cidx-1),cidx) = fpmin
                    fpmaxvs(g1_endpts(cidx)+i-g2_endpts(cidx-1),cidx) = fpmax
                    g2_endpts(cidx) = g2_endpts(cidx) + 1
                END IF
            END DO

            DO i = 1, nelement
                DO j = 1, nelement
                    DO k = j, nelement
                        IF (fpParas(i)%g2s(j, k)%nFPs .EQ. 0) THEN
                            accN = 0
                        ELSE
                            accN = 1
                        END IF

                        IF ((j .EQ. 1) .AND. (k .EQ. 1)) THEN
                            fpParas(i)%g2s(j, k)%startpoint = fpParas(i)%g1_endpoint + accN
                        ELSE
                            IF (j .EQ. k) THEN
                                fpParas(i)%g2s(j, k)%startpoint = fpParas(i)%g2s(j-1, nelement)%endpoint+accN
                            ELSE
                                fpParas(i)%g2s(j, k)%startpoint = fpParas(i)%g2s(j, k-1)%endpoint+accN
                            END IF
                        END IF
                        IF (accN .EQ. 0) THEN
                            fpParas(i)%g2s(j, k)%endpoint = fpParas(i)%g2s(j, k)%startpoint
                        ELSE
                            fpParas(i)%g2s(j, k)%endpoint = fpParas(i)%g2s(j, k)%startpoint + fpParas(i)%g2s(j, k)%nFPs - 1
                        END IF
                    END DO
                END DO
            END DO
            !print *, fpParas(2)%g2s(2,2)%startpoint, fpParas(2)%g2s(2,2)%endpoint
            !print*, 'G2 reading done'
            DO i = 1, nelement
                DO j = 1, nelement
                    DO k = 1, j - 1
                        !print *, i, j, k
                        IF (fpParas(i)%g2s(j,k)%nFPs .GT. 0) THEN
                            fpParas(i)%g2s(j, k)%etas       =  fpParas(i)%g2s(k, j)%etas
                            fpParas(i)%g2s(j, k)%zetas      =  fpParas(i)%g2s(k, j)%zetas
                            fpParas(i)%g2s(j, k)%lambdas    =  fpParas(i)%g2s(k, j)%lambdas
                            fpParas(i)%g2s(j, k)%theta_ss   =  fpParas(i)%g2s(k, j)%theta_ss
                            fpParas(i)%g2s(j, k)%r_cuts     =  fpParas(i)%g2s(k, j)%r_cuts
                            fpParas(i)%g2s(j, k)%startpoint =  fpParas(i)%g2s(k, j)%startpoint
                            fpParas(i)%g2s(j, k)%endpoint   =  fpParas(i)%g2s(k, j)%endpoint
                        END IF
                    END DO
                END DO
            END DO
        END IF 
                
        !Allocate atom_idx here 
        ALLOCATE(atom_idx(MAXVAL(natoms_arr),nelement))
        !print*, 'Allocating mags'
        ALLOCATE(magnitude(MAX_FPS, MAXVAL(natoms_arr), nelement))
        ALLOCATE(interceptScale(MAX_FPS, MAXVAL(natoms_arr), nelement))
        magnitude = 0
        interceptScale = 0
        !Store magnitude interceptScale in memory
        CALL normalizeParas(nelement)
        !print*, 'reading mlParas Done'
    END SUBROUTINE

    SUBROUTINE read_mlff(nAtoms, nelement, MAX_FPS, atomicNumbers, uniqueNrs)

        ! Inputs
        INTEGER :: nAtoms
        INTEGER :: nelement, MAX_FPS
        INTEGER, DIMENSION(nAtoms) :: atomicNumbers
        INTEGER, DIMENSION(nelement) :: uniqueNrs
        INTEGER, DIMENSION(nAtoms) :: symbols
        CHARACTER*2, DIMENSION(nelement) :: uniq_element

        ! Variables
        INTEGER :: nG1, nG2, numGs, nFPs, cidx, nidx, nidx1, nidx2, tempidx
        INTEGER :: i, j, k, accN, currIndex, m, n
        INTEGER, DIMENSION(nelement) :: g1_endpts, g2_endpts
        DOUBLE PRECISION :: djunk, fpmin, fpmax, eta, Rs, rcut, thetas, zeta, lambda
        CHARACTER*3 :: center, neigh1, neigh2, cjunk
        CHARACTER(LEN=30) :: G_type, line
        CHARACTER*3, DIMENSION(92) :: elementArray

        DATA elementArray / "H","He","Li","Be","B","C","N","O", &
                 "F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc", &
                 "Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se", &
                 "Br","Kr","Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag", &
                 "Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd", &
                 "Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta", &
                 "W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn", &
                 "Fr","Ra","Ac","Th","Pa","U" /

        CHARACTER(len=2), DIMENSION(nelement) :: coheElement
        DOUBLE PRECISION, DIMENSION(nelement) :: temp_coheEs

        ALLOCATE(coheEs(nelement))
        use_cohesive_energy = .FALSE.

        !filename = 'fpParas.dat'
        nG1 = 0
        nG2 = 0
        ALLOCATE(rmins(nelement, nelement))
        DO i = 1, nelement
             ! Read from pos.con
             uniq_element(i) = elementArray(uniqueNrs(i))
             DO j = 1, nelement
                 rmins(i, j) = 0.0
             END DO
        END DO

        DO i = 1, nAtoms
            DO j = 1, nelement
                IF (atomicNumbers(i) == uniqueNrs(j)) THEN
                    symbols(i) = j
                END IF
            END DO
        END DO

        ! Outputs
        !DOUBLE PRECISION, DIMENSION(MAX_FPS, nelement) :: fpminvs, fpmaxvs, diffs

        ALLOCATE (uniq_elements(nelement))
        uniq_elements = uniq_element
        ALLOCATE(natoms_arr(nelement))
        ALLOCATE(nGs(nelement))
        ALLOCATE(fpminvs(MAX_FPS, nelement))
        ALLOCATE(fpmaxvs(MAX_FPS, nelement))
        ALLOCATE(diffs(MAX_FPS, nelement))
        g1_endpts = 0
        g2_endpts = 0
        nGs = 0
        fpminvs = 0
        fpmaxvs = 0
        diffs = 0

        DO k = 1, nelement
            natoms_arr(k) = COUNT(symbols .EQ. k)
        END DO
        ALLOCATE(fpParas(nelement))
        DO i = 1, nelement
            ALLOCATE(fpParas(i)%g1s(nelement))
            ALLOCATE(fpParas(i)%g2s(nelement, nelement))
            fpParas(i)%tnFPs = 0
            fpParas(i)%g1_startpoint = 0
            fpParas(i)%g1_endpoint = 0
            fpParas(i)%g2_startpoint = 0
            fpParas(i)%g2_endpoint = 0
            DO j = 1, nelement
                fpParas(i)%g1s(j)%startpoint = 0
                fpParas(i)%g1s(j)%endpoint = 0
                fpParas(i)%g1s(j)%nFPs = 0
                fpParas(i)%g1s(j)%currIndex = 0
                DO k = 1, nelement
                    fpParas(i)%g2s(j, k)%startpoint = 0
                    fpParas(i)%g2s(j, k)%endpoint = 0
                    fpParas(i)%g2s(j, k)%nFPs = 0
                    fpParas(i)%g2s(j, k)%currIndex = 0
                END DO
            END DO
        END DO
        OPEN (11, FILE='mlff.pyamff', status='old')
        READ (11,*) !skip #Fingerprint type
        READ (11,*) !fp_type 
        READ (11,*) !Rmins
        !print*, 'reading Rmins'
        DO i = 1, nelement
            READ (11,*) (rmins(i,j), j = 1, nelement)
            !READ (11,*) r(i,1), r(i,2)
            !print*, 'rmins', rmins(i,1), rmins(i,2)
        END DO

        ! Mai edit: Read the cohesive Energy
        !!!!!!!!! If there is a #Cohesive tag
        ! read the cohesive energies to the array

        DO WHILE (line .NE. "#MachineLearning")
            READ (11,*) line! can be #  type or #Cohesive 
            IF (line .EQ. "#Cohesive") THEN
                use_cohesive_energy = .TRUE.
                READ (11,*) coheElement
                READ (11,*) temp_coheEs
                DO m = 1, nelement
                    n = find_loc_char(coheElement, uniq_element(m), nelement)
                    coheEs(m) = temp_coheEs(n)
                END DO
                READ (11,*) line ! skip # type 
            END IF
            IF (line .EQ. "#MachineLearning") GOTO 40
            READ (11,*) G_type, numGs
            IF (G_type .EQ. 'G1') THEN
                nG1 = numGs
                READ (11,*) ! skip # center neighbor ...
                DO i = 1, nG1
                    READ (11,*) center, neigh1, djunk, djunk, djunk, djunk, djunk
                    cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                    nidx = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))
                    nGs(cidx) = nGs(cidx) + 1
                    fpParas(cidx)%g1s(nidx)%nFPs = fpParas(cidx)%g1s(nidx)%nFPs + 1
                END DO

            ELSE IF (G_type .EQ. 'G2') THEN
                nG2 = numGs
                READ (11,*) ! skip # center neighbor ...
                DO i = 1, nG2
                    READ (11,*) center, neigh1, neigh2, djunk, djunk, djunk, djunk, djunk, djunk, djunk
                    cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                    nidx1 = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))
                    nidx2 = find_loc_char(uniq_elements, neigh2, SIZE(uniq_elements))
                    nGs(cidx) = nGs(cidx) + 1
                    fpParas(cidx)%g2s(nidx1, nidx2)%nFPs = fpParas(cidx)%g2s(nidx1, nidx2)%nFPs + 1

                    IF (nidx1 .NE. nidx2) THEN
                        fpParas(cidx)%g2s(nidx2, nidx1)%nFPs = fpParas(cidx)%g2s(nidx2, nidx1)%nFPs + 1
                    END IF
                END DO
            END IF
        END DO

  40    REWIND 11

        ! ALLOCATE fpParas
        DO i = 1, nelement
            DO j = 1, nelement
                nFPs = fpParas(i)%g1s(j)%nFPs
                ALLOCATE(fpParas(i)%g1s(j)%etas(nFPs))
                ALLOCATE(fpParas(i)%g1s(j)%rss(nFPs))
                ALLOCATE(fpParas(i)%g1s(j)%r_cuts(nFPs))
                DO k = 1, nelement
                    nFPs = fpParas(i)%g2s(j,k)%nFPs
                    IF (nFPs .GT. 0) THEN
                        ALLOCATE(fpParas(i)%g2s(j,k)%etas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%gammas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%lambdas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%zetas(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%r_cuts(nFPs))
                        ALLOCATE(fpParas(i)%g2s(j,k)%theta_ss(nFPs))
                        fpParas(i)%g2s(j,k)%etas = 0.0
                        fpParas(i)%g2s(j,k)%gammas = 0.0
                        fpParas(i)%g2s(j,k)%lambdas = 0.0
                        fpParas(i)%g2s(j,k)%zetas = 0.0
                        fpParas(i)%g2s(j,k)%r_cuts = 0.0
                        fpParas(i)%g2s(j,k)%theta_ss = 0.0
                    ENDIF
                    !print*, 'fpParas', fpParas(i)%g2s(j,k)%etas
                END DO
            END DO
        END DO
   
        READ (11,*) !skip #Fingerprint type
        READ (11,*) !fp_type 
        READ (11,*) !Rmins
        DO i = 1, nelement
            READ (11,*) 
        END DO

         ! Mai edit: Read the cohesive Energy
         !!!!!!!!! If there is a #Cohesive tag
         ! read the cohesive energies to the array

         !print*, 'reading g1'
        READ (11,*) line ! can be #  type or #Cohesive
        IF (line .EQ. "#Cohesive") THEN
            READ (11,*)
            READ (11,*)
            READ (11,*) !skip # type
        END IF
        READ (11,*) G_type, numGs
        IF (nG1 .GT. 0) THEN
            READ (11,*) !skip center ...
            DO i = 1, nG1
                READ (11,*) center, neigh1, eta, Rs, rcut, fpmin, fpmax
                ! ideally have dynamic rcut for different interactions
                IF (rcut .GT. max_rcut) THEN
                    max_rcut = rcut
                END IF
                cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                nidx1 = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))
                fpParas(cidx)%g1s(nidx1)%fp_type = G_type
                fpParas(cidx)%g1s(nidx1)%species1 = center
                fpParas(cidx)%g1s(nidx1)%species1_code = cidx
                fpParas(cidx)%g1s(nidx1)%species2 = neigh1
                fpParas(cidx)%g1s(nidx1)%currIndex = fpParas(cidx)%g1s(nidx1)%currIndex + 1
                currIndex = fpParas(cidx)%g1s(nidx1)%currIndex
                fpParas(cidx)%g1s(nidx1)%etas(currIndex) = eta
                fpParas(cidx)%g1s(nidx1)%rss(currIndex) = Rs
                fpParas(cidx)%g1s(nidx1)%r_cuts(currIndex) = rcut
                fpParas(cidx)%tnFPs = fpParas(cidx)%tnFPs + 1
                IF (cidx .EQ. 1) THEN
                    fpminvs(i,cidx) = fpmin
                    fpmaxvs(i,cidx) = fpmax
                    g1_endpts(cidx) = g1_endpts(cidx) + 1
                ELSE
                    fpminvs(i-g1_endpts(cidx-1), cidx) = fpmin
                    fpmaxvs(i-g1_endpts(cidx-1), cidx) = fpmax
                    g1_endpts(cidx) = g1_endpts(cidx) + 1
                END IF

                IF (nidx1 .EQ. 1) THEN
                    fpParas(cidx)%g1s(nidx1)%startpoint = 1
                    fpParas(cidx)%g1s(nidx1)%endpoint = fpParas(cidx)%g1s(nidx1)%endpoint + 1
                ELSE
                    fpParas(cidx)%g1s(nidx1)%startpoint = fpParas(cidx)%g1s(nidx1-1)%endpoint + 1
                    fpParas(cidx)%g1s(nidx1)%endpoint = fpParas(cidx)%g1s(nidx1)%startpoint + fpParas(cidx)%g1s(nidx1)%nFPs - 1
                END IF
            END DO
        END IF

        !print*, 'reading G2s'
        IF (nG2 .GT. 0) THEN
            IF (nG1 .GT. 0) THEN
                READ (11,*) !skip type ...
                READ (11,*) G_type, numGs
            ELSE
                CONTINUE
            END IF    
            !print *, G_type, numGs
            DO k = 1, nelement
                fpParas(k)%g1_endpoint = fpParas(k)%tnFPs
                fpParas(k)%g2_startpoint = fpParas(k)%g1_endpoint + 1
            END DO
            READ (11,*) !skip # center...
            DO i = 1, nG2
                READ (11,*) center, neigh1, neigh2, eta, zeta, lambda, thetas, rcut, fpmin, fpmax
                ! ideally have dynamic rcut for different interactions
                IF (rcut .GT. max_rcut) THEN
                    max_rcut = rcut
                END IF
                cidx = find_loc_char(uniq_elements, center, SIZE(uniq_elements))
                nidx1 = find_loc_char(uniq_elements, neigh1, SIZE(uniq_elements))
                nidx2 = find_loc_char(uniq_elements, neigh2, SIZE(uniq_elements))
                IF (nidx1 .GT. nidx2) THEN
                    tempidx = nidx1
                    nidx1 = nidx2
                    nidx2 = tempidx
                END IF
                fpParas(cidx)%g2s(nidx1, nidx2)%fp_type = G_type
                fpParas(cidx)%g2s(nidx1, nidx2)%species1 = center
                fpParas(cidx)%g2s(nidx1, nidx2)%species1_code = cidx
                fpParas(cidx)%g2s(nidx1, nidx2)%species2 = neigh1
                fpParas(cidx)%g2s(nidx1, nidx2)%currIndex = fpParas(cidx)%g2s(nidx1, nidx2)%currIndex + 1
                currIndex = fpParas(cidx)%g2s(nidx1, nidx2)%currIndex

                fpParas(cidx)%g2s(nidx1, nidx2)%etas(currIndex)     = eta
                fpParas(cidx)%g2s(nidx1, nidx2)%zetas(currIndex)    = zeta
                fpParas(cidx)%g2s(nidx1, nidx2)%lambdas(currIndex)  = lambda
                fpParas(cidx)%g2s(nidx1, nidx2)%theta_ss(currIndex) = thetas
                fpParas(cidx)%g2s(nidx1, nidx2)%r_cuts(currIndex)   = rcut
                fpParas(cidx)%tnFPs = fpParas(cidx)%tnFPs + 1

                IF (cidx .EQ. 1) THEN
                    fpminvs(g1_endpts(cidx)+i,cidx) = fpmin
                    fpmaxvs(g1_endpts(cidx)+i,cidx) = fpmax
                    g2_endpts(cidx) = g2_endpts(cidx) + 1
                ELSE
                    fpminvs(g1_endpts(cidx)+i-g2_endpts(cidx-1),cidx) = fpmin
                    fpmaxvs(g1_endpts(cidx)+i-g2_endpts(cidx-1),cidx) = fpmax
                    g2_endpts(cidx) = g2_endpts(cidx) + 1
                END IF
            END DO

            DO i = 1, nelement
                DO j = 1, nelement
                    DO k = j, nelement
                        IF (fpParas(i)%g2s(j, k)%nFPs .EQ. 0) THEN
                            accN = 0
                        ELSE
                            accN = 1
                        END IF

                        IF ((j .EQ. 1) .AND. (k .EQ. 1)) THEN
                            fpParas(i)%g2s(j, k)%startpoint = fpParas(i)%g1_endpoint + accN
                        ELSE
                            IF (j .EQ. k) THEN
                                fpParas(i)%g2s(j, k)%startpoint = fpParas(i)%g2s(j-1, nelement)%endpoint+accN
                            ELSE
                                fpParas(i)%g2s(j, k)%startpoint = fpParas(i)%g2s(j, k-1)%endpoint+accN
                            END IF
                        END IF
                        IF (accN .EQ. 0) THEN
                            fpParas(i)%g2s(j, k)%endpoint = fpParas(i)%g2s(j, k)%startpoint
                        ELSE
                            fpParas(i)%g2s(j, k)%endpoint = fpParas(i)%g2s(j, k)%startpoint + fpParas(i)%g2s(j, k)%nFPs - 1
                        END IF
                    END DO
                END DO
            END DO
            !print *, fpParas(2)%g2s(2,2)%startpoint, fpParas(2)%g2s(2,2)%endpoint
            DO i = 1, nelement
                DO j = 1, nelement
                    DO k = 1, j - 1
                        !print *, i, j, k
                        IF (fpParas(i)%g2s(j,k)%nFPs .GT. 0) THEN
                            fpParas(i)%g2s(j, k)%etas       =  fpParas(i)%g2s(k, j)%etas
                            fpParas(i)%g2s(j, k)%zetas      =  fpParas(i)%g2s(k, j)%zetas
                            fpParas(i)%g2s(j, k)%lambdas    =  fpParas(i)%g2s(k, j)%lambdas
                            fpParas(i)%g2s(j, k)%theta_ss   =  fpParas(i)%g2s(k, j)%theta_ss
                            fpParas(i)%g2s(j, k)%r_cuts     =  fpParas(i)%g2s(k, j)%r_cuts
                            fpParas(i)%g2s(j, k)%startpoint =  fpParas(i)%g2s(k, j)%startpoint
                            fpParas(i)%g2s(j, k)%endpoint   =  fpParas(i)%g2s(k, j)%endpoint
                        END IF
                    END DO
                END DO
            END DO
        END IF
        !Allocate atom_idx here 
        ALLOCATE(atom_idx(MAXVAL(natoms_arr),nelement))
        !Allocate magnitudes and interceptScale array here
        ALLOCATE(magnitude(MAX_FPS, MAXVAL(natoms_arr), nelement))
        ALLOCATE(interceptScale(MAX_FPS, MAXVAL(natoms_arr), nelement))
        magnitude = 0.0
        interceptScale = 0.0
        !Store magnitude interceptScale in memory
        CALL normalizeParas(nelement)

    END SUBROUTINE


    SUBROUTINE calcfps(nAtoms, pos_car, cell, symbols, max_fps, nelement,forceEngine, &
                       fps, dfps, neighs, num_neigh)

        ! Parameters
        INTEGER, PARAMETER :: MAX_NEIGHS  = 100
        !INTEGER, PARAMETER :: MAX_ANGLES = 1024

        ! input values
        !CHARACTER*20, INTENT(IN) :: filename  !file that stores fingerprint parameters
        INTEGER :: nAtoms, nelement
        INTEGER :: forceEngine
        INTEGER*4, DIMENSION(nAtoms) :: symbols
        DOUBLE PRECISION, DIMENSION(nAtoms,3) :: pos_car
        DOUBLE PRECISION, DIMENSION(3,3) :: cell
        INTEGER :: MAX_FPS  !find max number of fingerprints, element-based
        !CHARACTER*2, INTENT(IN), DIMENSION(nelement) :: uniq_elements
!f2py   INTENT(IN) :: pos_car
!f2py   INTENT(IN) :: cell
!f2py   INTENT(IN) :: symbols
!f2py   INTENT(IN) :: forceEngine
!f2py   INTENT(IN) :: nelement
!!!!f2py    INTENT(IN) :: nAtoms, nelement
!f2py   INTENT(IN) :: MAX_FPS  !find max number of fingerprints, element-based

        ! variables for nlist.calc
        INTEGER :: j, k, l
        INTEGER :: npairs, maxneighs
        !INTEGER, DIMENSION(nAtoms, MAX_NEIGHS) :: neighs
        DOUBLE PRECISION,DIMENSION(nAtoms*MAX_NEIGHS) :: pairs 
        INTEGER, DIMENSION(2, nAtoms*MAX_NEIGHS) :: pair_indices 
        INTEGER, DIMENSION(nAtoms) :: pair_start, pair_end
        DOUBLE PRECISION,DIMENSION(2, nAtoms*MAX_NEIGHS, 3) :: unitvects_pair

        ! variables for calcg1s
        !TYPE (fingerprints),SAVE, DIMENSION(nelement) :: fpParas
        !TYPE (fingerprintsData), DIMENSION(nAtoms) :: fpdata

        !COMMON fpParas, max_rcut

        ! variables for calcTriplet
        INTEGER :: ntriplets
        DOUBLE PRECISION, DIMENSION(3, nAtoms*MAX_NEIGHS*MAX_NEIGHS) :: angles
        INTEGER, DIMENSION(3, nAtoms*MAX_NEIGHS*MAX_NEIGHS) :: angle_indices
        DOUBLE PRECISION, DIMENSION(3, 3) :: vectsigns

        ! variables for DataToMatrix

        ! output values
        DOUBLE PRECISION, DIMENSION(nAtoms, MAX_FPS) :: fps
        DOUBLE PRECISION, DIMENSION(nAtoms, MAX_NEIGHS, 3, MAX_FPS) :: dfps
        INTEGER, DIMENSION(nAtoms, MAX_NEIGHS) :: neighs
        INTEGER, DIMENSION(nAtoms) :: num_neigh
!f2py   INTENT(OUT) :: fps
!f2py   INTENT(OUT) :: dfps
!f2py   INTENT(OUT) :: neighs
!f2py   INTENT(OUT) :: num_neigh

        vectsigns = reshape((/1, 1, -1, 1, -1, -1, 1, -1, 1/), [3,3])

        !ALLOCATE(rmins(nelement, nelement))
        CALL calcNlist(nAtoms, MAX_NEIGHS, pos_car, cell, max_rcut, nelement, symbols, forceEngine, rmins, &
                       npairs, num_neigh, neighs, pairs, pair_indices, pair_start, pair_end, unitvects_pair)
        maxneighs = MAXVAL(num_neigh)

        !ALLOCATE(tfps(2,2))
        !tfps=1.0
        !ALLOCATE(fpdata(nAtoms))
        !ALLOCATE(fps(nAtoms, MAX_FPS))
        !ALLOCATE(dfps(nAtoms, maxneighs+1, 3, MAX_FPS))
        !dfps = 0.0
        !fps = 0.0
        !DO j = 1, natoms
        !  ALLOCATE(fpdata(j)%gs(fpParas(symbols(j))%tnFPs))
        !  ALLOCATE(fpdata(j)%dgdxs(1:num_neigh(j)+1))
        !  ALLOCATE(fpdata(j)%dgdxs(1)%dgdx(1:3, 1:fpParas(symbols(j))%tnFPs))
        !  fpdata(j)%dgdxs(1)%dgdx = 0.0
        !  DO k = 2, num_neigh(j)+1
        !    ALLOCATE(fpdata(j)%dgdxs(k)%dgdx(1:3, 1:fpParas(symbols(neighs(j,k-1)))%tnFPs)) 
        !    fpdata(j)%dgdxs(k)%dgdx = 0.0
        !  ENDDO
        !  fpdata(j)%gs = 0
        !ENDDO
        !CALL calcg1s(natoms, npairs, symbols, maxneighs, neighs(:, 1:maxneighs), &
        !         pairs(1:npairs), pair_indices(:,1:npairs), unitvects_pair(:, 1:npairs, :))
        CALL calcg1s(natoms, npairs, symbols, maxneighs, max_fps, neighs(:, 1:maxneighs), & 
                     num_neigh,&
                     pairs(1:npairs), pair_indices(:,1:npairs), unitvects_pair(:, 1:npairs, :), &
                     fps, dfps(:, 1:MAXNEIGHS+1, :, :))
        !print*, "1", dfps(1, 1, 1, :)
        !print*, "2", dfps(2, 1, 1, :)
        !print*, "3", dfps(3, 1, 1, :)
        !print *, 'calculating triplets'
        CALL calcTriplet(nAtoms,MAX_NEIGHS, npairs, pair_indices(2,1:npairs), pair_start, pair_end,&
        unitvects_pair(1, 1:npairs, :), &
        ntriplets, angles, angle_indices)
        !CALL calcg2s(natoms, nelement, npairs, ntriplets,num_neigh, maxneighs, symbols, fpParas, max_rcut, neighs(:, 1:maxneighs),  pairs(1:npairs), pair_indices(:, 1:npairs), pair_start, pair_end, unitvects_pair(1,1:npairs, :),vectsigns, angles(:, 1:ntriplets), angle_indices(:, 1:ntriplets), &
        !      fpdata)
        CALL calcg2s(natoms, npairs, ntriplets, maxneighs, max_fps, symbols, max_rcut, neighs(:, 1:maxneighs), &
                     num_neigh, pairs(1:npairs), pair_indices(:, 1:npairs), unitvects_pair(1,1:npairs, :), &
                     vectsigns, angles(:, 1:ntriplets), angle_indices(:, 1:ntriplets), &
                     fps, dfps(:, 1:MAXNEIGHS+1, :, :))
        !print *, 'fps'
        !DO j = 1, natoms
        !  print *, j, fpdata(j)%gs
        !ENDDO

        !CALL DataToMatrix(nAtoms, MAX_NEIGHS, MAX_FPS, neighs(:, 1:maxneighs), num_neigh, symbols, &
        !fps, dfps)
        !dfps(nAtoms, 1:maxneighs+1, 3, MAX_FPS))

        !print *, 'fpsCalc done'
        !DO j = 1, natoms
        !  print *, j, fps(j,:)
        !ENDDO

        !print *, 'dfps main'
        !DO j = 1, natoms
        !  DO k = 1, 3
        !    DO l = 1,num_neigh(j)+1
        !      IF (l==1) THEN
        !        print *, j-1, j-1, k-1, dfps(j, l, k, :)
        !      ELSE
        !        print *, j-1, neighs(j,l-1)-1, k-1, dfps(j, l, k, :)
        !      ENDIF
        !    ENDDO
        !  ENDDO
        !ENDDO
        !print*, 'fpdata deallocated'
        !write(*,*) 'Fingerprint calculation is done'
    END SUBROUTINE

       !SUBROUTINE calcg1s(natoms, nelement, npairs, symbols, fps, num_neigh, &
       !maxneighs, neighs, pairs, pair_indices, unitvects_pair, &
       !  fpdata)
    SUBROUTINE calcg1s(natoms, npairs, symbols, &
               maxneighs, MAX_FPS, neighs, num_neigh, &
               pairs, pair_indices, unitvects_pair, &
               fps, dfps)

         ! input values
         INTEGER :: nAtoms, npairs, maxneighs, max_fps
         INTEGER, DIMENSION(nAtoms) :: symbols
         INTEGER, DIMENSION(nAtoms, maxneighs) :: neighs
         DOUBLE PRECISION, DIMENSION(npairs) :: pairs  
         INTEGER, DIMENSION(2, npairs) :: pair_indices 
         DOUBLE PRECISION, DIMENSION(2, npairs, 3) :: unitvects_pair
         INTEGER :: nfps
         INTEGER, DIMENSION(nAtoms) :: num_neigh
         !TYPE (fingerprints), DIMENSION(nelement) :: fps

         ! variables
         INTEGER :: i, k, j
         INTEGER :: center_1, index_1, sp_1, sp_2, ep_1, ep_2
         INTEGER :: center_2, index_2
         INTEGER, DIMENSION(1) :: neighloc_1, neighloc_2
         !TYPE (fingerprintsData), DIMENSION(2, npairs) :: fpdata_temp
         TYPE (fingerprintsData), DIMENSION(2) :: fpdata_temp
         ! outputs
         DOUBLE PRECISION, DIMENSION(nAtoms, MAX_FPS) :: fps
         DOUBLE PRECISION, DIMENSION(nAtoms, MAXNEIGHS+1, 3, MAX_FPS) :: dfps

         !Loop over all pairs: Calculate fps and common parts in dfps
         !print *, shape(pair_indices)
         !fps = 0.0
         !dfps = 0.0
         DO i = 1, npairs
             !centered on indices_1(i)
             !print *, 'i', i, npairs
             index_1 = pair_indices(1,i)
             index_2 = pair_indices(2,i)
             center_1 = symbols(pair_indices(1,i))
             center_2 = symbols(pair_indices(2,i))
             IF (center_1 == 0) THEN
                 CYCLE
             END IF
             sp_1 = fpParas(center_1)%g1s(center_2)%startpoint
             ep_1 = fpParas(center_1)%g1s(center_2)%endpoint
             nFPs = fpParas(center_1)%g1s(center_2)%nFPs
             IF (nFPs .EQ. 0) THEN
                 CYCLE
             END IF
             !print *, center_1, center_2, nFPs
             !possible optimization: allocate the size based on the pair only not in total number of fps
             ALLOCATE(fpdata_temp(1)%gs(fpParas(center_1)%tnFPs))  
             ALLOCATE(fpdata_temp(1)%pre_dgdxs(fpParas(center_1)%tnFPs))
             fpdata_temp(1)%gs = 0
             fpdata_temp(1)%pre_dgdxs = 0
             !print *, 'pair', i, index_1, index_2
             !print *,i, pairs(i)
             !print *, fpParas(center_2)%g1s(center_1)%etas
             !print *, fpParas(center_2)%g1s(center_1)%rss
             !print *, fpParas(center_2)%g1s(center_1)%r_cuts
             CALL fg1(pairs(i), nFPs, &
                      fpParas(center_1)%g1s(center_2)%etas, &
                      fpParas(center_1)%g1s(center_2)%rss, &
                      fpParas(center_1)%g1s(center_2)%r_cuts, &
                      fpdata_temp(1)%gs(sp_1:ep_1))
             CALL fdg1(pairs(i), nFPs, &
                       fpParas(center_1)%g1s(center_2)%etas, &
                       fpParas(center_1)%g1s(center_2)%rss, &
                       fpParas(center_1)%g1s(center_2)%r_cuts, &
                       fpdata_temp(1)%pre_dgdxs(sp_1:ep_1))
             !print*, 'g1 fdg1'
             !centered on indices_2(i)
             IF (center_1 .EQ. center_2) THEN
                 ALLOCATE(fpdata_temp(2)%gs(fpParas(center_2)%tnFPs))
                 ALLOCATE(fpdata_temp(2)%pre_dgdxs(fpParas(center_2)%tnFPs))
                 sp_2 = sp_1
                 ep_2 = ep_1
                 fpdata_temp(2)%gs = fpdata_temp(1)%gs
                 fpdata_temp(2)%pre_dgdxs = fpdata_temp(1)%pre_dgdxs
             ELSE
                 sp_2 = fpParas(center_2)%g1s(center_1)%startpoint
                 ep_2 = fpParas(center_2)%g1s(center_1)%endpoint
                 nFPs = fpParas(center_2)%g1s(center_1)%nFPs
                 ALLOCATE(fpdata_temp(2)%gs(fpParas(center_2)%tnFPs))
                 ALLOCATE(fpdata_temp(2)%pre_dgdxs(fpParas(center_2)%tnFPs))
                 fpdata_temp(2)%gs = 0
                 fpdata_temp(2)%pre_dgdxs = 0 
                 CALL fg1(pairs(i), nFPs, &
                          fpParas(center_2)%g1s(center_1)%etas, &
                          fpParas(center_2)%g1s(center_1)%rss, &
                          fpParas(center_2)%g1s(center_1)%r_cuts, &
                          fpdata_temp(2)%gs(sp_2:ep_2))
                 CALL fdg1(pairs(i), nFPs, &
                           fpParas(center_2)%g1s(center_1)%etas, &
                           fpParas(center_2)%g1s(center_1)%rss, &
                           fpParas(center_2)%g1s(center_1)%r_cuts, &
                           fpdata_temp(2)%pre_dgdxs(sp_2:ep_2))
             END IF

             ! fingerprints
             !fpdata(index_1)%gs(sp_1:ep_1) = fpdata(index_1)%gs(sp_1:ep_1) + &
             !                           fpdata_temp(1)%gs(sp_1:ep_1)

             !fpdata(index_2)%gs(sp_2:ep_2) = fpdata(index_2)%gs(sp_2:ep_2) + &
             !                           fpdata_temp(2)%gs(sp_2:ep_2)
             fps(index_1, sp_1:ep_1) = fps(index_1, sp_1:ep_1) + &
                                        fpdata_temp(1)%gs(sp_1:ep_1)

             fps(index_2, sp_2:ep_2) = fps(index_2, sp_2:ep_2) + &
                                        fpdata_temp(2)%gs(sp_2:ep_2) 
             !derivative
             !print *, 'temp fp'
             !print *, '  ',fpdata_temp(1)%pre_dgdxs
!!!!!!        neighloc_1 = FINDLOC(neighs(index_1,:), VALUE=index_2) + 1 
             neighloc_1 = find_loc_int(neighs(index_1,:), index_2, SIZE(neighs(index_1,:))) + 1
!!!!!!        neighloc_2 = FINDLOC(neighs(index_2,:), VALUE=index_1) + 1
             neighloc_2 = find_loc_int(neighs(index_2,:), index_1, SIZE(neighs(index_2,:))) +1
             !print*, 'Pair #:',i
             DO k = 1, 3
                 !center on itself
                 !fpdata(index_1)%dgdxs(1)%dgdx(k,sp_1:ep_1) = &
                 !               fpdata(index_1)%dgdxs(1)%dgdx(k, sp_1:ep_1) - &
                 !               fpdata_temp(1)%pre_dgdxs(sp_1:ep_1) * &
                 !               unitvects_pair(1, i, k)
                 dfps(index_1, 1, k, sp_1:ep_1) = &
                                dfps(index_1, 1, k, sp_1:ep_1) - &
                                fpdata_temp(1)%pre_dgdxs(sp_1:ep_1) * &
                                unitvects_pair(1, i, k)
                 !print*,'  i1:', index_1, '1', k, sp_1, ep_1 
                 !print*,'     ', fpdata(index_1)%dgdxs(1)%dgdx(k,sp_1:ep_1), "dfps", dfps(index_1, 1, k, sp_1:ep_1)
                 !print*,'  i2:', index_2, '1', k, sp_2, ep_2 
                 !print*,'     before', fpdata(index_2)%dgdxs(1)%dgdx(k,sp_2:ep_2), dfps(index_2, 1, k,sp_2:ep_2)
                 !fpdata(index_2)%dgdxs(1)%dgdx(k,sp_2:ep_2) = &
                 !               fpdata(index_2)%dgdxs(1)%dgdx(k,sp_2:ep_2) - &
                 !               fpdata_temp(2)%pre_dgdxs(sp_2:ep_2) * &
                 !               unitvects_pair(2, i, k)

                 dfps(index_2, 1, k,sp_2:ep_2) = &
                                dfps(index_2, 1, k,sp_2:ep_2) - &
                                fpdata_temp(2)%pre_dgdxs(sp_2:ep_2) * &
                                unitvects_pair(2, i, k)
                 !print*,'     after', fpdata(index_2)%dgdxs(1)%dgdx(k,sp_2:ep_2), "dfps", dfps(index_2, 1, k, sp_2:ep_2)
                 !Center on index_2 but w.r.t. index_1
                 !fpdata(index_1)%dgdxs(neighloc_1(1))%dgdx(k,sp_2:ep_2) = fpdata_temp(2)%pre_dgdxs(sp_2:ep_2) * &
                 !                                  unitvects_pair(2, i, k)
                 dfps(index_1, neighloc_1(1), k,sp_2:ep_2) = fpdata_temp(2)%pre_dgdxs(sp_2:ep_2) * &
                                                   unitvects_pair(2, i, k)
                 !print*,'  o2:', index_1, neighloc_1(1), k, sp_2, ep_2
                 !print*,'             ', dfps(index_1, neighloc_1(1), k,sp_2:ep_2)
                 !Center on index_1 but w.r.t. index_2
                 !fpdata(index_2)%dgdxs(neighloc_2(1))%dgdx(k,sp_1:ep_1) = fpdata_temp(1)%pre_dgdxs(sp_1:ep_1) * &
                 !                                  unitvects_pair(1, i, k)
                 dfps(index_2, neighloc_2(1), k,sp_1:ep_1) = fpdata_temp(1)%pre_dgdxs(sp_1:ep_1) * &
                                                   unitvects_pair(1, i, k)
                 !print*,'  o1', index_2, neighloc_2(1), k, sp_1, ep_1
                 !print*,'             ', dfps(index_2, neighloc_2(1), k,sp_1:ep_1)
             END DO
             !print*, "pair", i, dfps(1,1,1,:)
             DEALLOCATE(fpdata_temp(1)%gs)
             DEALLOCATE(fpdata_temp(1)%pre_dgdxs)
             DEALLOCATE(fpdata_temp(2)%gs)
             DEALLOCATE(fpdata_temp(2)%pre_dgdxs)
         END DO
         !print *, 'fps'
         !DO i = 1, natoms
         !    print *, i, fpdata(i)%gs
         !END DO
         !print *, 'dfps'
         !DO i = 1, natoms
         !    DO j = 1, 3
         !        DO k = 1,num_neigh(i)+1
         !            print *, i, k, j
         !            print *, '         ', fpdata(i)%dgdxs(k)%dgdx(j,:)
         !            print *, '         ', dfps(i,k,j,:)
         !        END DO
         !    END DO
         !END DO
    END SUBROUTINE


    SUBROUTINE calcg2s(natoms, npairs, ntriplets, maxneighs, max_fps, symbols, r_cut, neighs, num_neigh, &
                       pairs, pair_indices, unitvects, vectsigns, angles, angles_indices, fps, dfps)

        ! Define inputs
        INTEGER :: nAtoms, npairs, ntriplets, maxneighs, max_fps
        DOUBLE PRECISION :: r_cut
        INTEGER, DIMENSION(nAtoms) :: symbols
        INTEGER, DIMENSION(nAtoms, maxneighs) :: neighs
        !TYPE (fingerprints), DIMENSION(nelement) :: fps
        DOUBLE PRECISION, DIMENSION(npairs) :: pairs
        DOUBLE PRECISION, DIMENSION(npairs, 3) :: unitvects
        DOUBLE PRECISION, DIMENSION(3,3) :: vectsigns
        INTEGER, DIMENSION(2, npairs) :: pair_indices
        DOUBLE PRECISION, DIMENSION(3, ntriplets) :: angles
        INTEGER, DIMENSION(3, ntriplets) :: angles_indices
        INTEGER, DIMENSION(nAtoms) :: num_neigh

        ! Define parameters
        !DOUBLE PRECISION :: pi_rc, inv_Rc2
        !COMMON pi_rc, inv_Rc2

        ! Define variable
        INTEGER :: i, j, k
        INTEGER :: sp, ep, nFPs
        INTEGER, DIMENSION(1) :: neigh1_loc, neigh2_loc
        INTEGER, DIMENSION(3) :: center, pairIndex, csymbol
        INTEGER, DIMENSION(3,2) :: neighbor
        INTEGER, DIMENSION(2) :: currneighs
        DOUBLE PRECISION :: fexpt, fct
        DOUBLE PRECISION, DIMENSION(npairs) :: pair_sins, fcuts, fexp
        INTEGER, DIMENSION(3,3) :: indices, vertices
        DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: f_thetas_1, f_thetas, f_rs
        !TYPE (fingerprintsData), DIMENSION(3, ntriplets) :: fpdata_temp
        !TYPE (fingerprintsData), DIMENSION :: fpdata_temp
        DOUBLE PRECISION, DIMENSION(:, :, :), allocatable :: arm
        DOUBLE PRECISION, DIMENSION(:), allocatable :: gs

        ! Define outputs
        !TYPE (fingerprintsData), DIMENSION(nAtoms) :: fpdata
        DOUBLE PRECISION, DIMENSION(nAtoms, MAX_FPS) :: fps
        DOUBLE PRECISION, DIMENSION(nAtoms, MAXNEIGHS+1, 3, MAX_FPS) :: dfps

        !dfps = 0.0
        !fps = 0.0
        ! Loop over all angles and calculate common parts
        CALL commonparts(npairs, pairs, r_cut,&
                          pair_sins, fcuts, fexp)
        DO i = 1, ntriplets
            ! fetch the pair index for each pair in triplet
            pairIndex(1) = angles_indices(1, i)
            pairIndex(2) = angles_indices(2, i)
            pairIndex(3) = angles_indices(3, i)

            ! fetch the center index for each angle in triplet
            center(1) = pair_indices(1, pairIndex(1))
            center(2) = pair_indices(2, pairIndex(1))
            center(3) = pair_indices(2, pairIndex(2))

            ! record the indices of vertex of the triplet
            vertices(1, :) = center
            vertices(2, :) = [center(2), center(3), center(1)]
            vertices(3, :) = [center(3), center(1), center(2)]

            csymbol = symbols(center)
            !neighbor(1) = csymbol(2) + csymbol(3)
            !neighbor(2) = csymbol(1) + csymbol(3)
            !neighbor(3) = csymbol(1) + csymbol(2)
            neighbor(1,:) = [csymbol(2), csymbol(3)]
            neighbor(2,:) = [csymbol(1), csymbol(3)]
            neighbor(3,:) = [csymbol(1), csymbol(2)]

            fexpt = PRODUCT(fexp(pairIndex))
            fct   = PRODUCT(fcuts(pairIndex))

            indices(1, :) = pairIndex
            indices(2, :) = pairIndex([3, 1, 2])  !j, k, i
            !indices(2, :) = pairIndex([1, 3, 2])  !j, k, i
            indices(3, :) = pairIndex([2, 3, 1])  !k, i, j
            !indices(3, :) = pairIndex([2, 3, 1])  !k, i, j

            DO j = 1, 3
                currneighs = neighbor(j,:)
                nFPs = fpParas(csymbol(j))%g2s(currneighs(1),currneighs(2))%nFPs
                IF (nFPs == 0) THEN
                    CYCLE
                END IF

                sp = fpParas(csymbol(j))%g2s(currneighs(1),currneighs(2))%startpoint
                ep = fpParas(csymbol(j))%g2s(currneighs(1),currneighs(2))%endpoint

                ALLOCATE(gs(fpParas(csymbol(j))%tnFPs))
                ALLOCATE(arm(3,3, fpParas(csymbol(j))%tnFPs))
                ALLOCATE(f_thetas_1(nFPs))
                ALLOCATE(f_thetas(nFPs))
                ALLOCATE(f_rs(nFPs))
                arm = 0.0
                f_thetas = 0.0
                f_rs = 0.0
                CALL fg2(nFPs, &
                         fpParas(csymbol(j))%g2s(currneighs(1),currneighs(2))%theta_ss, &
                         fpParas(csymbol(j))%g2s(currneighs(1),currneighs(2))%etas, &
                         fpParas(csymbol(j))%g2s(currneighs(1),currneighs(2))%zetas, &
                         fpParas(csymbol(j))%g2s(currneighs(1),currneighs(2))%lambdas, &
                         angles(j, i), fexpt, fct, &
                         gs(sp:ep), &
                         f_thetas_1, f_thetas, f_rs)

                CALL fdg2(nFPs, &
                         fpParas(csymbol(j))%g2s(currneighs(1),currneighs(2))%theta_ss, &
                         fpParas(csymbol(j))%g2s(currneighs(1),currneighs(2))%etas, &
                         fpParas(csymbol(j))%g2s(currneighs(1),currneighs(2))%zetas, &
                         fpParas(csymbol(j))%g2s(currneighs(1),currneighs(2))%lambdas, &
                         r_cut, &
                         angles(j, i), &
                         f_thetas_1, &
                         f_thetas, &
                         f_rs, &
                         pairs(indices(j,:)), &
                         fcuts(indices(j, :)), &
                         pair_sins(indices(j, :)), &
                         unitvects(indices(j, :), :), &
                         vectsigns(j,:), &
                         arm(1, :, sp:ep), &
                         arm(2, :, sp:ep), &
                         arm(3, :, sp:ep))
                DEALLOCATE(f_thetas_1)
                DEALLOCATE(f_thetas)
                DEALLOCATE(f_rs)

                !fpdata(center(j))%gs(sp:ep) = fpdata(center(j))%gs(sp:ep) + gs(sp:ep)
                fps(center(j), sp:ep) = fps(center(j), sp:ep) + gs(sp:ep)

                ! center on itself
                !fpdata(center(j))%dgdxs(1)%dgdx(:, sp:ep) = &
                !                  fpdata(center(j))%dgdxs(1)%dgdx(:, sp:ep) + &
                !                  arm(1, :, sp:ep)

                dfps(center(j), 1, :, sp:ep) = &
                                  dfps(center(j), 1, :, sp:ep) + &
                                  arm(1, :, sp:ep)
                
                !neigh1_loc = FINDLOC(neighs(vertices(j, 2),:), VALUE=center(j)) + 1
                neigh1_loc = find_loc_int(neighs(vertices(j, 2),:), center(j), SIZE(neighs(vertices(j, 2),:))) + 1
                !neigh2_loc = FINDLOC(neighs(vertices(j, 3),:), VALUE=center(j)) + 1
                neigh2_loc = find_loc_int(neighs(vertices(j, 3),:), center(j), SIZE(neighs(vertices(j, 3),:))) +1
                ! contr. to neighbor 1
                !print *, 'contrto', vertices(j, 2), 'from', center(j), 'located', neigh1_loc(1)
                !print *,'neigh1 size', neighs(vertices(j, 2),:)
                !fpdata(vertices(j, 2))%dgdxs(neigh1_loc(1))%dgdx(:, sp:ep) = &
                !                  fpdata(vertices(j, 2))%dgdxs(neigh1_loc(1))%dgdx(:, sp:ep) + &
                !                  arm(2, :, sp:ep)
                dfps(vertices(j, 2),neigh1_loc(1), :, sp:ep) = &
                                  dfps(vertices(j, 2),neigh1_loc(1), :, sp:ep) + &
                                  arm(2, :, sp:ep)
                !print *, fpdata(vertices(j, 2))%dgdxs(neigh1_loc(1))%dgdx(1, sp:ep)
                ! contr. to neighbor 2
                !print *, 'contrto', vertices(j, 3), 'from', center(j), 'located', neigh2_loc(1)
                !print *,'neigh2 size', neighs(vertices(j, 3),:)
                !fpdata(vertices(j, 3))%dgdxs(neigh2_loc(1))%dgdx(:, sp:ep) = &
                !                  fpdata(vertices(j, 3))%dgdxs(neigh2_loc(1))%dgdx(:, sp:ep) + &
                !                  arm(3, :, sp:ep)
                dfps(vertices(j, 3), neigh2_loc(1), :, sp:ep) = &
                                  dfps(vertices(j, 3), neigh2_loc(1), :, sp:ep) + &
                                  arm(3, :, sp:ep)
                DEALLOCATE(gs)
                DEALLOCATE(arm)

            END DO
        END DO

        !print *, 'fps'
        !DO i = 1, natoms
        !    print *, i, fpdata(i)%gs
        !END DO

        !print *, 'dfps g2s'
        !DO i = 1, natoms
        !    DO j = 1, 3
        !        DO k = 1,num_neigh(i)+1
        !            print *, i, k, j
        !            print *, '         ', fpdata(i)%dgdxs(k)%dgdx(j,:)
        !            print *, '         ', dfps(i,k,j,:)
        !        END DO
        !    END DO
        !END DO
        !print *, 'done'
    END SUBROUTINE

       !SUBROUTINE DataToMatrix(nAtoms, nelement, maxneighs, maxfps, neighs, num_neigh, symbols, fpParas, fpdata, &
       !  fps, dfps)
       !SUBROUTINE DataToMatrix(nAtoms, maxneighs, maxfps, neighs, num_neigh, symbols, &
       !  fps, dfps)

       ! inputs
       !  INTEGER :: nAtoms, maxneighs, maxfps
       !  INTEGER, DIMENSION(nAtoms) :: num_neigh
       !  INTEGER, DIMENSION(nAtoms) :: symbols
       !  INTEGER, DIMENSION(nAtoms, maxneighs) :: neighs
         !TYPE (fingerprintsData), DIMENSION(nAtoms) :: fpdata
         !TYPE (fingerprints), DIMENSION(nelement) :: fpParas

       ! variables
       !  INTEGER :: i, j, k, ep

       ! outputs
       !  DOUBLE PRECISION, DIMENSION(nAtoms, maxfps) :: fps
       !  DOUBLE PRECISION, DIMENSION(nAtoms, maxneighs, 3, maxfps) :: dfps

       !  print *, 'toMatrix'
       !  DO i = 1, natoms
       !    ep = fpParas(symbols(i))%tnFPs
           !print *, 'ep', ep
           !print *, fpdata(i)%gs
       !    print *, 'fps fetching', i
       !    fps(i, 1:ep) = fpdata(i)%gs
           !print *, 'fps fetched'
           !print *, 'ep', ep
       !    DO j = 1, 3
       !      DO k = 1, num_neigh(i)+1
       !        IF (k > 1) THEN
       !          ep = fpParas(symbols(neighs(i,k-1)))%tnFPs
       !        ELSE
       !          ep = fpParas(symbols(i))%tnFPs
       !        ENDIF
       !        print *, 'dfps fetching', i, k, j, ep
               !print *,'  ', fpdata(i)%dgdxs(k)%dgdx(j,:)
       !        dfps(i, k, j, 1:ep) = fpdata(i)%dgdxs(k)%dgdx(j,:)
               !print *, '    ', dfps(i, k, j, 1:ep)
               !print *, dfps(i,k,j,i:ep)
       !        print *, 'dfps fetched'
       !      ENDDO
       !    ENDDO
       !  ENDDO

       !  print *, 'deallocating fpdata'
       !  DO j = 1, natoms
           !print *, 'gs deallocating', j
       !    DEALLOCATE(fpdata(j)%gs)
           !print *, 'gs deallocated', j
       !    DO k = 1, num_neigh(j)+1
       !      print *, 'dgs deallocating', j, k
       !      print *, ' ', num_neigh(j)
             !print *, fpdata(j)%dgdxs(k)%dgdx
       !      IF (k .EQ. 1) THEN
       !        DEALLOCATE(fpdata(j)%dgdxs(1)%dgdx)
               !print*,j,k
       !      ELSE
       !        print*,ALLOCATED(fpdata(j)%dgdxs(k)%dgdx)
       !        DEALLOCATE(fpdata(j)%dgdxs(k)%dgdx)
       !      ENDIF
       !      print *, 'dgs deallocated', j, k
       !    ENDDO
       !  ENDDO
       !  DEALLOCATE(fpdata)
         !print *, 'toMatrix Done'

       !END SUBROUTINE

    SUBROUTINE cleanup() BIND(C,name='cleanup')
         USE, INTRINSIC :: iso_c_binding
         DEALLOCATE(fpParas)
         DEALLOCATE(uniq_elements)
         DEALLOCATE(rmins)
         !DO i = 1, natoms
         !  print *, 'deallocating', i
         !  IF (fpParas(symbols(i))%tnFPs .GT. 0) THEN
         !    DEALLOCATE(fpdata(i)%gs)
         !    DEALLOCATE(fpdata(i)%dgdxs)
         !  ENDIF
           !DEALLOCATE(fpdata(j)%dgdxs(1)%dgdx)
           !DO k = 2, num_neigh(j)+1
           !  DEALLOCATE(fpdata(j)%dgdxs(k)%dgdx)
           !ENDDO

         !ENDDO
         !print *,'cleanup calculation done'
    END SUBROUTINE

END MODULE
