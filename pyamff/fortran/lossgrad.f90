MODULE lossgrad
  use nnType
  use trainType
  IMPLICIT NONE
  
  CONTAINS

  SUBROUTINE init_backward
    IMPLICIT NONE
    !Allocate arrays
    ALLOCATE(natomsE(nimages))
    ALLOCATE(inputE(nimages))
    ALLOCATE(targetE(nimages))
    ALLOCATE(natomsF(nAtimg))
    ALLOCATE(inputF(3,nAtimg))
    ALLOCATE(targetF(3,nAtimg))
    ! TODO:Followings should be generalized w.r.t multiple images
    ALLOCATE(layer_backgrad(MAXVAL(natoms_arr),MAXVAL(nhidneurons),MAXVAL(nhidneurons),nhidlayers,nelements))
    ALLOCATE(bias_grad(MAXVAL(nhidneurons),nhidlayers+1,nelements))
    ALLOCATE(weight_grad(max(MAXVAL(nGs),MAXVAL(nhidneurons)),MAXVAL(nhidneurons),nhidlayers+1,nelements))
    ALLOCATE(all_neurons(MAXVAL(natoms_arr),MAXVAL(nhidneurons),nhidlayers+1,nelements,nimages))
    ALLOCATE(input_fps(MAXVAL(natoms_arr),MAXVAL(nGs),nelements,nimages))
    
    ! Zero arrays
    layer_backgrad=0
    bias_grad=0
    weight_grad=0
    all_neurons=0
    input_fps=0

  END SUBROUTINE

  SUBROUTINE backward
    !variables
    INTEGER :: i, k, l, img, j
   
    ! Zero arrays
    layer_backgrad=0
    bias_grad=0
    weight_grad=0

    DO i=1, nelements
      DO img=1, nimages
        !print *, 'img=',img
        IF (nhidlayers==1) THEN
          ! No hidweights
          GOTO 10
        ELSE  
          layer_backgrad(1:natoms_arr(i),1:nhidneurons(1),1:nhidneurons(2),1,i) = &
          layer_backgrad(1:natoms_arr(i),1:nhidneurons(1),1:nhidneurons(2),1,i)+ &
          backwardgrad(all_neurons(1:natoms_arr(i),1:nhidneurons(1),1,i,img),&
          hid_weights(1:nhidneurons(1),1:nhidneurons(2),1,i),natoms_arr(i),nhidneurons(2),nhidneurons(1))
          IF (nhidlayers==2) THEN
            ! Only one hidweight
            GOTO 10 
          ELSE
            DO l=1, nhidlayers-2
              layer_backgrad(1:natoms_arr(i),1:nhidneurons(l+1),1:nhidneurons(l+2),l+1,i)=&
              layer_backgrad(1:natoms_arr(i),1:nhidneurons(l+1),1:nhidneurons(l+2),l+1,i)+&
              backwardgrad(all_neurons(1:natoms_arr(i),1:nhidneurons(l+1),l+1,i,img),&
              hid_weights(1:nhidneurons(l+1),1:nhidneurons(l+2),l+1,i),natoms_arr(i),nhidneurons(l+2),nhidneurons(l+1))
            END DO
          END IF
        END IF
 10     CONTINUE 
        layer_backgrad(1:natoms_arr(i),1:nhidneurons(nhidlayers),:1,nhidlayers,i)=&
        layer_backgrad(1:natoms_arr(i),1:nhidneurons(nhidlayers),:1,nhidlayers,i)+&
        backwardgrad(all_neurons(1:natoms_arr(i),1:nhidneurons(nhidlayers),nhidlayers,i,img),&
        out_weights(1:nhidneurons(nhidlayers),:1,i),natoms_arr(i),1,nhidneurons(nhidlayers))
      END DO
      !average over nimages
      layer_backgrad=layer_backgrad/nimages
    END DO
   
    !Calculate bias grad first
    DO i=1, nelements
      !TODO
      !1.Include forceloss
      !2.Debug with pytorch
      
      ! Bias grad of output layer
      bias_grad(1,nhidlayers+1,i) = SUM(2*((inputE-targetE)/natomsE))*slope
      IF (nelements == 1) THEN
        CONTINUE
      ELSE
        ! Follow pytorch way 
        bias_grad(1,nhidlayers+1,i)=bias_grad(1,nhidlayers+1,i)*dble(natoms_arr(i))/dble(total_natoms)
      END IF

      ! Bias grad of hidden layers and input layer
      DO l=nhidlayers,1,-1
        IF (l==nhidlayers) THEN
          DO k=1, natoms_arr(i)
            bias_grad(1:nhidneurons(l),l,i)= bias_grad(1:nhidneurons(l),l,i)+&
            layer_backgrad(k,1:nhidneurons(l),1,l,i)*bias_grad(1,l+1,i)
          END DO
          !average
          bias_grad(1:nhidneurons(l),l,i)=bias_grad(1:nhidneurons(l),l,i)/(natoms_arr(i))
        ELSE
          DO k=1, natoms_arr(i)
            bias_grad(1:nhidneurons(l),l,i)=bias_grad(1:nhidneurons(l),l,i)+&
            MATMUL(layer_backgrad(k,1:nhidneurons(l),1:nhidneurons(l+1),l,i),bias_grad(1:nhidneurons(l+1),l+1,i))
          END DO 
          !average over natoms
          bias_grad(1:nhidneurons(l),l,i)=bias_grad(1:nhidneurons(l),l,i)/(natoms_arr(i)) 
        END IF
      END DO
    END DO

    !Calculate weight grads 
    DO i=1, nelements
      !output layer's weight grads
      DO img=1, nimages
        DO k=1, natoms_arr(i)
          weight_grad(1:nhidneurons(nhidlayers),1,nhidlayers+1,i)=weight_grad(1:nhidneurons(nhidlayers),1,nhidlayers+1,i)+&
          all_neurons(k,1:nhidneurons(nhidlayers),nhidlayers,i,img)*bias_grad(1,nhidlayers+1,i)
        END DO
      END DO
      ! Average over the number of atoms and images
      weight_grad(1:nhidneurons(nhidlayers),1,nhidlayers+1,i)=&
      weight_grad(1:nhidneurons(nhidlayers),1,nhidlayers+1,i)/(natoms_arr(i)*nimages)
      
      DO l=nhidlayers,1,-1
        IF (l==1) THEN
          DO img=1, nimages
            DO k=1, natoms_arr(i)
              weight_grad(1:nGs(i),1:nhidneurons(l),l,i)=weight_grad(1:nGs(i),1:nhidneurons(l),l,i)+&
              MATMUL(reshape(input_fps(k,1:nGs(i),i,img),(/nGs(i),1/)),&
              reshape(bias_grad(1:nhidneurons(l),l,i),(/1,nhidneurons(l)/)))
            END DO
          END DO
          ! Average over the number of atoms and images
          weight_grad(1:nGs(i),1:nhidneurons(l),l,i)=weight_grad(1:nGs(i),1:nhidneurons(l),l,i)/(natoms_arr(i)*nimages)
        ELSE   
          DO img=1, nimages
            DO k=1, natoms_arr(i)
              weight_grad(1:nhidneurons(l-1),1:nhidneurons(l),l,i)=weight_grad(1:nhidneurons(l-1),1:nhidneurons(l),l,i)+&
              MATMUL(reshape(all_neurons(k,1:nhidneurons(l-1),l-1,i,img),(/nhidneurons(l-1),1/)),&
              reshape(bias_grad(1:nhidneurons(l),l,i),(/1,nhidneurons(l)/)))
            END DO
          END DO
          ! Average over the number of atoms and images
          weight_grad(1:nhidneurons(l-1),1:nhidneurons(l),l,i)=&
          weight_grad(1:nhidneurons(l-1),1:nhidneurons(l),l,i)/(natoms_arr(i)*nimages)
        END IF
      END DO
    END DO
    
    DO i=1, nelements
      DO l=1, nhidlayers+1
        if (l==nhidlayers+1) then
          print *, 'output layer weight grads of element', i
          print *, weight_grad(1:nhidneurons(l-1),1,l,i)
          print *, 'output layer bias grads of element', i
          print *, bias_grad(1,l,i)
        else if (l==1) then 
          print *, l,'th layer weight grads of element', i
          do j=1,nhidneurons(l)
            print *, weight_grad(1:nGs(i),j,l,i)
          end do  
          print *, l,'th layer bias grads of element', i
          print *, bias_grad(1:nhidneurons(l),l,i)
        else 
          print *, l,'th layer weight grads of element', i
          print *, weight_grad(1:nhidneurons(l-1),1:nhidneurons(l),l,i)
          print *, l,'th layer bias grads of element', i
          print *, bias_grad(1:nhidneurons(l),l,i)
        end if  
      END DO
    END DO 

  END SUBROUTINE
  
  FUNCTION backwardgrad(actval, weight, i, j, k) RESULT (backgrad)
    IMPLICIT NONE
    DOUBLE PRECISION, DIMENSION(i, k) :: actval
    DOUBLE PRECISION, DIMENSION(k, k) :: diagonal
    DOUBLE PRECISION, DIMENSION(k, j) :: weight
    DOUBLE PRECISION, DIMENSION(i,k,j) :: backgrad
    INTEGER :: a, b, i, j, k

    IF (actfuncId == 'sigmoid') THEN
      DO a = 1, i
        diagonal = 0.0d0
        DO b = 1, k
          diagonal(b,b) = actval(a,b)*(1.0d0 - actval(a,b))
        END DO
        backgrad(a,1:k,1:j) = MATMUL(diagonal,weight)
      END DO
    ELSE IF (actfuncId == 'tanh') THEN
      DO a = 1, i
        diagonal = 0.0d0
        DO b = 1, k
          diagonal(b,b) = 1.0d0-actval(a,b)**2
        END DO
        backgrad(a,1:k,1:j) = MATMUL(diagonal,weight)
      END DO
    ELSE IF (actfuncId == 'relu') THEN
      DO a = 1, i
        diagonal = 0.0d0
        DO b = 1, k
          IF (actval(a,b) > 0) THEN
            diagonal(b,b) = 1.0d0
          ELSE
            diagonal(b,b) = 0.0d0
          END IF
        END DO
        backgrad(a,1:k,1:j) = MATMUL(diagonal,weight)
      END DO
    END IF
  
  END FUNCTION

  SUBROUTINE LossFunction(force_coeff,energyloss, forceloss,loss)
      DOUBLE PRECISION, OPTIONAL :: force_coeff
      DOUBLE PRECISION, INTENT(OUT) :: energyloss, forceloss, loss
      
      IF (PRESENT(force_coeff)) THEN
        CONTINUE
      ELSE
        force_coeff=0.05
      END IF
      print *, 'inputE=', inputE
      print *, 'targetE=', targetE
      energyloss=SUM(((inputE-targetE)/natomsE)**(2.0)) 
      forceloss=SUM(SUM((inputF-targetF)**(2.0),dim=1)/natomsF)/3.0
      !loss=energyloss+forceloss*force_coeff 
      loss=energyloss
      
      !print *, 'energyloss=', energyloss
      !print *, 'forceloss=', forceloss
      print *, 'loss=', loss

  END SUBROUTINE

  !SUBROUTINE ForceTraining()
    ! Analytical solution of Force loss grad w.r.t each layer's parameters
  !  IMPLICIT NONE

  !  DO i=1, total_natoms
           
  !  END DO
  
  !END SUBROUTINE

  !FUNCTION dforces_dweights RESULT()
  ! Calculate derivatives of forces per atom w.r.t weights
  !  IMPLICIT NONE


  !END FUNCTION 

  !FUNCTION dedg_dweights RESULT()
    ! Calculate all layers' derivative of dedg per atom w.r.t weights
    !IMPLICIT NONE
   
    ! Output layer's
     
  !  DO l=1, nhidlayers
      
        
  !    END IF  
  !  END DO  
    
  
  !END FUNCTION

  !FUNCTION actgrad RESULT()
  ! Calculate derivative of activation function
  !  IMPLICIT NONE

  !  IF (


  !END FUNCTION
  SUBROUTINE backcleanup
    IMPLICIT NONE

    DEALLOCATE(input_fps)
    DEALLOCATE(all_neurons)
    DEALLOCATE(layer_backgrad)
    DEALLOCATE(bias_grad)
    DEALLOCATE(weight_grad)
    DEALLOCATE(natomsE)
    DEALLOCATE(natomsF)
    DEALLOCATE(inputE)
    DEALLOCATE(targetE)
    DEALLOCATE(inputF)
    DEALLOCATE(targetF)

  END SUBROUTINE
END MODULE

