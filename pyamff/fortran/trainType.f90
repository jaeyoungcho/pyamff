MODULE trainType

    IMPLICIT NONE
    PUBLIC

    LOGICAL :: energy_training
    INTEGER :: nimages, nAtimg, img_idx
    DOUBLE PRECISION, DIMENSION(:,:,:,:), ALLOCATABLE :: input_fps !calculated fps of all images 
    DOUBLE PRECISION, DIMENSION(:,:,:,:,:), ALLOCATABLE :: all_neurons !all neuron values of all images
    DOUBLE PRECISION, DIMENSION(:,:,:,:,:), ALLOCATABLE :: layer_backgrad
    DOUBLE PRECISION, DIMENSION(:,:,:), ALLOCATABLE :: bias_grad
    DOUBLE PRECISION, DIMENSION(:,:,:,:), ALLOCATABLE :: weight_grad
    INTEGER, DIMENSION(:), ALLOCATABLE :: natomsE, natomsF
    DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: inputE, targetE
    DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: inputF, targetF
   

    CONTAINS 

    SUBROUTINE dummyTrain()
    END SUBROUTINE

END MODULE
