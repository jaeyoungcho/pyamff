MODULE training
    USE nnType
    USE trainType
    USE neuralnetwork
    USE fnnmodule
    USE lossgrad
    USE adam
    IMPLICIT NONE
    
    CONTAINS

    SUBROUTINE train_init(nAtoms,nelement,atomicNrs,uniqueNrs)
        USE fpCalc
        IMPLICIT NONE
        INTEGER :: nAtoms, nelement
        INTEGER, PARAMETER :: max_fps = 100
        INTEGER, DIMENSION(nAtoms) :: atomicNrs
        INTEGER, DIMENSION(nelement) :: uniqueNrs

        CALL read_mlff(nAtoms, nelement, max_fps, atomicNrs, uniqueNrs)
        CALL prepfNN_ase(natoms, nelement, MAX_FPS, atomicNrs,uniqueNrs)
        CALL init_backward
    END SUBROUTINE

    SUBROUTINE trainExec(nAtoms,pos_car,box,atomicNumbers,nelement,uniqueNrs,opt_type,max_epoch)
        USE nlist
        USE fpCalc
        USE normalize
        IMPLICIT NONE
        CHARACTER(*) :: opt_type
        INTEGER :: nAtoms, max_epoch
        INTEGER :: NAT, i, j
        REAL, DIMENSION(9), INTENT(IN) :: box
        INTEGER, DIMENSION(nAtoms) :: atomicNumbers

        INTEGER, PARAMETER :: MAX_FPs = 100
        INTEGER, PARAMETER :: MAX_NEIGHS = 100
        INTEGER, PARAMETER ::forceEngine = 1
        DOUBLE PRECISION, DIMENSION(nAtoms,3) :: pos_car
        INTEGER, DIMENSION(nAtoms) :: symbols
        CHARACTER*3, DIMENSION(nAtoms) :: atomicSymbols

        DOUBLE PRECISION, DIMENSION(3,3) :: cell

        INTEGER :: nelement
        INTEGER, DIMENSION(nelement) :: uniqueNrs
        CHARACTER*20 :: filename
        CHARACTER*3, DIMENSION(nelement) :: uniq_elements
        DOUBLE PRECISION, DIMENSION(nAtoms,3) :: pos_dir
        DOUBLE PRECISION, DIMENSION(3,3) :: dir2car

        DOUBLE PRECISION, DIMENSION(nAtoms, MAX_FPS) :: fps
        DOUBLE PRECISION, DIMENSION(nAtoms, MAX_FPS, nimages) :: fps_img
        DOUBLE PRECISION, DIMENSION(nAtoms, MAX_NEIGHS, 3, MAX_FPS) :: dfps
        CHARACTER*3, DIMENSION(92) :: elementArray
        INTEGER, DIMENSION(nAtoms, MAX_NEIGHS) :: neighs
        INTEGER, DIMENSION(nAtoms) :: num_neigh
     
        DATA elementArray / "H","He","Li","Be","B","C","N","O", &
                 "F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc", &
                 "Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se", &
                 "Br","Kr","Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag", &
                 "Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd", &
                 "Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta", &
                 "W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn", &
                 "Fr","Ra","Ac","Th","Pa","U" /

        DO i = 1, nelement
            ! Read from pos.con
            uniq_elements(i) = elementArray(uniqueNrs(i))
        END DO

        DO i = 1, nAtoms
            DO j = 1, nelement
                IF (mod(atomicNumbers(i),uniqueNrs(j)) .EQ. 0) THEN
                    symbols(i) = j
                END IF
            END DO
        END DO

        cell(1,1) = box(1)
        cell(1,2) = box(2)
        cell(1,3) = box(3)

        cell(2,1) = box(4)
        cell(2,2) = box(5)
        cell(2,3) = box(6)

        cell(3,1) = box(7)
        cell(3,2) = box(8)
        cell(3,3) = box(9)

        nat = nAtoms

        dfps = 0.0
        fps = 0.0
        
        CALL calcfps(nAt, pos_car, cell, symbols, MAX_FPs, nelement, forceEngine, &
                     fps, dfps, neighs, num_neigh)
       
        CALL normalizeFPs(nelement, nAt, uniq_elements, symbols, MAX_FPS, MAXVAL(num_neigh), &
                          num_neigh, neighs, fps, dfps(:,1:MAXVAL(num_neigh)+1,:,:))
        
        ! Store fingerprints in 2d per image
        IF (energy_training .EQV. .TRUE.) THEN
            fps_img(:,:,img_idx)=fps
        END IF

        CALL forwardCalc(num_neigh, MAXVAL(num_neigh),neighs(:,1:MAXVAL(num_neigh)), symbols, &
                         fps, dfps(:,1:MAXVAL(num_neigh)+1,:,:), &
                         MAXVAL(nGs), MAXVAL(natoms_arr), MAXVAL(nhidneurons))
        IF (energy_training .EQV. .TRUE.) THEN
            inputE(img_idx) = Etotal-intercept
        END IF

        !DO i = 1, nAtoms
        !    inputF(1,) = forces(1,i)
        !    inputF(2,) = forces(2,i)
        !    inputF(3,) = forces(3,i)
        !END DO
        
        ! Call trainer when computations of all images are done
        IF (img_idx == nimages) THEN
            CALL Trainer(opt_type,max_epoch,num_neigh,MAXVAL(num_neigh),neighs(:,1:MAXVAL(num_neigh)),&
            symbols,fps,dfps(:,1:MAXVAL(num_neigh)+1,:,:),MAXVAL(nGs),MAXVAL(natoms_arr),MAXVAL(nhidneurons))
        END IF

    END SUBROUTINE
   
    SUBROUTINE Trainer(opt_type,maxepochs,num_neigh,max_num_neigh,neighs,symbols,fps,dfps,max_nGs,&
    max_natoms_arr, max_hidneurons,force_coeff,energyRMSEtol,forceRMSEtol,learningRate)
        IMPLICIT NONE
        CHARACTER(*) :: opt_type 
        INTEGER, INTENT(IN) :: maxepochs, max_num_neigh, max_nGs, max_natoms_arr, max_hidneurons
        INTEGER, DIMENSION(total_natoms), INTENT(IN) :: num_neigh, symbols
        INTEGER, DIMENSION(total_natoms, max_num_neigh) :: neighs
        DOUBLE PRECISION, DIMENSION(total_natoms,max_nGs,nimages) :: fps
        DOUBLE PRECISION, DIMENSION(total_natoms,max_num_neigh+1,3,max_nGs) :: dfps
        DOUBLE PRECISION, OPTIONAL :: energyRMSEtol, forceRMSEtol
        DOUBLE PRECISION, OPTIONAL :: force_coeff, learningRate
    
        !Must be input variables eventually
        DOUBLE PRECISION :: beta1, beta2, eps, weight_decay
        !variables
        INTEGER :: epoch, time, i
        DOUBLE PRECISION :: fconst, etol, ftol, lr
        DOUBLE PRECISION :: energyloss, forceloss, loss
        DOUBLE PRECISION :: energyRMSE, forceRMSE
    
        IF (PRESENT(force_coeff)) THEN
            fconst=force_coeff
        ELSE  
            fconst=0.1
        END IF
        IF (PRESENT(energyRMSEtol)) THEN
            etol=energyRMSEtol
        ELSE
            etol=0.01
        END IF
        IF (PRESENT(forceRMSEtol)) THEN
            ftol=forceRMSEtol
        ELSE  
            ftol=0.1
        END IF
        IF (PRESENT(learningRate)) THEN
            lr=learningRate
        ELSE  
            lr=0.01
        END IF   
    
        DO epoch=1, maxepochs
            print *, '*********************************'
            print *, 'epoch=', epoch
            print *, '*********************************'
            ! Compute energy and forces with updated parameters
            IF (epoch /= 1) THEN
                DO i=1, nimages
                  CALL forward(num_neigh, max_num_neigh, neighs, symbols, fps(:,:,i), dfps,&
                  max_nGs, max_natoms_arr, max_hidneurons)
                  !Update calculated energy of image i
                  inputE(i)=Etotal-intercept
                END DO
            END IF
    
            ! Compute loss and backward propagation
            CALL LossFunction(fconst,energyloss,forceloss,loss)
            CALL backward
    
            ! Step of optimizer 
            ! TODO: Needs to be generalized.
            IF (opt_type=='adam') THEN 
                IF (epoch == 1) THEN
                    CALL adam_init
                END IF
                !Temporary: Use PyAMFF default
                beta1=0.9
                beta2=0.999
                lr=0.01
                eps=1.0e-8
                weight_decay=0.
                CALL adam_step(beta1,beta2,lr,weight_decay,eps,epoch) 
            END IF
      
            energyRMSE=sqrt(energyloss/nimages)
            forceRMSE=sqrt(forceloss/nimages)
           
            print *, 'energyRMSE=', energyRMSE
            IF (energyRMSE < etol .and. forceRMSE < ftol) THEN 
                PRINT *, 'Minimization converged'
                CALL adam_cleanup
                STOP
            END IF
        END DO
        PRINT *, 'Minimization NOT converged. Check your config values' 
        
        ! Clean up arrays used for optimizer
        CALL adam_cleanup

    END SUBROUTINE 

    SUBROUTINE traincleanup
        USE fpCalc
        IMPLICIT NONE

        CALL cleanup
        CALL nncleanup
        CALL backcleanup
    END SUBROUTINE
 
END MODULE

