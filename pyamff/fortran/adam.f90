MODULE adam
  USE nnType
  USE trainType
  IMPLICIT NONE
  DOUBLE PRECISION, DIMENSION(:,:,:), ALLOCATABLE :: bias_v, bias_m
  DOUBLE PRECISION, DIMENSION(:,:,:,:), ALLOCATABLE :: weight_v, weight_m
   
  CONTAINS

  SUBROUTINE adam_init
    IMPLICIT NONE
    ! Allocate first moment and second moment arrays
    ALLOCATE(bias_m(MAXVAL(nhidneurons),nhidlayers+1,nelements))
    ALLOCATE(bias_v(MAXVAL(nhidneurons),nhidlayers+1,nelements))
    ALLOCATE(weight_m(max(MAXVAL(nGs),MAXVAL(nhidneurons)),MAXVAL(nhidneurons),nhidlayers+1,nelements))
    ALLOCATE(weight_v(max(MAXVAL(nGs),MAXVAL(nhidneurons)),MAXVAL(nhidneurons),nhidlayers+1,nelements))
    
    ! Zeros arrays
    bias_v=0.
    bias_m=0.
    weight_v=0.
    weight_m=0.
  END SUBROUTINE

  SUBROUTINE adam_step(beta1,beta2,lr,weight_decay,eps,time)
    IMPLICIT NONE
    DOUBLE PRECISION :: beta1,beta2,lr,weight_decay,eps 
    INTEGER, INTENT(IN) :: time
    ! Variables
    INTEGER :: i,k,l

    ! Zeros arrays
    bias_v=0.
    bias_m=0.
    weight_v=0.
    weight_m=0.
    
    ! Update decary factors w.r.t time
    beta1=beta1**time
    beta2=beta2**time
    
    ! Update learning rate
    lr=lr*sqrt(1-beta2)/(1-beta1)
    
    ! Update parameters
    DO i=1, nelements
      ! Update first/second moments of input weights and biases
      weight_m(1:nGs(i),1:nhidneurons(1),1,i)=&
      beta1*weight_m(1:nGs(i),1:nhidneurons(1),1,i)+(1-beta1)*weight_grad(1:nGs(i),1:nhidneurons(1),1,i)
      weight_v(1:nGs(i),1:nhidneurons(1),1,i)=&
      beta2*weight_v(1:nGs(i),1:nhidneurons(1),1,i)+(1-beta2)*((weight_grad(1:nGs(i),1:nhidneurons(1),1,i))**2)

      bias_m(1:nhidneurons(1),1,i)=&
      beta1*bias_m(1:nhidneurons(1),1,i)+(1-beta1)*bias_grad(1:nhidneurons(1),1,i)
      bias_v(1:nhidneurons(1),1,i)=&
      beta2*bias_v(1:nhidneurons(1),1,i)+(1-beta2)*((bias_grad(1:nhidneurons(1),1,i))**2)

      ! Update input weights and biases
      in_weights(1:nGs(i),1:nhidneurons(1),i)=&
      in_weights(1:nGs(i),1:nhidneurons(1),i)&
      -lr*weight_m(1:nGs(i),1:nhidneurons(1),1,i)/(sqrt(weight_v(1:nGs(i),1:nhidneurons(1),1,i))+eps)
      print *, 'updated input weights'
      print *, in_weights(1:nGs(i),1:nhidneurons(1),i)
      DO k=1, natoms_arr(i)
        in_biases(k,1:nhidneurons(1),i)=&
        in_biases(k,1:nhidneurons(1),i)&
        -lr*bias_m(1:nhidneurons(1),1,i)/(sqrt(bias_v(1:nhidneurons(1),1,i))+eps)
      END DO 
      print *, 'updated input biases'
      print *, in_biases(1,1:nhidneurons(1),i)
      DO l=1, nhidlayers-1
        ! Update first/second moments of input weights and biases 
        weight_m(1:nhidneurons(l),1:nhidneurons(l+1),l+1,i)=&
        beta1*weight_m(1:nhidneurons(l),1:nhidneurons(l+1),l+1,i)&
        +(1-beta1)*weight_grad(1:nhidneurons(l),1:nhidneurons(l+1),l+1,i)
        weight_v(1:nhidneurons(l),1:nhidneurons(l+1),l+1,i)=&
        beta2*weight_v(1:nhidneurons(l),1:nhidneurons(l+1),l+1,i)&
        +(1-beta2)*(weight_grad(1:nhidneurons(l),1:nhidneurons(l+1),l+1,i)**2)  
        
        bias_m(1:nhidneurons(l+1),l+1,i)=&
        beta1*bias_m(1:nhidneurons(l+1),l+1,i)+(1-beta1)*bias_grad(1:nhidneurons(l+1),l+1,i)
        bias_v(1:nhidneurons(l+1),l+1,i)=&
        beta2*bias_v(1:nhidneurons(l+1),l+1,i)+(1-beta2)*(bias_grad(1:nhidneurons(l+1),l+1,i)**2)

        ! Update lth hidden layer's weights and biases
        hid_weights(1:nhidneurons(l),1:nhidneurons(l+1),l,i)=&
        hid_weights(1:nhidneurons(l),1:nhidneurons(l+1),l,i)&
        -lr*weight_m(1:nhidneurons(l),1:nhidneurons(l+1),l+1,i)/&
        (sqrt(weight_v(1:nhidneurons(l),1:nhidneurons(l+1),l+1,i))+eps)
        print *, l,'th hiddenlayer weights'
        print *, hid_weights(1:nhidneurons(l),1:nhidneurons(l+1),l,i)
        DO k=1, natoms_arr(i)
          hid_biases(k,1:nhidneurons(l+1),l,i)=&
          hid_biases(k,1:nhidneurons(l+1),l,i)&
          -lr*bias_m(1:nhidneurons(l+1),l+1,i)/(sqrt(bias_v(1:nhidneurons(l+1),l+1,i))+eps)
        END DO
        print *, l,'th hiddenlayer biases'
        print *, hid_biases(1,1:nhidneurons(l+1),l,i)
      END DO
      
      ! Update first moments of input weights and biases  
      weight_m(1:nhidneurons(nhidlayers),1,nhidlayers+1,i)=&
      beta1*weight_m(1:nhidneurons(nhidlayers),1,nhidlayers+1,i)&
      +(1-beta1)*weight_grad(1:nhidneurons(nhidlayers),1,nhidlayers+1,i)
      weight_v(1:nhidneurons(nhidlayers),1,nhidlayers+1,i)=&
      beta2*weight_v(1:nhidneurons(nhidlayers),1,nhidlayers+1,i)&
      +(1-beta2)*(weight_grad(1:nhidneurons(nhidlayers),1,nhidlayers+1,i)**2)

      bias_m(1,nhidlayers+1,i)=&
      beta1*bias_m(1,nhidlayers+1,i)+(1-beta1)*bias_grad(1,nhidlayers+1,i)
      bias_v(1,nhidlayers+1,i)=&
      beta2*bias_v(1,nhidlayers+1,i)+(1-beta2)*(bias_grad(1,nhidlayers+1,i)**2)

      ! Update output layerr's weights and biases
      out_weights(1:nhidneurons(nhidlayers),1,i)=&
      out_weights(1:nhidneurons(nhidlayers),1,i)&
      -lr*weight_m(1:nhidneurons(nhidlayers),1,nhidlayers+1,i)/&
      (sqrt(weight_v(1:nhidneurons(nhidlayers),1,nhidlayers+1,i))+eps)
      print *, 'output layer weight'
      print *, out_weights(1:nhidneurons(nhidlayers),1,i)
      DO k=1, natoms_arr(i)
        out_biases(k,1,i)=out_biases(k,1,i)&
        -lr*bias_m(1,nhidlayers+1,i)/(sqrt(bias_v(1,nhidlayers+1,i))+eps)
      END DO
      print *, 'output layer biases'
      print *, out_biases(1,1,i)
    END DO  

  
  END SUBROUTINE

  SUBROUTINE adam_cleanup
    IMPLICIT NONE

    DEALLOCATE(weight_v)
    DEALLOCATE(weight_m)
    DEALLOCATE(bias_v)
    DEALLOCATE(bias_m)
    
  END SUBROUTINE

END MODULE
