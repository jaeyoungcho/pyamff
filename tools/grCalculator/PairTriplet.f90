MODULE ptcalc
       USE nlist
       USE fbp

       IMPLICIT NONE
     
       PUBLIC
     
       CONTAINS
     
     
       SUBROUTINE findPT(nAtoms, pos_car, cell, symbols, nelement,forceEngine,max_rcut,&
                         npairs, pairs, pair_indices, ntriplets, angles, angle_indices)

         INTEGER, PARAMETER :: MAX_NEIGHS  = 8
         INTEGER, INTENT(IN) :: nAtoms, nelement, forceEngine
         DOUBLE PRECISION, INTENT(IN) :: max_rcut
         INTEGER*4, DIMENSION(nAtoms), INTENT(IN) :: symbols
         DOUBLE PRECISION, DIMENSION(nAtoms,3), INTENT(IN) :: pos_car
         DOUBLE PRECISION, DIMENSION(3,3), INTENT(IN) :: cell

         ! variables for nlist.calc
         INTEGER :: j, k, l
         INTEGER, INTENT(OUT) :: npairs
         INTEGER :: maxneighs
         !INTEGER, DIMENSION(nAtoms, MAX_NEIGHS) :: neighs
         DOUBLE PRECISION, DIMENSION(nAtoms*MAX_NEIGHS), INTENT(OUT) :: pairs
         INTEGER, DIMENSION(2, nAtoms*MAX_NEIGHS), INTENT(OUT) :: pair_indices
         INTEGER, DIMENSION(nAtoms) :: pair_start, pair_end
         DOUBLE PRECISION,DIMENSION(2, nAtoms*MAX_NEIGHS, 3) :: unitvects_pair
         DOUBLE PRECISION, DIMENSION(:, :), ALLOCATABLE :: rmins



         ! variables for calcTriplet
         INTEGER, INTENT(OUT) :: ntriplets
         DOUBLE PRECISION, DIMENSION(3, nAtoms*MAX_NEIGHS*MAX_NEIGHS), INTENT(OUT) :: angles
         INTEGER, DIMENSION(3, nAtoms*MAX_NEIGHS*MAX_NEIGHS), INTENT(OUT) :: angle_indices
         DOUBLE PRECISION, DIMENSION(3, 3) :: vectsigns

         ! output values
         INTEGER, DIMENSION(nAtoms, MAX_NEIGHS) :: neighs
         INTEGER, DIMENSION(nAtoms) :: num_neigh

         print *, 'calcing Nlist'
         ALLOCATE(rmins(nelement, nelement))
         CALL calcNlist(nAtoms, MAX_NEIGHS, pos_car, cell, max_rcut, nelement, symbols, forceEngine, rmins, &
                        npairs, num_neigh, neighs, pairs, pair_indices, pair_start, pair_end, unitvects_pair)
         print *, 'calcNlist done'
         maxneighs = MAXVAL(num_neigh)

         CALL calcTriplet(nAtoms, MAX_NEIGHS, npairs, pair_indices(2,1:npairs), pair_start, pair_end,&
                          unitvects_pair(1, 1:npairs, :), ntriplets, angles, angle_indices)
         DEALLOCATE(rmins)
       END SUBROUTINE
END MODULE
