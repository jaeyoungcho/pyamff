"""Structure optimization. """

import sys
import time
from math import sqrt
from os.path import isfile

import collections


class Optimizer(object):
    """Base-class for all structure optimization classes."""
    def __init__(self, fps, logfile):
        """Structure optimizer object.

        Parameters:

        restart: str
            Filename for restart file.  Default value is *None*.
        
        logfile: file object or str
            If *logfile* is a string, a file with that name will be opened.
            Use '-' for stdout.
        
        master: boolean
            Defaults to None, which causes only rank 0 to save files.  If
            set to true,  this rank will save files.
        """
        self.logfile = open(logfile,'w')

        self.fps = fps
        self.nsteps=0

    def todict(self):
        description = {'type': 'optimization',
                       'optimizer': self.__class__.__name__}
        return description
 
    def initialize(self):
        pass

    def run(self, fmax=0.05, steps=100000000):
        """Run structure optimization algorithm.

        This method will return when the forces on all individual
        atoms are less than *fmax* or when the number of steps exceeds
        *steps*."""

        self.fmax = fmax
        step = 0
        while step < steps:
            f = self.fps.get_gradients()
            self.log(f)
            if self.converged(f):
                return
            self.step(f)
            self.nsteps += 1
            step += 1

    def converged(self, forces=None):
        """Did the optimization converge?"""
        if forces is None:
            forces = self.fps.get_gradients()
        #if hasattr(self.atoms, 'get_curvature'):
        #    return ((forces**2).sum(axis=1).max() < self.fmax**2 and
        #            self.atoms.get_curvature() < 0.0)
        return (forces**2).sum(axis=1).max() < self.fmax**2

    def log(self, forces):
        fmax = sqrt((forces**2).sum(axis=1).max())
        e = self.fps.get_tao()
        fp_paras=' '
        print(self.fps.get_paras())
        for paras in self.fps.get_paras():
           for i in range(len(paras)):
              if i ==0:
                 item = paras[i]*1000.
              else: 
                 item = paras[i]
              fp_paras += '{:14.8f}'.format(item)
        T = time.localtime()
        if self.logfile is not None:
            name = self.__class__.__name__
            print(name, self.nsteps, T[3], T[4], T[5], fp_paras, e, forces, fmax)
            self.logfile.write('%s: %3d  %02d:%02d:%02d %s %15.6f %12.8f\n' %
                               (name, self.nsteps, T[3], T[4], T[5], fp_paras, e, fmax))
            self.logfile.flush()
        
