import sys
from amp import Amp
from ase.io import read

args = sys.argv
imgs = read(args[1], index=":")
calc =Amp.load('amp.amp')
i=0
#calc.model.vector(len(imgs[0]))
for img in imgs:
  img.set_calculator(calc)
  print('image:',i)
  print(img.get_potential_energy())
  print(img.get_forces())
  i+=1
