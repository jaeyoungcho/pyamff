.. _trainNN_tutorial:

=================
Train NN Tutorial
=================

You can find the following tutorial under the pyamff/examples/pyamffTrain directory of your copy of PyAMFF. The system is a set of four Pd13H2 clusters with energies and forces. 

If you have already added pyamff/bin to your *$PATH*, navigate to the above directory and type::
    
    $ pyamff

If you have not added pyamff/bin to your *$PATH*, simply type::
    
    $ ../../bin/pyamff

After running, you should now see the following files:

* pyamff.log -- log output from pyamff run


* pyamff.pt -- the saved pyamff forcefield in pytorch readable format


* mlff.pyamff -- the output model parameters in human readable format (for use with fortran module)


* fps.pckl -- all fingerprint information stored in a pickle file


