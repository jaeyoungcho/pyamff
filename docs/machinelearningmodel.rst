.. _machinelearningmodel:

====================
MachineLearningModel
====================

These are the options that go in **[MachineLearningModel]** section of the config.ini file.

    **options**

        **model_type**: The type of machine learning protocol to be used.
            **kind**: string

            **default**: ``neural_network``
            
            **values**:
                - neural_network

        **activation_function**:
            **kind**: string

            **default**: sigmoid

            **values**:
                - sigmoid
                - relu
                - tanh
                - softplus

        **hidden_layers**: Takes an integer list for the number of hidden layers and nodes

            **kind**: integerlist
            
            **default**: ``20 10``


