.. _parralelization:

===============
Parralelization
===============

These are the options that go in **[Parallelization]** section of the config.ini file.
Total number of batches are equal to 'process_number * batch_number'.

**number_process**: Takes an integer as the number of parallel jobs to perform.
    default: ``1``
**batch_number**: Takes an integer as the number of batches per process.
    default: ``1``

