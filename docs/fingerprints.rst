.. _fingerprints:

============
Fingerprints
============

These are the options that go in **[Fingerprints]** section of the config.ini file.

**fp_type**: The type of fingerprint to be calculated.

    default: ``BP``

    options:

        ``BP``: Behler-Parinello symmetry functions (G1 and G2)

**fp_engines**: 
 
    default: ``Fortran``

    options: 

        ``Fortran``
        ``OldFortran``


**parameter_file**: Name of fingerprint parameter file.

    default: ``fp_paras.dat``


