.. _debug:

=====
Debug
=====

    options:

These are the options that go in **[Debug]** section of the config.ini file.

**init_model_parameters**: The model paras.
    kind: ``boolean``
    default: ``False``
